var app = angular.module('dressbook.filters');

app.filter('groupUserNameByAlphabet', ['$filter',
  function($filter) {
    var dividers = {};

    return function(users) {
      if (!users || !users.length) return;

      var output = [],
        previousKey,
        currentKey;

      // sort users
      users = _.orderBy(users,[function(item){
        if(item.user){
          if(!item.user.name){
            return "";
          }
          return item.user.name.slice( 0, 1 ).toUpperCase();
        }else if(item.name){
          return item.name.slice( 0, 1 ).toUpperCase();
        }else{
          return "";
        }
      }],['ASC']);

      for (var i = 0, ii = users.length; i < ii && (item = users[i]); i++) {
        var currentKey;
        if(item.user){
          if(!item.user.name){
            currentKey = "";
          }else{
            currentKey = item.user.name.slice( 0, 1 ).toUpperCase();
          }
        }else if(item.name){
          currentKey = item.name.slice( 0, 1 ).toUpperCase();
        }else{
          currentKey = "";
        }

        if (_.isUndefined(previousKey) || currentKey != previousKey ) {

          var dividerId = currentKey;

          if (!dividers[dividerId]) {
            dividers[dividerId] = {
              isDivider: true,
              divider: currentKey
            };
          }

          output.push(dividers[dividerId]);
        }

        output.push(item);
        previousKey = currentKey;
      }

      return output;
    };
  }
])
