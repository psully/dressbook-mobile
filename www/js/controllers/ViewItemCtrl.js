var app = angular.module('dressbook.controllers');

app.controller('ViewItemCtrl', function($rootScope, $timeout, $scope, $stateParams, $state, $q, $ionicModal, $ionicLoading, Borrow, Order, Category, Color, Size, Brand, Item, AppConfig, AuthFactory, Utils, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('View item view');

  var $this = this;
  $scope.AppConfig = AppConfig;
  $scope.Utils = Utils;
  $scope.providerAvatar = AppConfig.defaultAvatar;
  $scope.newCommentData = {};
  $scope.user = $rootScope.main.user;

  // show loading
  $ionicLoading.show("Loading...");

  $scope.borrowBookFormData = {
    from: $stateParams.startDate,
    to: $stateParams.returnDate
  };
  $scope.forms = {};

  var itemId = $stateParams.id;
  // load item
  var item = Item.get({
    id: itemId
  }, function() {
    // must set item here because using comment directive
    $scope.item = item;

    $scope.isOwn = $scope.user._id == $scope.item.provider._id;

    $scope.itemImages = Item.imagesLinks(item, 'medium');

    $timeout(function() {
      $(".item-carousel.image").owlCarousel({
        singleItem:true,
        autoHeight : true
      });
    }, 0);

    $scope.providerAvatar = Utils.getUserAvatar($scope.item.provider);

    async.parallel([
      // get own borrow
      function(callback) {
        $scope.item.getOwnBorrow().then(function(borrowBook) {
          callback(null, borrowBook);
        }, function() {
          callback("Error in load borrow");
        });
      },
      // get current order
      function(callback) {
        $scope.item.getOwnOrderNoResponse().then(function(order) {
          callback(null, order);
        }, function() {
          callback("Error in load order");
        });
      }
    ], function(err, results) {
      $ionicLoading.hide();

      if (err) {
        $ionicLoading.hide();
        swalFac.error({
          text: 'Has error in loading data. Please try again.',
          title: 'Error!'
        });
        return;
      }

      var borrowBook = results[0];
      var order = results[1];

      if (borrowBook) {
        $scope.borrowBook = borrowBook;
      }

      if (order) {
        $scope.order = order;
      }


      var disabledDates = [];
      if ($scope.item.timesInvalid) {
        $scope.item.timesInvalid.forEach(function(rangeInvalid) {
          var fr = new Date(rangeInvalid[0]);
          var to = new Date(rangeInvalid[1]);

          var currentDate = new Date().setHours(0, 0, 0, 0);
          var start = currentDate > fr ? currentDate : fr;

          var i = start;
          while (i <= to) {
            disabledDates.push(new Date(i));
            i.setDate(i.getDate() + 1);
          };
        });
      }

      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);
      $scope.borrowBookFromPicker = {
        titleLabel: 'Borrow available from',
        templateType: 'popup',
        inputDate: new Date(),
        mondayFirst: true,
        from: yesterday,
        disabledDates: disabledDates,
        callback: function(val) {
          if (typeof(val) === 'undefined') {
            // do nothing
          } else {
            $scope.borrowBookFormData.to = undefined;
            $scope.borrowBookFromPicker.inputDate = new Date(val);
            $scope.borrowBookFormData.from = moment(val).format("DD MMM YYYY");

            $scope.borrowBookToPicker.from = new Date(val);
            $scope.borrowBookToPicker.inputDate = $scope.borrowBookFromPicker.inputDate;

            if (disabledDates && disabledDates.length) {
              for (var i = 0; i < disabledDates.length; i++) {
                if (disabledDates[i] >= $scope.borrowBookFromPicker.inputDate) {
                  var to = new Date(disabledDates[i]);
                  to.setDate(to.getDate() - 1);

                  if (to > $scope.borrowBookFromPicker.inputDate) {
                    $scope.borrowBookToPicker.to = to;
                  } else {
                    $scope.borrowBookToPicker.to = $scope.borrowBookFromPicker.inputDate;
                  }
                  break;
                }
              }
            }

            $scope.forms.borrowBookFrm.from.$dirty = true;
            $scope.forms.borrowBookFrm.from.$validate();
            $scope.forms.borrowBookFrm.to.$validate();
          }
        }
      };

      if ($scope.item.borrowAvailableRange) {
        if ($scope.item.borrowAvailableRange.from) {
          $scope.borrowBookFromPicker.from = new Date($scope.item.borrowAvailableRange.from);
        }
        if ($scope.item.borrowAvailableRange.to) {
          $scope.borrowBookFromPicker.to = new Date($scope.item.borrowAvailableRange.to);
        }
      }


      $scope.borrowBookToPicker = {
        titleLabel: 'Borrow available to',
        templateType: 'popup',
        mondayFirst: true,
        from: yesterday,
        callback: function(val) {
          if (typeof(val) === 'undefined') {
            // do nothing
          } else {
            $scope.borrowBookToPicker.inputDate = new Date(val);
            $scope.borrowBookFormData.to = moment(val).format("DD MMM YYYY");

            $scope.forms.borrowBookFrm.to.$dirty = true;
            $scope.forms.borrowBookFrm.from.$validate();
            $scope.forms.borrowBookFrm.to.$validate();
          }
        }
      };
      if ($scope.item.borrowAvailableRange) {
        if ($scope.item.borrowAvailableRange.from) {
          $scope.borrowBookToPicker.from = new Date($scope.item.borrowAvailableRange.from);
        }
        if ($scope.item.borrowAvailableRange.to) {
          $scope.borrowBookToPicker.to = new Date($scope.item.borrowAvailableRange.to);
        }
      }
    });
  }, function() {
    $ionicLoading.hide();

    swalFac.error({
      text: 'Has error in loading item. Please try again.',
      title: 'Error!'
    });
  });

  var reqBorrowBookPopup = undefined;
  $scope.borrowBookReq = function(event, form) {
    // show loading
    $ionicLoading.show("Submiting...");
    // create borrow request
    GAnalyticsHelper.trackEvent('Items', 'Borrow item');

    var borrowBook;
    if ($this.isBorrowBookEditing) {
      borrowBook = $scope.borrowBook;
    } else {
      borrowBook = new Borrow({
        item: itemId
      });
    }
    borrowBook.startDate = $scope.borrowBookFromPicker.inputDate;
    borrowBook.returnDate = $scope.borrowBookToPicker.inputDate;

    borrowBook.$save(function() {
      $ionicLoading.hide();
      $scope.borrowBook = borrowBook;

      // hide modal
      if ($scope.borrowBookModal) {
        $scope.borrowBookModal.hide();
      }

      // show success
      swalFac.success({
        text: 'Your request to borrow this item has been submitted to the owner. Watch out for their confirmation.',
        title: 'Success'
      });
    }, function(rps) {
      $ionicLoading.hide();
      var errorMsg = 'Has error in submit borrow book. Please try again.';
      if (rps.status == 406) {
        errorMsg = 'this item is already out on loan, try another date or item.';
      }
      swalFac.error({
        text: errorMsg,
        title: 'Error!'
      });
    });
  };

  this.isBorrowBookEditing = false;
  $scope.borrowBookOpen = function($event, isEdit) {
    $event.preventDefault();

    $this.isBorrowBookEditing = isEdit;

    $ionicModal.fromTemplateUrl('js/templates/item/borrow_book_modal.html', function($ionicModal) {
      $scope.borrowBookModal = $ionicModal;
      // show modal
      $scope.borrowBookModal.show();
    }, {
      scope: $scope
    });
  };

  $scope.borrowBookClose = function($event) {
    $event.preventDefault();
    $scope.borrowBookModal.hide();
  };

  $scope.$watch('borrowBook', function(newValue) {
    if (newValue && newValue.is_accept !== true && newValue.is_accept !== false) {
      $scope.borrowBookFormData.from = moment(newValue.startDate).format("DD MMM YYYY");
      $scope.borrowBookFormData.to = moment(newValue.returnDate).format("DD MMM YYYY");

      if ($scope.borrowBookFromPicker) {
        $scope.borrowBookFromPicker.inputDate = new Date(newValue.startDate);
      }
      if ($scope.borrowBookToPicker) {
        $scope.borrowBookToPicker.inputDate = new Date(newValue.returnDate);
        $scope.borrowBookToPicker.from = new Date(newValue.startDate);
      }
    }
  }, true);
  var handler = StripeCheckout.configure({
    key: AppConfig.stripeKey,
    image: 'img/dressbook_logo5.png',
    locale: 'auto',
    token: function(token) {
      var order = new Order({
        item: $scope.item._id,
        token: token
      });

      order.$save(function(rps) {
        $ionicLoading.hide();
        $scope.order = order;
        if (!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)) {
          $scope.$apply();
        }
      }, function(rps) {
        $ionicLoading.hide();
        swalFac.error({
          text: 'Has error in submit order. Please try again.',
          title: 'Error!'
        });
      });
    }
  });

  $scope.requestBuy = function($event) {
    $event.preventDefault();
    $ionicLoading.show();
    // Open Checkout with further options:
    handler.open({
      name: 'Buy item on dressbook',
      description: 'Checkout item "' + $scope.item.name + '"',
      amount: $scope.item.cost * 100,
      currency: 'GBP',
      email: $rootScope.main.user.email
    });

    GAnalyticsHelper.trackEvent('Items', 'Buy item');
  }

  // Close Checkout on page navigation:
  $(window).on('popstate', function() {
    handler.close();
  });

  /*$scope.requestBuy = function($event) {
    $event.preventDefault();

    GAnalyticsHelper.trackEvent('Items', 'Buy item');
    // show confirm
    swalFac.confirm({
      title: 'Confirm order',
      text: 'Are you sure you want to buy item "'+$scope.item.name+'"?',
      closeOnConfirm: false,
      showLoaderOnConfirm: true
    }).then(function(){
      // $ionicLoading.show("Submiting...");

      var order = new Order({
        item: $scope.item._id
      });
      
      order.$save(function(rps) {
        console.log(rps);
        // $ionicLoading.hide();
        $scope.order = order;

        swalFac.close();
      }, function(rps) {
        // $ionicLoading.hide();
        swalFac.error({
          text: 'Has error in submit order. Please try again.',
          title: 'Error!'
        });
      });
    }, function(){
      console.log("Cancel");
    });
  };*/
});
