var app = angular.module('dressbook.controllers');


//  IMAGE CONTROLLER FOR THE HOME PAGE.
app.controller("GetImagesCtrl", function($scope, AppConfig){
  $scope.images = [];

  $scope.loadimages = function(){
    for (var i =0; i < 100; i ++){
      $scope.images.push({id: i, src: AppConfig.server+"/50x50"});
    }
  }

});
