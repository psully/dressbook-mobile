var app = angular.module('dressbook.controllers');
app.controller('Message_NewChatCtrl', function($scope, $ionicScrollDelegate, $window, $http, $timeout, $ionicLoading, User, AppConfig, AuthFactory, ChatFactory, Utils, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Message new chat view');

  $scope.userId = AuthFactory.getUserFromToken().id;
  $scope.utils = Utils;

  $scope.searchChattersKeyword = undefined;

  var currentPage = 0;
  var maxPagesLoad = 8;
  var endInfiniteLoad = false;
  var perPage = 25;

  $scope.moreDataCanBeLoaded = function(){
    return !endInfiniteLoad && currentPage <= maxPagesLoad;
  };

  $scope.chatters = [];
  $scope.showEmpty = false;

  var reloadChatters = function(){
    // reset state
    currentPage = 0;
    endInfiniteLoad = false;
    $scope.chatters = [];
    $scope.showEmpty = false;
    // begin search
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };

  $scope.loadMoreChatters = function(){
    ChatFactory.loadChatters({
      page:++currentPage,
      perPage:perPage,
      keyword:$scope.searchChattersKeyword
    }).then(function(rps){
      endInfiniteLoad = rps.data < perPage;

      // build avatar
      rps.data = _.map(rps.data, function(item){
        item.user.avatar = Utils.getUserAvatar(item.user);
        return item;
      });

      rps.data.forEach(function(item){
        $scope.chatters.push(item);
      });

      // success
      if($scope.chatters.length == 0){
        $scope.showEmpty = true;
      }

      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, function(err){
      swalFac.error({
        text: 'Has error in loadding data. Please try again.',
        title: 'Error!'
      });
    });
  }

  var typingTimeOut;
  var searchChanged = false;
  $scope.typingSearchChatters = function($event){
    if(typingTimeOut){
      $timeout.cancel(typingTimeOut);
      typingTimeOut = undefined;
    }
    searchChanged = false;

    typingTimeOut = $timeout(function(){
      if(!searchChanged){
        reloadChatters();
        searchChanged = true;
      }
      typingTimeOut = undefined;
    },1000);
  };

  $scope.reqSearchChatters = function($event){
    if(!searchChanged){
      reloadChatters();
      searchChanged = true;
    }
  };
});
