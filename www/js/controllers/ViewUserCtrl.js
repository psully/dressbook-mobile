var app = angular.module('dressbook.controllers');

app.controller('ViewUserCtrl', function($scope, $stateParams, $rootScope, $window, $http, $timeout, $ionicLoading, User, AppConfig, AuthFactory, GAnalyticsHelper, Utils, swalFac) {
  GAnalyticsHelper.trackView('View user view');

	var userId = $stateParams.user;

  $scope.$watch(function($scope){
    return $rootScope.main.user.friends && $rootScope.main.user.friends.indexOf(userId) != -1;
  }, function(newValue, oldValue){
    $scope.isFriend = newValue;
  });

  $scope.$watch(function($scope){
    return $rootScope.main.user.friendsRequesting && $rootScope.main.user.friendsRequesting.indexOf(userId) != -1;
  }, function(newValue, oldValue){
    $scope.isFriendRequesting = newValue;
  });

  $scope.isFriend = $rootScope.main.user.friends && $rootScope.main.user.friends.indexOf(userId) != -1;
  $scope.isFriendRequesting = $rootScope.main.user.friendsRequesting && $rootScope.main.user.friendsRequesting.indexOf(userId) != -1;

	// show loading
	$ionicLoading.show({
		template: 'Loading...'
	});

	$scope.user = User.get({id:userId}, function(user){
    // event add friend
    $scope.addFriend = function(event) {
      event.preventDefault();

      swalFac.confirmSuccessWithAvatar({
        title: 'Confirm friend request',
        imageUrl: Utils.getUserAvatar(user),
        html: 'Are you sure want to become friend with <strong>'+user.username+'</strong>',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel',
        showLoaderOnConfirm: true,
        closeOnConfirm: false
      }).then(function(){
        // show loadding
        $scope.isAddFriendReqLoading = true;
        $scope.main.user.reqAddFriend(user._id).then(function(rps) {
          $scope.isAddFriendReqLoading = false;
          $scope.isFriendRequesting = true;
          $rootScope.main.user.friendsRequesting.push(userId);

          swalFac.success({
            title: "Friend request send",
            html: 'Your friend request has been sent to <strong>'+user.username+'</strong>.'
          });
        }, function(err) {
          // show error
          $scope.isAddFriendReqLoading = false;
          swalFac.error({
            text: 'Has error in request add friend. Please try again.',
            title: 'Error!'
          });
        });
      }, function(){
        console.log("cancel");
      });
    };

		async.parallel([
			// count user items
			function(callback){
				$scope.user.countItems().then(function(res){
					callback(null, res.data.count);
				},callback);
			},
			// count items liked
			function(callback){
				$scope.user.countItemsLiked().then(function(res){
					callback(null, res.data.count);
				},callback);
			},
		], function(err, results){
			// hide loading
			$ionicLoading.hide();
			if(err){
				return swalFac.error({
					text: 'Has error in getting data. Please try again.',
					title: 'Error!'
				});
			};

			$scope.countItems = results[0];
			$scope.countItemsLiked = results[1];
		});
	});
});
