var app = angular.module('dressbook.controllers');

app.controller('TAndCCtrl', function($scope, $state, $ionicPopup, $ionicLoading, $timeout, $stateParams, AuthFactory, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('T and C');

  $scope.back = $stateParams.back;
});
