var app = angular.module('dressbook.controllers');
moment.locale('en', {
	relativeTime: {
		future: "in %s",
		past: "%s ago",
		s: "%d sec",
		m: "a minute",
		mm: "%d minutes",
		h: "an hour",
		hh: "%d hours",
		d: "a day",
		dd: "%d days",
		M: "a month",
		MM: "%d months",
		y: "a year",
		yy: "%d years"
	}
});
app.controller('Message_ChatCtrl', function($scope, $ionicPosition, $state, $stateParams, $ionicScrollDelegate, $window, $http, $timeout, $ionicLoading, User, AppConfig, AuthFactory, ChatFactory, ChatFactory, Utils, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Private chat view');
	GAnalyticsHelper.trackEvent('Chats', 'Use private chat','Private chat');

	$scope.utils = Utils;

	var socket = null;
	/*$state.get('app.message_chat').onExit = function(){
		if(socket){
			console.log("required disconnect");
        	socket.removeAllListeners();
			socket.disconnect();
			delete socket;
		}
	};*/

	$scope.$on( /*'$destroy'*/ '$ionicView.leave', function(event) {
		if (socket) {
			console.log("required disconnect");
			socket.removeAllListeners();
			socket.disconnect();
			delete socket;
		}
	});

	var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
	var footerBar; // gets set in $ionicView.enter
	var scroller;
	var txtInput;
	$scope.$on('$ionicView.enter', function() {
		$timeout(function() {
			footerBar = document.body.querySelector('.page-message-chat .bar-footer');
			scroller = document.body.querySelector('.page-message-chat .scroll-content');
			txtInput = $('.page-message-chat .bar-footer textarea');
		}, 0);
	});

	$scope.$on('elastic:resize', function(e, element, oldHeight, newHeight) {
		if (!element) return;
		if (!footerBar) return;
		var newFooterHeight = newHeight + 10;
		newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;

		footerBar.style.height = newFooterHeight + 'px';
		scroller.style.bottom = newFooterHeight + 'px';
	});

	$scope.message = '';
	// $scope.partnerAvatar = AppConfig.defaultAvatar;

	// show loading
	$ionicLoading.show({
		template: 'Loading...'
	});

	var loadDataErrorFunc = function() {
		$ionicLoading.hide();
		swalFac.error({
			text: 'Has error in loading data. Please try again.',
			title: 'Error!'
		});
	};

	$scope.messages = [];
	$scope.chat = undefined;

	async.parallel([
		// load partner
		function(callback) {
			$scope.partner = User.get({
				id: $stateParams.user
			}, function(rps) {
				callback();
			}, callback);
		}
	], function(err, results) {
		if (err) {
			// show error
			loadDataErrorFunc();
			return;
		}
		// update partner avatar
		// $scope.partnerAvatar = Utils.getUserAvatar($scope.partner);

		// load chat
		var loadingMore = false;
		var endLoadBefore = false;
		ChatFactory.loadPrivateChat({
			partner: $scope.partner._id
		}).then(function(rps) {
			var successLoad = function() {
				$ionicLoading.hide();
				// create socket for private chat
				socket = io(AppConfig.socketServer, {
					query: "token=" + AuthFactory.getToken()
				});

				var isFirstConnected = true;
				socket.on('connected', function(data) {
					console.log("Socket connected");

					socket.emit('private:create', {
						with: $scope.partner._id
					}, function(err, success) {
						if (err) {
							swalFac.error({
								text: err,
								title: 'Error!'
							});
						}
						if(!isFirstConnected){
							return ;
						}else{
							isFirstConnected = false;
						}

						/* events for receiving incoming events */
						// add event for new message come
						socket.on('private:newMessage', function(message) {
							console.log("received message");
							if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
								$scope.$apply(function() {
									$scope.messages.push(message);
								});
							}
							$timeout(function() {
								viewScroll.scrollBottom();
								txtInput.focus();
							});

							// mark read this message
							socket.emit('private:messageReceived',message._id);
						});

						/* dom events */
						var loadPreviousMessages = function(){
							if($scope.chat){
								var loadQuery = {
									chat: $scope.chat._id
								};
								if($scope.messages.length){
									loadQuery.beforeMessage = $scope.messages[0]._id;
								}
								ChatFactory.loadMessages(loadQuery).then(function(rps){
									if(!rps.data || !rps.data.length){
										endLoadBefore = true;
									}else{
										// prepend response messages before $scope.messages

										var scrollPosition = viewScroll.getScrollPosition();
										var scrollFrombottom = $('.messages-box .scroll').height() - scrollPosition.top;
										var messages = _.reverse(rps.data);
										messages.forEach(function(message){
											$scope.messages.unshift(message);
										});
										$timeout(function() {
											viewScroll.scrollTo(0, $('.messages-box .scroll').height() - scrollFrombottom);
										}, 0);

									}
									loadingMore = false;
								}, function(err){
									swalFac.error({
										text: 'Has error in load message. Please try again.',
										title: 'Error!'
									});
									loadingMore = false;
								});
							}
						};
						$scope.loadMore = function(){
							if(endLoadBefore){
								return ;
							}
							var distanceTopToloadBefore = 100;

							var scrollPosition = viewScroll.getScrollPosition();
							var distanTopToload = distanceTopToloadBefore*scrollPosition.zoom;
							if(!loadingMore && distanTopToload >= scrollPosition.top){
								loadingMore = true;

								loadPreviousMessages();
							}
						}

						// register events for messages
						$scope.sendMessage = function($event, form) {
							$event.preventDefault();

							// for test
							// alert("Begin send message");

							// show loading
							$scope.showPushMessageLoading = true;
							$timeout(function() {
								viewScroll.scrollBottom();
								txtInput.focus();
							}, 0);

							$scope.disableInput = true;
							socket.emit('private:message', $scope.message, function(err, message) {
								if (err) {
									$scope.showPushMessageLoading = false;
									$scope.disableInput = false;
									if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
										$scope.$apply();
									}
									swalFac.error({
										text: 'Has error in pushing your message. Please try again.',
										title: 'Error!'
									});
									return;
								}
								if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
									$scope.$apply(function() {
										$scope.messages.push(message);
										$timeout(function() {
											viewScroll.scrollBottom();
											txtInput.focus();
										}, 100);
										$scope.showPushMessageLoading = false;
										$scope.disableInput = false;
										$scope.message = null;
									});
								}
							});
						};
					});
				});
			}

			// if no chat found then do nothing
			// console.log("rps.data");
			// console.log(rps.data);
			if (!rps.data) {
				endLoadBefore = true;
				$scope.isNewChat = true;
				successLoad();
				return;
			}

			// save chat
			$scope.chat = rps.data;

			// update partnerLastSeen
			var partner = _.find($scope.chat.members, function(member) {
				return member.user == $scope.partner._id;
			});
			if (partner && partner.lastSeen) {
				$scope.partnerLastSeen = moment(partner.lastSeen).format('hh[h]:mm MM/DD/YYYY');
			}

			// load messages
			ChatFactory.loadMessages({
				chat: $scope.chat._id
			}).then(function(rps) {
				// save messages
				$scope.messages = rps.data;
				$timeout(function() {
					viewScroll.scrollBottom();
				});
				successLoad();
			}, function(err) {
				// show error
				loadDataErrorFunc();
			});
		}, function(err) {
			// show error
			loadDataErrorFunc();
		});
	});
});
