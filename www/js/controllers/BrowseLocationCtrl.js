var app = angular.module('dressbook.controllers');

app.controller('BrowseLocationCtrl', function($scope, $window, $state, $q, $ionicLoading, $stateParams, Category, Color, Size, Brand, Item, AppConfig, AuthFactory, Utils, matchmedia, GAnalyticsHelper, $ionicPlatform, $timeout, $cordovaGeolocation, swalFac) {
  GAnalyticsHelper.trackView('List Item view');

  $scope.AppConfig = AppConfig;
  $scope.Utils = Utils;
  $scope.Item = Item;
  $scope.moment = $window.moment;
  $scope.radius = AppConfig.dfBrowseItemDistance;

  var query = function(){
    $ionicLoading.show();
    var queryParams = {
      sold: false,
      lat: $scope.map.center.latitude,
      lng: $scope.map.center.longitude,
      radius: $scope.radius
    };

    var user = AuthFactory.getUserFromToken();
    if (user) {
      queryParams.ormitProviders = [user.id]
    }

    $scope.items = Item.query(queryParams);
    $scope.items.$promise.then(function(rps) {
      // build windows
      $scope.items.forEach(function(item){
        item.windowOpts = {scrollwheel:false};
      });
      $ionicLoading.hide();
    }, function(err) {
      $ionicLoading.hide();
      swalFac.error({
        text: 'Has error in loading items. Please try again.',
        title: 'Error!'
      });
    });
  };

  var initCenter = {
    latitude: 51.500083,
    longitude: -0.126182
  }; // london

  var input = document.getElementById('center-loc-search');
  $scope.map = {
    zoom:12,
    center: {
      latitude: initCenter.latitude,
      longitude: initCenter.longitude
    },
    events:{
      dragend: function(map){
        // refresh search box
        // $(input).val('');
        // make query
        $scope.map.center.latitude = map.getCenter().lat();
        $scope.map.center.longitude = map.getCenter().lng();
        query();
      }
    }
  };

  var getCurrentPositionSuccessHandle = function(position) {
    $ionicLoading.hide();
    $scope.map.center = {
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    }

    query();
  };

  var getCurrentPositionErrorHandle = function(err){
    $ionicLoading.hide();
    query();
    // var testRegex = new RegExp('Only secure origins are allowed','i');
    // if(err.code == 1 && testRegex.test(err.message)){
    //   query();
    // }else{
    //   query();
    //   swalFac.error({
    //     text: 'Please accept app get your current position.',
    //     title: 'Error!'
    //   });
    // }
  };

  var getCurrentPositionOtps = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  var getCurrentPosition = function(){
    if (ionic.Platform.isWebView()) {
      $cordovaGeolocation.getCurrentPosition(getCurrentPositionOtps).then(getCurrentPositionSuccessHandle, getCurrentPositionErrorHandle);
    } else if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(getCurrentPositionSuccessHandle, getCurrentPositionErrorHandle, getCurrentPositionOtps);
    }else{
      query();
    }
  }

  getCurrentPosition();

  $scope.setCenterAtCurrent = function($event){
    $event.preventDefault();

    getCurrentPosition();
    $(input).val('');
  }

  // add event for search box
  var searchBox = new google.maps.places.SearchBox(input);

  $timeout(function(){
    container = document.getElementsByClassName('pac-container');
    // disable ionic data tab
    angular.element(container).attr('data-tap-disabled', 'true');
  }, 500);

  // map.addListener('bounds_changed', function() {
  //   searchBox.setBounds(map.getBounds());
  // });

  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    // map.fitBounds(bounds);
    $scope.map.zoom = 12;

    // change meeting place to first place
    if(!$scope.$$phase && !$scope.$root.$$phase){
      $scope.$apply(function(){
        if(places[0].geometry.location){
          $scope.map.center.latitude = places[0].geometry.location.lat();
          $scope.map.center.longitude = places[0].geometry.location.lng();
        }else{
          $scope.map.center.latitude = bounds.getCenter().lat();
          $scope.map.center.longitude = bounds.getCenter().lng();
        }
      });
    }
  });

  $timeout(function(){
    $('.distance-'+AppConfig.dfBrowseItemDistance).addClass('active');
  });
  $scope.setDistance = function(event, distance){
    event.preventDefault();
    $('.distance').removeClass("active");
    $(event.currentTarget).addClass('active');
    $scope.radius = distance;
    query();
  };

  $ionicPlatform.ready(function() {
  });
});
