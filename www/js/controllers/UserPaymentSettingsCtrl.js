var app = angular.module('dressbook.controllers');

app.controller('UserPaymentSettingsCtrl', function($scope, $rootScope, $ionicPlatform, $window, $http, $timeout, $ionicLoading, $ionicPopup, User, AppConfig, AuthFactory, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('User payment settings view');

  $scope.forms = [];
  // $scope.cardInfoData = {};
  // $scope.errorSaveCard = null;
  // $scope.saveCardSuccess = null;
  $scope.AppConfig = AppConfig;

  $scope.connectUrl = 'https://connect.stripe.com/oauth/authorize?response_type=code&client_id=' + encodeURI(AppConfig.stripeConnectClientId) + '&scope=read_write&state=' + encodeURI(AuthFactory.getToken())

  // $scope.saveCard = function($event) {
  //   $event.preventDefault();
  //   $scope.errorSaveCard = null;
  //   $scope.saveCardSuccess = null;
  //
  //   // create card token
  //   Stripe.card.createToken($scope.cardInfoData, function(status, response) {
  //     if (response.error) {
  //       if (!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)) {
  //         $scope.$apply(function() {
  //           $scope.errorSaveCard = response.error.message;
  //         });
  //       }
  //       return;
  //     }
  //
  //     // sent data to server);
  //     $rootScope.main.user.saveCardInfo({
  //       cardToken: response.id,
  //       cardNumber: $scope.cardInfoData.number
  //     }).then(function(rps) {
  //       // update main user
  //       $rootScope.main.user = new User(rps);
  //       // save card success
  //       $scope.saveCardSuccess = true;
  //     }, function(err) {
  //       // save card success
  //       $scope.errorSaveCard = "Has error in saving data. Please try again.";
  //     });
  //   });
  // };

  $scope.reqConnectStripe = function($event) {
    $event.preventDefault();
    var ref = window.open($scope.connectUrl, '_blank', 'location=no');
    ref.addEventListener('loadstart', function(event) {
      if (event.url.match("fake/close")) {
        ref.close();
        // reload stripe data
        $rootScope.main.user.refreshStripe();
      }
    });
  }
});
