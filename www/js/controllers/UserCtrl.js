var app = angular.module('dressbook.controllers');

app.controller('UserCtrl', function($scope, $rootScope, $ionicPlatform, $window, $http, $timeout, $ionicLoading, User, AppConfig, AuthFactory, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('User page view');

	$scope.forms = {};

	$scope.saveGeneralInfoSuccess = false;
	$scope.saveEmailSuccess = false;
	$scope.savePasswordSuccess = false;

	$scope.errorSaveGeneralInfoMessage = undefined;
	$scope.errorSaveEmailMessage = undefined;
	$scope.errorSavePasswordMessage = undefined;

	$scope.editGeneralInfo = false;
	$scope.editEmail = false;
	$scope.editPassword = false;

	$scope.isShowProfile = true;
	$scope.isShowChangeAvatar = false;
	$scope.isShowTransactions = false;

	$scope.showProfile = function() {
		$scope.isShowProfile = true;
		$scope.isShowChangeAvatar = false;
		$scope.isShowTransactions = false;
	};

	$scope.showChangeAvatar = function() {
		$scope.isShowProfile = false;
		$scope.isShowChangeAvatar = true;
		$scope.isShowTransactions = false;
	};

	$scope.syncData = {
		isShowGeneralInfo: false,
		isShowEmail: false,
		isShowPassword: false
	};

	$scope.user = undefined;
	$scope.userAvatar = 'img/no_avatar.gif';
	var updateUserAvatar = function() {
		$scope.userAvatar = $scope.user.imageUrl ? AppConfig.server + AppConfig.profilePicturePath + '/' + $scope.user.imageUrl : $scope.userAvatar;
	}

	var parseGeneralInforFormData = function() {
		$scope.generalInforData = _.pick($scope.user, ['name', 'username', 'address', 'postcode']);
	};
	var parseEmailFormData = function() {
		$scope.emailData = _.pick($scope.user, ['email']);
	};
	var parsePasswordFormData = function() {
		$scope.passwordData = {};
	};

	// show loading
	$ionicLoading.show({
		template: 'Loading...'
	});

	// get user infor
	$scope.user = User.get({
		id: AuthFactory.getUserFromToken().id
	}, function(user) {
		updateUserAvatar();
		parseGeneralInforFormData();
		parseEmailFormData();
		parsePasswordFormData();

		// save user to main
		$rootScope.main.user = $scope.user;

		async.parallel([
			// count user items
			function(callback) {
				$scope.user.countItems().then(function(res) {
					callback(null, res.data.count);
				}, callback);
			},
			// count items liked
			function(callback) {
				$scope.user.countItemsLiked().then(function(res) {
					callback(null, res.data.count);
				}, callback);
			}
		], function(err, results) {
			// hide loading
			$ionicLoading.hide();
			if (err) {
				return swalFac.error({
					text: 'Has error in getting data. Please try again.',
					title: 'Error!'
				});
			};

			$scope.countItems = results[0];
			$scope.countItemsLiked = results[1];
		});
	});

	$scope.openEditGeneralInfo = function($event) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		$scope.syncData.isShowGeneralInfo = true;
		$scope.editGeneralInfo = true;
		$scope.saveGeneralInfoSuccess = false;
		$scope.errorSaveGeneralInfoMessage = undefined;
	};

	$scope.openEditEmail = function($event) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		$scope.syncData.isShowEmail = true;
		$scope.editEmail = true;
		$scope.saveEmailSuccess = false;
		$scope.errorSaveEmailMessage = undefined;
	};

	$scope.$watch('syncData.isShowPassword', function(newValue) {
		if (newValue) {
			$scope.openEditPassword();
		}
	});
	$scope.openEditPassword = function($event) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		$scope.syncData.isShowPassword = true;
		$scope.editPassword = true;
		$scope.savePasswordSuccess = false;
		$scope.errorSavePasswordMessage = undefined;
	};

	$scope.closeEditGeneralInfo = function($event, isShowGeneralInfo) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		if (_.isBoolean(isShowGeneralInfo)) {
			$scope.syncData.isShowGeneralInfo = isShowGeneralInfo;
		} else {
			$scope.syncData.isShowGeneralInfo = false;
		}
		$scope.editGeneralInfo = false;
		// $scope.saveGeneralInfoSuccess = false;
		$scope.errorSaveGeneralInfoMessage = undefined;

		// reset form
		parseGeneralInforFormData();
		$scope.forms.generalInfoFrm.$setPristine();
	};

	$scope.closeEditEmail = function($event, isShowEmail) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		if (_.isBoolean(isShowEmail)) {
			$scope.syncData.isShowEmail = isShowEmail;
		} else {
			$scope.syncData.isShowEmail = false;
		}
		$scope.editEmail = false;
		// $scope.saveEmailSuccess = false;
		$scope.errorSaveEmailMessage = undefined;

		// reset form
		parseEmailFormData();
		$scope.forms.emailFrm.$setPristine();
	};

	$scope.closeEditPassword = function($event, isShowPassword) {
		if ($event) {
			$event.preventDefault();
			$event.stopPropagation();
		}

		if (_.isBoolean(isShowPassword)) {
			$scope.syncData.isShowPassword = isShowPassword;
		} else {
			$scope.syncData.isShowPassword = false;
		}

		$scope.editPassword = false;
		// $scope.savePasswordSuccess = false;
		$scope.errorSavePasswordMessage = undefined;


		// reset form
		parsePasswordFormData();
		$scope.forms.passwordFrm.$setPristine();
	};

	$scope.saveGeneralInfo = function($event, form) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.saveGeneralInfoSuccess = false;
		$scope.errorSaveGeneralInfoMessage = undefined;

		// show loading
		$ionicLoading.show({
			template: 'Saving...'
		});

    $scope.user.saveGeneralInfo($scope.generalInforData).then(function(userRps, rps) {
      $scope.user = new User(userRps.data);

			$ionicLoading.hide();

			$rootScope.main.user = _.merge($rootScope.main.user, _.pick(userRps.data, ['postcode', 'lat', 'lng', 'name', 'address']));

			$scope.saveGeneralInfoSuccess = true;
			$scope.closeEditGeneralInfo(null, true);
		}, function(data) {
			$ionicLoading.hide();
			$scope.errorSaveGeneralInfoMessage = data.error || "Has error in processing. Please try again.";
		});
	};

	$scope.saveEmail = function($event, form) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.saveEmailSuccess = false;
		$scope.errorSaveEmailMessage = undefined;

		// show loading
		$ionicLoading.show({
			template: 'Saving...'
		});

    $scope.user.saveEmail($scope.emailData).then(function(userRps, rps) {
      $scope.user = new User(userRps.data);

			$ionicLoading.hide();

			$rootScope.main.user = _.merge($rootScope.main.user, _.pick(userRps.data, ['email']));

			$scope.saveEmailSuccess = true;
			$scope.closeEditEmail(null, true);
		}, function(data) {
			$ionicLoading.hide();
			$scope.errorSaveEmailMessage = data.error || "Has error in processing. Please try again.";
		});
	};

	$scope.savePassword = function($event, form) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.savePasswordSuccess = false;
		$scope.errorSavePasswordMessage = undefined;

		// show loading
		$ionicLoading.show({
			template: 'Saving...'
		});

    $scope.user.savePassword($scope.passwordData).then(function(userRps, rps) {
			$ionicLoading.hide();
			$scope.savePasswordSuccess = true;
			$scope.closeEditPassword(null, true);
		}, function(data) {
			$ionicLoading.hide();
			$scope.errorSavePasswordMessage = data.error || "Has error in processing. Please try again.";
		});
	};

	/* for avatar upload */
	$scope.imageToCrop = null;
	$scope.croppedImage = null;
	$scope.avatarValid = false;


	var vanilla;
	$ionicPlatform.ready(function() {

		$scope.browseData = {
			fromGalleryOptions: {
				// quantity:80,
				// targetWidth:200
			},
			fromCameraOptions: {
				// quantity:80,
				// targetWidth:200
			},
			doneCallback: function(err, file) {
				if (err) {
					swalFac.error({
						text: 'Has error in processing image. Please try again.',
						title: 'Error!'
					});
					return;
				}

				if(!vanilla){
					vanilla = new Croppie(document.getElementById('image-editor'), {
					    viewport: { width: 200, height: 200 },
					    boundary: { width: 300, height: 300 },
					    mouseWheelZoom:false
					});
				}

				vanilla.bind({
          url:file.fileUrl
        });
				$scope.avatarValid = true;
			}
		};
	});

	$scope.saveAvatar = function($event) {
		$event.preventDefault();
		// show loading
		$ionicLoading.show({
			template: 'Uploading...'
		});

		vanilla.result('canvas').then(function(data) {
			// send data to server
			$http.post(AppConfig.server + '/api/user/avatar', {
				data: data
			}).then(function(rps) {
				$scope.user = _.merge($scope.user, rps.data);
				$rootScope.main.user = _.merge($rootScope.main.user, rps.data);

				updateUserAvatar();

				$ionicLoading.hide();
				swalFac.success({
					text: 'Your avatar has been changed.',
					title: 'Success!'
				});
			}, function(err) {
				$ionicLoading.hide();
				swalFac.error({
					text: 'Has error in uploading image. Please try again.',
					title: 'Error!'
				});
			});
		});
	}
});
