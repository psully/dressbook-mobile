var app = angular.module('dressbook.controllers');

app.controller('UserNotificationsCtrl', function($scope, $state, $ionicPopup, $ionicLoading, $timeout, NotificationFactory, AuthFactory, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('User notifications view');
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
});
