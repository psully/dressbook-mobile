var app = angular.module('dressbook.controllers');

app.controller('SignupCtrl', function($rootScope, $scope, $state, $ionicLoading, $timeout, AuthFactory, UserFactory, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Signup view');
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.signupData = {};
  if (ionic.Platform.isWebView()) {
      $scope.signupData.platform = device.platform.toLowerCase();
  }

  $scope.closeConfirm = function(){
    swalFac.close();
  }

  // Perform the login action when the user submits the login form
  $scope.doSignup = function($event, isValid) {
    $event.preventDefault();
    GAnalyticsHelper.trackEvent('Authentication', 'Do signup');
    if (isValid) {
      // show confirm signup
      swalFac.confirm({
        title: 'Confirm register',
        html: 'By registering with Dressbook, you accept the <a ui-sref="app.TandC" ng-click="closeConfirm($event)">Terms & Conditions</a> and are at least 18 years old or have permission from a parent',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Cancel',
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        scope: $scope
      }).then(function() {
        // $ionicLoading.show({
        //   template: 'Signup...'
        // });
        UserFactory.create($scope.signupData)
          .success(function(data) {

            AuthFactory.loginUserData(data);
            GAnalyticsHelper.trackEvent('Authentication', 'Signup success');
            $state.go("app.listItems", {}, {
              reload: true
            });

            // $ionicLoading.hide();
            swalFac.close();
          })
          .error(function(data) {
            GAnalyticsHelper.trackEvent('Authentication', 'Signup failure');
            swalFac.error({
              title: "Signup Error!",
              text: data.error
            });
          });
      }, function(){
        console.log("cancel");
      });
    } else {
      // do nothing
    }
  };
});
