var app = angular.module('dressbook.controllers');

app.controller('BrowseCategoriesCtrl', function($rootScope, $scope, $state, $stateParams, $ionicModal, $timeout, $ionicLoading, $ionicPopup, Category, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('Browser Category view');

  $scope.isLoading = true;
  
  $scope.current = $stateParams.current;
  $scope.keyword = $stateParams.keyword;

  $scope.getSpaceChars = function(number) {
    var result = '';
    for (var i = 0; i < number; i++) {
      result += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    }
    return result;
  }
  $scope.categories = Category.query(function() {
    $scope.isLoading = false;
    // chec have subs
    _.forEach($scope.categories, function(category) {
      category.hasSubs = _.find($scope.categories, function(catCheck) {
        return catCheck.lft > category.lft && catCheck.rgt < category.rgt;
      }) != undefined;
    });
    // build categories tree
    // $scope.categories = Category.buildTree($scope.categories);

  }, function(err) {
    $scope.isLoading = false;
    $scope.errorMessage = 'Has error in load categories.';
  });
});
