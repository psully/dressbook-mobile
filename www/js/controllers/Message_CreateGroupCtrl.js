var app = angular.module('dressbook.controllers');

app.controller('Message_CreateGroupCtrl', function($scope, $state, $ionicScrollDelegate, $window, $http,  $ionicLoading, $ionicPopup, User, AppConfig, AuthFactory, ChatFactory, Utils, Chat, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('Create group view');

  $scope.syncData = {
    newGroup:{
      checkMembers:undefined
    }
  };

  $scope.membersAdded = [];

  var checkMembers = function(){
      $scope.syncData.newGroup.checkMembers = $scope.membersAdded.length?'1':undefined;
  };

  $scope.syncData.addToGroup = function(member){
    $scope.membersAdded.push(member);
  };

  $scope.removeToGroup = function($event, index){
    $event.preventDefault();
    // remove from members added
    var member = $scope.membersAdded[index];
    $scope.membersAdded.splice(index,1);
    checkMembers();
    $scope.syncData.removeToGroup(member);
  };

  $scope.reqCreateGroup = function($event, isValid){
    $event.preventDefault();
    if(isValid){
      var chat = new Chat({
        name:$scope.syncData.newGroup.name,
        type:'group',
        members:_.map($scope.membersAdded, function(member){
          return member.user._id;
        })
      });

      chat.$save(function(){
        $state.go('app.message_chatgroup',{chat:chat._id});
      });
    }
  };
});
