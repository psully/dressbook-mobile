var app = angular.module('dressbook.controllers');

app.controller('AddItemCtrl', function($rootScope, $scope, $state, $q, $ionicPlatform, $ionicModal, $ionicLoading, $timeout, $cordovaGeolocation, Category, Color, Size, Brand, Item, AppConfig, AuthFactory, uiGmapGoogleMapApi, uiGmapIsReady, UploadFactory,   GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Add item view');

  $scope.$on( /*'$destroy'*/ '$ionicView.leave', function(event) {
		if($scope.viewMeetingPlaceModal){
      $scope.viewMeetingPlaceModal.remove().then(function(){
        $scope.viewMeetingPlaceModal = null;
      });
    }
	});

  $scope.forms = {};

  $scope.AppConfig = AppConfig;

  $scope.user = $rootScope.main.user;

  $scope.item = new Item({
    images:[],
    meetingPlaceAddress: null,
    meetingPlaceLoc: null,
    meetingPlaceRadius: 10000,
    can_borrow:true
  });
  $scope.meetingPlaceLoc = null;
  $scope.$watch('meetingPlaceLoc', function(newVal, oldVal){
    if(!newVal){
      $scope.item.meetingPlaceLoc = null;
      return ;
    }

    $scope.item.meetingPlaceLoc = [newVal.longitude, newVal.latitude];
  }, true);

  $scope.hasUploadImage = false;
  $scope.isAllImagesError = false; // check if all images upload has error

  $scope.meetingPlaceCircle = {
    stroke: {
      color: '#08B21F',
      weight: 1,
      opacity: 0.5
    },
    fill: {
      color: '#08B21F',
      opacity: 0.2
    },
    geodesic: true, // optional: defaults to false
    draggable: false, // optional: defaults to false
    clickable: true, // optional: defaults to true
    editable: true, // optional: defaults to false
    visible: true, // optional: defaults to true
    control: {}
  };

  $scope.createItemSuccess = false;

  // init null values for itemImageFiles
  $scope.itemImageFiles = [null,null,null,null,null];
  var countItemImages = function(){
    return _.reject($scope.itemImageFiles, function(file){
      return !file;
    }).length;
  };

  var isValidImage = function(file){
    return file && file.uploadStatus == 1;
  };

  var countItemImagesValid = function(){
    return _.filter($scope.itemImageFiles, function(file){
      return isValidImage(file);
    }).length;
  };

  var countItemImagesError = function(){
    return _.filter($scope.itemImageFiles, function(file){
      return file && file.uploadStatus == 2;
    }).length;
  }

  $scope.imagesValid = false;
  var updateImageValidate = function() {
    var imagesLength = countItemImages();
    var imagesValidLength = countItemImagesValid();
    var imagesErrorLength = countItemImagesError();

    // check and disabled upload button
    if(imagesLength >= 5){
      $scope.uploadData.disabled = true;
    }else{
      $scope.uploadData.disabled = false;
    }

    // mark has upload image
    if (!$scope.hasUploadImage) {
      $scope.hasUploadImage = true;
    }

    // check images has been uploaded and build images data for item
    var isAllUploaded = true;
    $scope.item.images = [];
    $scope.itemImageFiles.forEach(function(file, index){
      if(file){
        if(!file.uploadStatus){
          isAllUploaded = false;
        }
        if(isValidImage(file)){
          if(!index){
            file.response.isMain = true;
          }
          $scope.item.images[index] = file.response;
        }
      }
    });

    // check main image validate
    $scope.isMainImageValidated = $scope.itemImageFiles[0];
    // $scope.isMainImageValidated = $scope.itemImageFiles[0] && $scope.itemImageFiles[0].uploadStatus == 1;

    if(isAllUploaded){
      // check if all images upload error
      if(imagesLength && imagesLength == imagesErrorLength){
        $scope.isAllImagesError = true;
      }else{
        $scope.isAllImagesError = false;
      }
    }

    // check for images valid
    $scope.imagesValid = isAllUploaded && !$scope.isAllImagesError && $scope.isMainImageValidated;
  };

  // add event for upload images
  $ionicPlatform.ready(function() {
    $scope.uploadData = {
      fromGalleryOptions: {
        server: AppConfig.server + '/api/itemPicture',
        browseOptions:{
            maximumImagesCount:5-countItemImages()
        },
        uploadOptions: {
          headers: {
            authorization: AuthFactory.getToken()
          }
        }
      },
      fromCameraOptions: {
        server: AppConfig.server + '/api/itemPicture',
        uploadOptions: {
          headers: {
            authorization: AuthFactory.getToken()
          }
        }
      },
      afterChooseMenuCallback: function(){
        $ionicLoading.show();
      },
      browseCallback: function(files, next){
        if(!files || !files.length){
          return next(false);
        }
        var nextFiles = [];
        for(var i=0;i<$scope.itemImageFiles.length && files.length;i++){
          if(!$scope.itemImageFiles[i]){
            var file = files.shift();
            file.uploadStatus = 0; // uploading
            $scope.itemImageFiles[i] = file;
            nextFiles.push(file);
          }
        }

        // async.each(nextFiles, function(file, callback){
        //   console.log("------------ file ------------");
        //   console.log(JSON.stringify(file));
        //   EXIF.getData(file.fileEntry, function() {
        //       console.log(EXIF.pretty(this));
        //   });
        // });

        // update image validate
        updateImageValidate();

        $ionicLoading.hide();

        if(nextFiles.length == 0){
          return next(false);
        }

        next(true, nextFiles);
      },
      beginUploadCallback_File: function(file){
        // register event for success upload or error upload
        // console.log("beginUploadCallback_File");
        // console.log(JSON.stringify(file));

        file.emitter.on('uploadSucceed', function(){
          var response = file.response;
          response.absUrl = AppConfig.server+response.url;
          if(response.error){
            file.uploadStatus = 2; // has error
            GAnalyticsHelper.trackEvent('Items', 'Upload images error', 'Add Item');
          }else{
            GAnalyticsHelper.trackEvent('Items', 'Upload images success', 'Add Item');
          }
          updateImageValidate();
        });
        file.emitter.on('uploadError', function(){
          GAnalyticsHelper.trackEvent('Items', 'Upload images error', 'Add Item');

          console.log('uploadError');
          updateImageValidate();
        });
      },
      // doneCallback: function(err, responses) {
      // }
    };

    var _currentPlaceHolderIndex = null;
    var _placeHolderUploadData = {
      fromGalleryOptions: {
        server: AppConfig.server + '/api/itemPicture',
        browseOptions:{
            maximumImagesCount:1
        },
        uploadOptions: $scope.uploadData.fromGalleryOptions.uploadOptions
      },
      fromCameraOptions: $scope.uploadData.fromCameraOptions,
      afterChooseMenuCallback: function(){
        $ionicLoading.show();
      },
      browseCallback: function(files, next){
        // console.log("browseCallback");
        // console.log(JSON.stringify(files));

        if(!files || !files.length){
          return next(false);
        }
        $scope.itemImageFiles[_currentPlaceHolderIndex] = files[0];

        // update image validate
        updateImageValidate();

        $ionicLoading.hide();

        next(true, files);
      },
      beginUploadCallback_File: function(file){
        // register event for success upload or error upload
        // console.log("beginUploadCallback_File");
        // console.log(JSON.stringify(file));

        console.log("----------Browse success");
        console.log(JSON.stringify(file));

        file.emitter.on('uploadSucceed', function(){
          var response = file.response;
          response.absUrl = AppConfig.server+response.url;
          if(response.error){
            file.uploadStatus = 2; // has error
            GAnalyticsHelper.trackEvent('Items', 'Upload images error', 'Add Item');
          }else{
            GAnalyticsHelper.trackEvent('Items', 'Upload images success', 'Add Item');
          }
          updateImageValidate();
        });
        file.emitter.on('uploadError', function(){
          GAnalyticsHelper.trackEvent('Items', 'Upload images error', 'Add Item');

          console.log('uploadError');
          updateImageValidate();
        });
      },
      // doneCallback: function(err, responses) {
      // }
    };

    var imageAtPlaceHolderUploader = new UploadFactory.multiUpload(_placeHolderUploadData);
    $scope.uploadImageAtPlaceHolder = function($event, index){
      $event.preventDefault();
      _currentPlaceHolderIndex = index;
      // browser and upload
      imageAtPlaceHolderUploader.browseUpload();
    }
  });

  $scope.removeImage = function(event, index) {
    event.preventDefault();
    event.stopPropagation();

    // arbot upload
    if($scope.itemImageFiles[index]){
      $scope.itemImageFiles[index].abortUpload();
      $scope.itemImageFiles[index] = null;
    }

    updateImageValidate();
  };

  $scope.getMinusChars = function(number) {
    var result = '';
    for (var i = 0; i < number; i++) {
      result += '---';
    }
    return result;
  }

  // load categories
  $scope.categories = Category.query(function() {
    // chec have subs
    _.forEach($scope.categories, function(category) {
      category.hasSubs = _.find($scope.categories, function(catCheck) {
        return catCheck.lft > category.lft && catCheck.rgt < category.rgt;
      }) != undefined;
    });
  });

  $scope.updateItemCategories = function() {
    $scope.itemCategoriesDirty = true;
    $scope.categoriesSelected = _.filter($scope.categories, function(category) {
      return category.selected;
    });
  };

  $scope.showCategories = function(event) {
    event.preventDefault();

    if (!$scope.viewCategoriesModal) {
      $ionicModal.fromTemplateUrl('js/templates/item/add-edit/view-categories.html', function($ionicModal) {
        $scope.viewCategoriesModal = $ionicModal;
        // show modal
        $scope.viewCategoriesModal.show();
      }, {
        scope: $scope
      });
    } else {
      $scope.viewCategoriesModal.show();
    }
  };


  // for colors
  $scope.colors = Color.query(function() {
    $scope.colors.push({
      _id: -1
    });
  });
  $scope.setSelectedColor = function(colorIndex) {
    var stop = false;
    $scope.colors.forEach(function(color, index) {
      if(color.selected){
        if(index == colorIndex){
          stop = true;
          return ;
        }else{
          color.selected = false;
        }
      }
    });
    if(stop){
      return ;
    }

    $scope.colors[colorIndex].selected = true;

    if(!$scope.item.color){
      $scope.item.color = {};
    }
    $scope.item.color.ref = $scope.colors[colorIndex]._id;
    $scope.item.color.name = $scope.colors[colorIndex].name;
  };

  $scope.showColors = function(event) {
    event.preventDefault();

    if (!$scope.viewColorsModal) {
      $ionicModal.fromTemplateUrl('js/templates/item/add-edit/view-colors.html', function($ionicModal) {
        $scope.viewColorsModal = $ionicModal;
        // show modal
        $scope.viewColorsModal.show();
      }, {
        scope: $scope
      });
    } else {
      $scope.viewColorsModal.show();
    }
  };

  //  for sizes
  $scope.sizes = Size.query(function() {
    $scope.sizes.push({
      _id: -1
    });
  });

  $scope.setSelectedSize = function(sizeIndex) {
    var stop = false;
    $scope.sizes.forEach(function(size, index) {
      if(size.selected){
        if(index == sizeIndex){
          stop = true;
          return ;
        }else{
          size.selected = false;
        }
      }
    });
    if(stop){
      return ;
    }

    $scope.sizes[sizeIndex].selected = true;

    if(!$scope.item.size){
      $scope.item.size = {};
    }
    $scope.item.size.ref = $scope.sizes[sizeIndex]._id;
    $scope.item.size.name = $scope.sizes[sizeIndex].name;
  };

  $scope.showSizes = function(event) {
    event.preventDefault();

    if (!$scope.viewSizesModal) {
      $ionicModal.fromTemplateUrl('js/templates/item/add-edit/view-sizes.html', function($ionicModal) {
        $scope.viewSizesModal = $ionicModal;
        // show modal
        $scope.viewSizesModal.show();
      }, {
        scope: $scope
      });
    } else {
      $scope.viewSizesModal.show();
    }
  };

  //  for brands
  $scope.brands = Brand.query(function() {
    $scope.brands.push({
      _id: -1
    });
  });

  $scope.setSelectedBrand = function(brandIndex) {
    var stop = false;
    $scope.brands.forEach(function(brand, index) {
      if(brand.selected){
        if(index == brandIndex){
          stop = true;
          return ;
        }else{
          brand.selected = false;
        }
      }
    });
    if(stop){
      return ;
    }

    $scope.brands[brandIndex].selected = true;
    if(!$scope.item.brand){
      $scope.item.brand = {};
    }
    $scope.item.brand.ref = $scope.brands[brandIndex]._id;
    $scope.item.brand.name = $scope.brands[brandIndex].name;
  };

  $scope.showBrands = function(event) {
    event.preventDefault();

    if (!$scope.viewBrandsModal) {
      $ionicModal.fromTemplateUrl('js/templates/item/add-edit/view-brands.html', function($ionicModal) {
        $scope.viewBrandsModal = $ionicModal;
        // show modal
        $scope.viewBrandsModal.show();
      }, {
        scope: $scope
      });
    } else {
      $scope.viewBrandsModal.show();
    }
  };

  // get current address
  $ionicLoading.show({
      template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
  });

  $scope.map = {
    center: {
      latitude: 45,
      longitude: -73
    },
    zoom: 10,
    control : {}
  };

  function getTownFromAddressComponents(addressComponents){
    var typeFields = ['postal_town','sublocality_level_1','administrative_area_level_2','administrative_area_level_1'];

    for(var i=0;i<typeFields.length;i++){
      for(var j=0;j<addressComponents.length;j++){
        for(var k=0;k<addressComponents[j].types.length;k++){
          if(addressComponents[j].types[k] == typeFields[i]){
            return addressComponents[j].long_name;
          }
        }
      }
    }
  }

  var geocodeLatLng = function(lat, lng) {
    // update map center
    $scope.map.center = {
      latitude: lat,
      longitude: lng
    };

    $scope.meetingPlaceLoc = {
      longitude: lng,
      latitude: lat
    };

    var latlng = {
      lat: lat,
      lng: lng
    };
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({
      'location': latlng
    }, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          if (!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)) {
            $scope.$apply(function() {
              $scope.item.meetingPlaceTown = getTownFromAddressComponents(results[0].address_components);
              $scope.item.meetingPlaceAddress = results[0].formatted_address;
            });
          }
        } else {

        }
      } else {

      }
    });
  };

  var getCurrentPositionSuccessHandle = function(position) {
    $ionicLoading.hide();
    /*if(!$scope.$$phase && !$scope.$root.$$phase){}*/
    if (!$scope.$$phase && !$scope.$root.$$phase) {
      $scope.$apply(function() {
        $scope.map = _.merge($scope.map, {
          center: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          },
          zoom: 10
        });
      });
    }
    geocodeLatLng(position.coords.latitude, position.coords.longitude);
  };

  var getCurrentPositionOtps = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };
  var getCurrentPositionErrorHandle = function(err){
    $ionicLoading.hide();
    console.log(err);
  };
  if (ionic.Platform.isWebView()) {
    $cordovaGeolocation.getCurrentPosition(getCurrentPositionOtps).then(getCurrentPositionSuccessHandle, getCurrentPositionErrorHandle);
  } else {
    navigator.geolocation.getCurrentPosition(getCurrentPositionSuccessHandle, getCurrentPositionErrorHandle, getCurrentPositionOtps);
  }

  $scope.showMeetingPlace = function(event) {
    event.preventDefault();

    if (!$scope.viewMeetingPlaceModal) {
      $ionicModal.fromTemplateUrl('js/templates/item/add-edit/view-meeting-place.html', function($ionicModal) {
        $scope.viewMeetingPlaceModal = $ionicModal;
        // show modal
        $scope.viewMeetingPlaceModal.show().then(function() {
          $scope.viewMeetingPlaceModalRendered = true;
          uiGmapIsReady.promise().then(function (maps) {
            var map = $scope.map.control.getGMap();

            var input = document.getElementById('meeting-place-address-search');
            var searchBox = new google.maps.places.SearchBox(input);

            $timeout(function(){
              container = document.getElementsByClassName('pac-container');
              // disable ionic data tab
              angular.element(container).attr('data-tap-disabled', 'true');
            }, 500);

            // map.addListener('bounds_changed', function() {
            //   searchBox.setBounds(map.getBounds());
            // });

            searchBox.addListener('places_changed', function() {
              var places = searchBox.getPlaces();
              $scope.item.meetingPlaceAddress = $(input).val();

              if (places.length == 0) {
                return;
              }

              // extract meetting place town
              var addressComponents = places[0].address_components;

              $scope.item.meetingPlaceTown = getTownFromAddressComponents(addressComponents);

              // For each place, get the icon, name and location.
              var bounds = new google.maps.LatLngBounds();
              places.forEach(function(place) {
                if (place.geometry.viewport) {
                  // Only geocodes have viewport.
                  bounds.union(place.geometry.viewport);
                } else {
                  bounds.extend(place.geometry.location);
                }
              });
              // map.fitBounds(bounds);
              $scope.map.zoom = 10;

              // change meeting place to first place
              if(!$scope.$$phase && !$scope.$root.$$phase){
                $scope.$apply(function(){
                  if(places[0].geometry.location){
                    $scope.map.center.latitude = places[0].geometry.location.lat();
                    $scope.map.center.longitude = places[0].geometry.location.lng();
                  }else{
                    $scope.map.center.latitude = bounds.getCenter().lat();
                    $scope.map.center.longitude = bounds.getCenter().lng();
                  }

                  $scope.meetingPlaceLoc = {
                    longitude: $scope.map.center.longitude,
                    latitude: $scope.map.center.latitude
                  };
                });
              }
            });
          });
        });
      }, {
        scope: $scope
      });
    } else {
      $scope.viewMeetingPlaceModal.show();
    }
  };

  $scope.createItem = function(event, form) {
    event.preventDefault();

    // show loading
    var loadingMd = $ionicLoading.show({
      template: 'Creating item...'
    });

    GAnalyticsHelper.trackEvent('Items', 'Add item', 'Add Item');

    // get categories
    $scope.item.categories = _.map($scope.categoriesSelected, function(category) {
      return category._id;
    });

    $scope.item.$save(function() {
      loadingMd.hide();
      $scope.createItemSuccess = true;

      GAnalyticsHelper.trackEvent('Items', 'Add item success', 'Add Item');
      if($scope.item.can_buy && !$scope.item.can_borrow){
        GAnalyticsHelper.trackEvent('Items', 'Choose sell rather than borrow', 'Add Item');
      }
      $timeout(function(){
        $state.go("app.listOwnItems", {}, {
          reload: true
        });
      }, 3000);
    }, function() {
      loadingMd.hide();
      GAnalyticsHelper.trackEvent('Items', 'Add item error', 'Add Item');
      swalFac.error({
        title: 'Error!',
        text: "Creating item has error."
      });
    });
  };
});
