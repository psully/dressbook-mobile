var app = angular.module('dressbook.controllers');
app.controller('Message_InboxCtrl', function($scope, $ionicPopover, AppConfig, User, AuthFactory, Utils, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('Message inbox view');

	$scope.userAvatar = AppConfig.defaultAvatar;

	$scope.syncData = {
		hasRightEditable:true
	};

	$scope.user = User.get({
		id: AuthFactory.getUserFromToken().id
	}, function(user) {
		// change user avatar
		$scope.userAvatar = Utils.getUserAvatar($scope.user);
	});

	$ionicPopover.fromTemplateUrl('js/templates/message/partials/popup_actions.html', {
		scope: $scope
	}).then(function(popover) {

		$scope.popover = popover;

		$scope.$on('$destroy', function(){
			$scope.popover.remove();
		});
	});

	$scope.openPopupMenu = function($event){
		$scope.popover.show($event);
	};
});
