var app = angular.module('dressbook.controllers');
app.controller('searchUserCtrl', function($rootScope, $scope, $ionicScrollDelegate, $window, $http, $timeout, $ionicLoading, User, AppConfig, AuthFactory, Utils, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Search User view');

  $scope.userId = AuthFactory.getUserFromToken().id;
  $scope.utils = Utils;

  $scope.searchKeyword = undefined;

  var currentPage = 0;
  var maxPagesLoad = 8;
  var endInfiniteLoad = false;
  var perPage = 25;

  $scope.moreDataCanBeLoaded = function(){
    return !endInfiniteLoad && currentPage <= maxPagesLoad;
  };

  $scope.users = [];
  $scope.showEmpty = false;

  $scope.$watch(function($scope){
    return $rootScope.main.user.friends;
  }, function(newValue, oldValue){
    $scope.users = _.map($scope.users, function(user){
      user.friendConnected = _.includes(newValue, user._id);
      return user;
    });
  });

  $scope.$watch(function($scope){
    return $rootScope.main.user.friendsRequesting;
  }, function(newValue, oldValue){
    $scope.users = _.map($scope.users, function(user){
      user.isFriendRequesting = _.includes(newValue, user._id);
      return user;
    });
  });

  $scope.addFriend = function(event, user) {
    event.preventDefault();

    swalFac.confirmSuccessWithAvatar({
      title: 'Confirm friend request',
      imageUrl: Utils.getUserAvatar(user),
      html: 'Are you sure want to become friend with <strong>'+user.username+'</strong>',
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      closeOnConfirm: false
    }).then(function(){
      // show loadding
      user.isAddFriendReqLoading = true;

      $scope.main.user.reqAddFriend(user._id).then(function(rps) {
        user.isAddFriendReqLoading = false;
        user.isFriendRequesting = true;
        $rootScope.main.user.friendsRequesting.push(user._id);

        swalFac.success({
          title: "Friend request send",
          html: 'Your friend request has been sent to <strong>'+user.username+'</strong>.'
        });
      }, function(err) {
        // show error
        user.isAddFriendReqLoading = false;
        swalFac.error({
          text: 'Has error in request add friend. Please try again.',
          title: 'Error!'
        });
      });
    }, function(){
      console.log("Cancel");
    });
  };

  var reloadUsers = function(){
    // reset state
    currentPage = 0;
    endInfiniteLoad = false;
    $scope.users = [];
    $scope.showEmpty = false;
    // begin search
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };

  $scope.loadMoreUsers = function(){
    User.list({
      page:++currentPage,
      perPage:perPage,
      keyword:$scope.searchKeyword,
      ormitUsers: [$scope.userId],
      noFriendsWith: $scope.userId
    }).then(function(rps){
      endInfiniteLoad = rps.data < perPage;

      // build avatar
      $scope.users = $scope.users.concat(_.map(rps.data, function(user){
        user.avatar = Utils.getUserAvatar(user);
        user.friendConnected = $rootScope.main.user.friends.indexOf(user._id) != -1;
        user.isFriendRequesting = $rootScope.main.user.friendsRequesting.indexOf(user._id) != -1;
        return user;
      }));

      // success
      if($scope.users.length == 0){
        $scope.showEmpty = true;
      }

      $scope.$broadcast('scroll.infiniteScrollComplete');
    }, function(err){
      swalFac.error({
        text: 'Has error in loadding data. Please try again.',
        title: 'Error!'
      });
    });
  }

  var typingTimeOut;
  var searchChanged = false;
  $scope.typingSearch = function($event){
    if(typingTimeOut){
      $timeout.cancel(typingTimeOut);
      typingTimeOut = undefined;
    }
    searchChanged = false;

    typingTimeOut = $timeout(function(){
      if(!searchChanged){
        reloadUsers();
        searchChanged = true;
      }
      typingTimeOut = undefined;
    },1000);
  };

  $scope.reqSearch = function($event){
    if(!searchChanged){
      reloadUsers();
      searchChanged = true;
    }
  };
});
