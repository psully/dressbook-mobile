var app = angular.module('dressbook.controllers');

app.controller('LoginCtrl', function($rootScope, $scope, $state, $ionicLoading, $timeout, $window, AuthFactory, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Login view');
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {
    rememberMe: true
  };
  // Perform the login action when the user submits the login form
  $scope.doLogin = function($event,isValid) {
    $event.preventDefault();
    if(isValid){
      $ionicLoading.show({
        template: 'Login...'
      });

      // add device data
      if (ionic.Platform.isWebView()) {
          $scope.loginData.platform = device.platform.toLowerCase();
      }
      if($rootScope.deviceData){
        $scope.loginData.device = $rootScope.deviceData.deviceToken;
      }
      // do login
      AuthFactory.login($scope.loginData)
        .then(function(user){
          $ionicLoading.hide();
          GAnalyticsHelper.trackEvent('Authentication', 'Login success');
          $state.go("app.listItems", {}, {reload: true});
        }, function(err){
          GAnalyticsHelper.trackEvent('Authentication', 'Login failure');
          $ionicLoading.hide();
          swalFac.error({
              title: 'Login Error',
              text: err
          });
        });
    }else{
      // do nothing
    }
  };
});
