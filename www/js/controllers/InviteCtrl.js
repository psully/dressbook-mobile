var app = angular.module('dressbook.controllers');

app.controller('InviteCtrl', function($scope, $http, $ionicLoading, AppConfig, AuthFactory, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('Invite friends view');

  $scope.senderEmail = AuthFactory.getLoginData().email;
  $scope.inviteFriendsData = {};
  $scope.inviteSuccess = false;
  $scope.errorMessage = false;

  $scope.invite = function($event, formValid) {
    $event.preventDefault();
    if (formValid) {
      $scope.inviteSuccess = false;
      $scope.errorMessage = false;

      // submit data to invite friend

      // show loading
      $ionicLoading.show({
        template: 'Sending...'
      });

      GAnalyticsHelper.trackEvent('Invites', 'Invite friend');

      $http.post(AppConfig.server + '/api/invite', $scope.inviteFriendsData)
        .success(function(data) {
          $ionicLoading.hide();
          $scope.inviteSuccess = true;
        })
        .error(function(data) {
          $ionicLoading.hide();
          $scope.errorMessage = data.error || "Has error in processing. Please try again.";
        });
    }
  }
});
