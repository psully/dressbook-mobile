var app = angular.module('dressbook.controllers');
moment.locale('en', {
	relativeTime: {
		future: "in %s",
		past: "%s ago",
		s: "%d sec",
		m: "a minute",
		mm: "%d minutes",
		h: "an hour",
		hh: "%d hours",
		d: "a day",
		dd: "%d days",
		M: "a month",
		MM: "%d months",
		y: "a year",
		yy: "%d years"
	}
});
app.controller('Message_ChatGroupCtrl', function($scope, $ionicPosition, $state, $stateParams, $ionicScrollDelegate, $ionicPopover, $ionicModal, $window, $http, $timeout, $ionicLoading, ChatsConnectedService, User, Chat, AppConfig, AuthFactory, ChatFactory, Utils, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Group chat view');
	GAnalyticsHelper.trackEvent('Chats', 'Use group chat','Group chat');

	$scope.groupAvatar = AppConfig.groupAvatar;
	$scope.userId = AuthFactory.getUserFromToken().id;
	$scope.utils = Utils;

	var socket = null;
	/*$state.get('app.message_chat').onExit = function(){
		if(socket){
			console.log("required disconnect");
        	socket.removeAllListeners();
			socket.disconnect();
			delete socket;
		}
	};*/

	$scope.$on( /*'$destroy'*/ '$ionicView.leave', function(event) {
		if (socket) {
			console.log("required disconnect");
			socket.removeAllListeners();
			socket.disconnect();
			delete socket;
		}
	});

	var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
	var footerBar; // gets set in $ionicView.enter
	var headerBar;
	var scroller;
	var txtInput;
	$scope.$on('$ionicView.enter', function() {
		$timeout(function() {
			footerBar = document.body.querySelector('.page-message-chat-group .bar-footer');
			headerBar = document.body.querySelector('.page-message-chat-group .bar-header');
			scroller = document.body.querySelector('.page-message-chat-group .scroll-content');
			txtInput = $('.page-message-chat-group .bar-footer textarea');
		}, 0);
	});

	$scope.$on('elastic:resize', function(e, element, oldHeight, newHeight) {
		if (!element) return;
		if (!footerBar) return;
		var newFooterHeight = newHeight + 10;
		newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;

		footerBar.style.height = newFooterHeight + 'px';
		scroller.style.bottom = newFooterHeight + 'px';
	});

	$scope.message = '';

	// show loading
	$ionicLoading.show({
		template: 'Loading...'
	});

	var loadDataErrorFunc = function() {
		$ionicLoading.hide();
		swalFac.error({
			text: 'Has error in loading data. Please try again.',
			title: 'Error!'
		});
	};

	$scope.messages = [];
	$scope.chat = undefined;

	var loadingMore = false;
	var endLoadBefore = false;

	async.parallel([
		// load chat
		function(callback) {
			if($stateParams.chat){
				$scope.chat = Chat.get({
					id: $stateParams.chat
				}, function(chat) {
					callback();
				}, callback);
			}else{
				callback();
			}
		},
		// load chat message
		function(callback) {
			if($stateParams.chat){
				// load messages
				ChatFactory.loadMessages({
					chat: $stateParams.chat
				}).then(function(rps) {
					// save messages
					$scope.messages = rps.data;
					$timeout(function() {
						viewScroll.scrollBottom();
					});
					if(!$scope.messages || !$scope.messages.length){
						endLoadBefore = true;
					}
					callback();
				}, callback);
			}else{
				callback();
			}
		}
	], function(err, results) {
		if (err) {
			// show error
			loadDataErrorFunc();
			return;
		}

		$ionicLoading.hide();

		// create socket for private chat
		socket = io(AppConfig.socketServer, {
			query: "token=" + AuthFactory.getToken()
		});

		var isFirstConnected = true;
		socket.on('connected', function(data) {
			console.log("Socket connected");

			socket.emit('group:create', {
				group: $scope.chat._id
			}, function(err, success) {
				if (err) {
					swalFac.error({
						text: err,
						title: 'Error!'
					});
				}

				if(!isFirstConnected){
					return ;
				}else{
					isFirstConnected = false;
				}

				/* events for receiving incoming events */
				// add event for new message come
				socket.on('group:newMessage', function(message) {
					console.log("received message");
					if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
						$scope.$apply(function() {
							$scope.messages.push(message);
						});
					}
					$timeout(function() {
						viewScroll.scrollBottom();
						txtInput.focus();
					});

					// mark read this message
					socket.emit('group:messageReceived',message._id);
				});

				socket.on('group:leave', function(chat, memberLeave){
					// add notification in chat box
					$scope.messages.push({isNotification:true, msg:memberLeave.name+' has been leaved from group.'})
					if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
						$scope.$apply(function() {
							$scope.chat.members = chat.members;
						});
					}
				});

				socket.on('group:addMembers', function(chat, newMembers){
					// console.log('res group:addMembers');
					// console.log(newMembers);
					var notifMsg = _.map(newMembers, function(newMember){
						return newMember.name;
					}).join(', ')+ ' have been added to group.';
					$scope.messages.push({isNotification:true, msg:notifMsg});

					if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
						$scope.$apply(function() {
							$scope.chat.members = chat.members;
						});
					}
				});

				var loadPreviousMessages = function(callback){
					var lasMessageId = $scope.messages && $scope.messages.length?$scope.messages[0]._id:
					ChatFactory.loadMessages({
						beforeMessage:$scope.messages[0]._id,
						chat: $scope.chat._id
					}).then(function(rps){

						if(!rps.data || !rps.data.length){
							endLoadBefore = true;
						}else{
							// prepend response messages before $scope.messages

							var scrollPosition = viewScroll.getScrollPosition();
							var scrollFrombottom = $('.messages-box .scroll').height() - scrollPosition.top;
							var messages = _.reverse(rps.data);
							messages.forEach(function(message){
								$scope.messages.unshift(message);
							});
							$timeout(function() {
								viewScroll.scrollTo(0, $('.messages-box .scroll').height() - scrollFrombottom);
							});

						}
						loadingMore = false;
					}, function(err){
						swalFac.error({
							text: 'Has error in load message. Please try again.',
							title: 'Error!'
						});
						loadingMore = false;
					});
				};
				$scope.loadMore = function(){
					if(endLoadBefore){
						return ;
					}
					var distanceTopToloadBefore = 100;

					var scrollPosition = viewScroll.getScrollPosition();
					var distanTopToload = distanceTopToloadBefore*scrollPosition.zoom;
					if(!loadingMore && distanTopToload >= scrollPosition.top){
						loadingMore = true;

						loadPreviousMessages();
					}
				}

				// register events for messages
				$scope.sendMessage = function($event, form) {
					$event.preventDefault();
					// show loading
					$scope.showPushMessageLoading = true;
					$timeout(function() {
						viewScroll.scrollBottom();
						txtInput.focus();
					});
					$scope.disableInput = true;
					socket.emit('group:message', $scope.message, function(err, message) {
						if (err) {
							if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
								$scope.$apply(function() {
									$scope.showPushMessageLoading = false;
									$scope.disableInput = false;
								});
							}
							swalFac.error({
								text: 'Has error in pushing your message. Please try again.',
								title: 'Error!'
							});
							return;
						}
						if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
							$scope.$apply(function() {
								$scope.messages.push(message);
								$scope.showPushMessageLoading = false;
								$scope.disableInput = false;
								$scope.message = null;
							});
							$timeout(function() {
								viewScroll.scrollBottom();
								txtInput.focus();
							});
						}
					});
				};

				$scope.openPopupActions = function($event){
					$ionicPopover.fromTemplateUrl('js/templates/message/partials/groupchat_popup_actions.html', {
						scope: $scope
					}).then(function(popupActions) {
						$scope.popupActions = popupActions;

						$scope.popupActions.show($event);

						$scope.$on('$destroy', function(){
							$scope.popupActions.remove();
						});
					});
				};

				$scope.leaveGroup = function($event){
					$event.preventDefault();

					socket.emit('group:leave');
					ChatsConnectedService.init();
					$state.go('app.message_inbox');
				};


				/* for add more member modal */
				$scope.addMoreMembers = function($event){
					$event.preventDefault();
					$scope.popupActions.hide();

					$ionicModal.fromTemplateUrl('js/templates/message/chatgroup_addmoremembers.html', function($ionicModal) {
							$scope.addMoreMembersModal = $ionicModal;

							// show modal
							$scope.addMoreMembersModal.show();
						}, {
							scope: $scope
						}
					);
				};


				$scope.addMoreMembersData = {
					newMembers:[]
				};
				$scope.addMoreMemberSyncData = {};

				$scope.$watch('chat.members', function(){
					$scope.addMoreMembersData.membersAdded = $scope.chat.members;
					$scope.addMoreMemberSyncData.ormitUsers = _.map($scope.chat.members, function(member){
						return member.user._id;
					});
				});


				$scope.addMoreMemberSyncData.addToGroup = function(member){
					$scope.addMoreMembersData.newMembers.push(member);
				};

				$scope.addMoreMembersData.removeToGroup = function($event, index){
					$event.preventDefault();
				    // remove from members added
				    var member = $scope.addMoreMembersData.newMembers[index];
				    $scope.addMoreMembersData.newMembers.splice(index,1);
				    $scope.addMoreMemberSyncData.removeToGroup(member);
				};

				$scope.addMoreMembersData.reqAddMembers = function($event, isValid) {
					$event.preventDefault();
					var membersNeedAdd = _.map($scope.addMoreMembersData.newMembers, function(member){
						return member.user._id;
					});
					//
					// console.log("req group:addMembers");
					// console.log(membersNeedAdd);
					if(membersNeedAdd.length){
						socket.emit('group:addMembers',membersNeedAdd);
					}

					$scope.addMoreMembersModal.remove();
				};
			});
		});
	});
});
