var app = angular.module('dressbook.controllers');

app.controller('UserTransactionCtrl',function($rootScope, $scope, $http, $ionicLoading, AppConfig, AuthFactory, SocketFactory, User, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('User transaction view');
	$scope.user = User.get({
		id: AuthFactory.getUserFromToken().id
	}, function(user) {
		// update main user
		$rootScope.main.user = $scope.user;

		$scope._ = _;

		async.parallel([
			// load transactions
			function(callback){
				$scope.user.transactions().then(function(rps){
					$scope.transactions = rps.data;
				}, callback);
			}
		], function(err, results){
			if(err){
				swalFac.error({
					text: 'Has error in upload images',
					title: 'Error!'
				});
				return ;
			}
		});


		SocketFactory.onConnected(function(isNewConnected){
			if(isNewConnected){
				console.log("User transaction socket connected");
				var socket = SocketFactory.overallSocket();

		        socket.on('transaction:newupdate', function(transaction){

		        	// find this transaction in scope transaction list
		        	var index = _.findIndex($scope.transactions, function(item){
		        		return item._id == transaction._id;
		        	});

		        	if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
						$scope.$apply(function(){
			        		if(!$scope.transactions){
				            	$scope.transactions = [];
				            }
				        	if(index != -1){
				        		$scope.transactions.splice(index, 1);
				        	}
				        	// push transaction at first
				            $scope.transactions.unshift(transaction);
						});
					}
		        });
			}
		});
	});
});
