var app = angular.module('dressbook.controllers');

app.controller('ListItemCtrl', function($scope, $rootScope, $window, $state, $q, $ionicLoading, $stateParams, $timeout, $ionicScrollDelegate, Category, Color, Size, Brand, Item, AppConfig, AuthFactory, Utils, matchmedia, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('List Item view');

  $scope.AppConfig = AppConfig;
  $scope.Utils = Utils;
  $scope.Item = Item;
  $scope.showEmpty = false;
  $scope.showItemsList = false;
  $scope.moment = $window.moment;
  $scope.showQuickButtons = false;

  matchmedia.on("(max-width: 480px)", function(mediaQueryList) {
    if (mediaQueryList.matches) {
      $scope.imageVersionApply = 'thumb';
    }
  });
  matchmedia.on("(min-width:481px) and (max-width: 768px)", function(mediaQueryList) {
    if (mediaQueryList.matches) {
      $scope.imageVersionApply = 'medium';
    }
  });
  matchmedia.on("(min-width:769px) and (max-width: 1080px)", function(mediaQueryList) {
    if (mediaQueryList.matches) {
      $scope.imageVersionApply = 'big';
    }
  });
  matchmedia.on("(min-width:1081px) and (max-width: 1500px)", function(mediaQueryList) {
    if (mediaQueryList.matches) {
      $scope.imageVersionApply = 'large';
    }
  });

  $scope.toogleQuickButtons = function($event) {
    $event.preventDefault();

    $scope.showQuickButtons = !$scope.showQuickButtons;
  }

  $scope.showFilter = function($event) {
    $event.preventDefault();

    $rootScope.toggleSearchBar();

    // forcus search input
    $timeout(function() {
      $('#search-item-ip')[0].focus();
    }, 500);
  }

  var currentPage = 0;
  var numPagesInStep = 5;
  var initData = function() {
    // reset data
    currentPage = 0;
    $scope.showCanLoadMore = false;
    $scope.canInfiniteLoad = true;
    $scope.items = [];

    $scope.showEmpty = false;
    $scope.showItemsList = true;
    $scope.showEndLoad = false;
  }
  initData();

  var queryParams = {
    sold: false,
    keyword: null,
    isDeleted: false,
    perPage: 10
  };

  if ($stateParams.cat) {
    $scope.cat = queryParams.cat = $stateParams.cat;
  }
  if ($stateParams.availableFrom) {
    $scope.availableFrom = queryParams.availableFrom = $stateParams.availableFrom;
  }
  if ($stateParams.availableTo) {
    $scope.availableTo = queryParams.availableTo = $stateParams.availableTo;
  }
  if($stateParams.keyword){
    $scope.itemSearchKeyword = queryParams.keyword = $stateParams.keyword;
    $rootScope.toggleSearchBar();
  }

  var user = AuthFactory.getUserFromToken();
  if (user) {
    queryParams.ormitProviders = [user.id]
  }

  // load category
  if ($stateParams.cat) {
    $scope.category = Category.get({
      id: $stateParams.cat
    });
  }

  var reqSearchItems = function(){
    GAnalyticsHelper.trackEvent('Items', 'Search');
    queryParams.keyword = $scope.itemSearchKeyword;
    // reset data
    initData();

    $timeout(function(){
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.resize();
    });
  };

  var firstRun = true;
  var timeoutToSearch;
  $scope.$watch('itemSearchKeyword', function(newValue, oldValue){
    if(firstRun){
      firstRun = false;
      return ;
    }
    if(timeoutToSearch){
      $timeout.cancel(timeoutToSearch);
      timeoutToSearch = null;
    }
    timeoutToSearch = $timeout(reqSearchItems, 1000);
  });

  $scope.clearQueryKeyword = function($event) {
    $event.preventDefault();
    // reset query
    $scope.itemSearchKeyword = "";
  };

  $scope.loadNextPaging = function($event) {
    $event.preventDefault();
    // $scope.items = [];
    $scope.showCanLoadMore = false;
    $scope.canInfiniteLoad = true;
  };

  $scope.loadMoreItems = function() {
    queryParams.page = currentPage + 1;
    async.parallel([
      // load items
      function(callback) {
        Item.query(queryParams).$promise.then(function(rps) {
          callback(null, rps);
        }, callback);
      },
      // check has next
      function(callback) {
        Item.checkHasNext(queryParams, callback);
      }
    ], function(err, results) {
      if (err) {
        swalFac.show({
          text: 'Has error in loading data. Please try again.',
          title: 'Error!'
        });
        return;
      }

      var currentQueryItems = results[0];
      var hasNext = results[1] == 1;
      if (currentPage == 0 && currentQueryItems.length == 0) {
        $scope.showEmpty = true;
        $scope.showItemsList = false;

        $scope.showCanLoadMore = false;
        $scope.canInfiniteLoad = false;
      } else {
        // increment current page
        currentPage++;

        $scope.showEmpty = false;
        $scope.showItemsList = true;
        $scope.items = $scope.items.concat(currentQueryItems);


        if (!hasNext) {
          $scope.showCanLoadMore = false;
          $scope.canInfiniteLoad = false;
          $scope.showEndLoad = true;
        } else {
          if (currentPage % numPagesInStep == 0) {
            $scope.showCanLoadMore = true;
            $scope.canInfiniteLoad = false;
          }
          if (currentPage % numPagesInStep == 1) {
            $scope.showCanLoadMore = false;
            $scope.canInfiniteLoad = true;
          }
        }
      }
      $timeout(function() {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.resize();
      });
    });
  };

  $scope.changeCategory = function($event){
    //  ui-sref="app.browseCategories"
    $event.preventDefault();
    var params = {};
    if($stateParams.cat){
      params.current = $stateParams.cat;
    }
    if($scope.itemSearchKeyword){
      params.keyword = $scope.itemSearchKeyword;
    }
    $state.go('app.browseCategories', params);
  }
});
