var app = angular.module('dressbook.controllers');

app.controller('UserScoreCtrl',function($rootScope, $scope, $http, $ionicLoading, AppConfig, AuthFactory, User, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('User score view');

	$scope.user = User.get({
		id: AuthFactory.getUserFromToken().id
	}, function(user) {
		// update main user
		$rootScope.main.user = $scope.user;

		async.parallel([
			// load scoreHistories
			function(callback){
				$scope.user.scoreHistories().then(function(rps){
					$scope.scoreHistories = rps.data;
					// console.log($scope.scoreHistories);
				}, callback);
			}
		], function(err, results){
			if(err){
				swalFac.error({
					text: 'Has error in upload images',
					title: 'Error!'
				});
				return ;
			}
		});
	});
});
