var app = angular.module('dressbook.controllers', ['dressbook.configs', 'dressbook.filters', 'dressbook.directives', 'dressbook.factories', 'dressbook.services', 'dressbook.models', 'ngCordova', 'ng-currency', 'ionic.ion.autoListDivider', 'monospaced.elastic', 'angularMoment', 'ionic-datepicker', 'uiGmapgoogle-maps', 'ionicLazyLoad', 'matchmedia-ng', 'swangular']);

app.controller('AppCtrl', function($rootScope, $scope, $state, $ionicModal, $timeout, $ionicLoading, $ionicBackdrop, $q, $cordovaSocialSharing, $ionicActionSheet, Utils, Item, AuthFactory, AppConfig, GAnalyticsHelper, swalFac) {
  $rootScope.toggleSearchBar = function() {
    $rootScope.hideNavBar = !$rootScope.hideNavBar;
  };

  $rootScope.showSettingsMenu = function($event) {
    $event.preventDefault();
    // show action sheet to choose sub menus
    $ionicActionSheet.show({
      buttons: [{
        text: '<i class="icon ion-ios-world-outline"></i> Profile'
      }, {
        text: '<i class="icon ion-ios-book-outline"></i> History'
      }, {
        text: '<i class="icon ion-ios-personadd-outline"></i> Invite a Friend'
      }, {
        text: '<i class="icon ion-ios-unlocked-outline"></i> Log Out'
      }],
      // destructiveText: 'Delete',
      titleText: 'Choose action',
      cancelText: 'Cancel',
      cancel: function() {},
      buttonClicked: function(index) {
        if (index == 0) {
          $state.go('app.profile');
        } else if (index == 1) {
          $state.go('app.userTransactions');
        } else if (index == 2) {
          $state.go('app.invite');
        } else if (index == 3) {
          $state.go('app.logout');
        }
        return true;
      }
    });
  };

  $rootScope.deleteItem = function(item) {
    GAnalyticsHelper.trackEvent('Items', 'Delete item', 'Item ' + item._id);
    var defer = $q.defer();
    swalFac.confirm({
      title: 'Confirm delete item',
      text: 'Are you sure you want to delete this item?',
      showLoaderOnConfirm: true,
      closeOnConfirm: false
    }).then(function(){
      console.log('You are sure.');
      // delete item
      Item.delete(item._id).then(function() {
        item.isDeleted = true;
        swalFac.success({
          title: 'Success!',
          text: 'Item has been delete successful'
        });
        defer.resolve();
        $rootScope.$emit('itemDeleted');
      }, function() {
        swalFac.error({
          title: 'Error!',
          text: 'Has error in delete item. Please try again.'
        });
        defer.reject();
      });
    }, function(){
      defer.reject();
    });

    return defer.promise;
  };

  $rootScope.likeItem = function(event, item, index) {
    event.preventDefault();
    if (item.liked) {
      GAnalyticsHelper.trackEvent('Items', 'Unlike item', 'Item ' + item._id);
    } else {
      GAnalyticsHelper.trackEvent('Items', 'Like item', 'Item ' + item._id);
    }
    var itemObj = item;
    if (!(item instanceof Item)) {
      itemObj = new Item(item);
    }

    itemObj.doLike().then(function(rps) {
      item.liked = rps.data.liked;
      item.countLiked = rps.data.countLiked;
    }, function(err) {
      swalFac.error({
        title: 'Error!',
        text: 'Has error in submit like. Please try later.'
      });
    });
  };

  $rootScope.showCommentItem = function(event, item, index) {
    event.preventDefault();

    var scope = $rootScope.$new();
    scope.item = item;
    var itemCommentsModal = $ionicModal.fromTemplateUrl('js/templates/item/comments-modal.html', {
      scope: scope
    }).then(function(modal) {
      scope.itemCommentsModal = modal;

      // add class for style
      modal.$el.addClass('item-comments-md');

      modal.show();
    });
  };

  $scope.showShareItem = function(event, item, index) {
    event.preventDefault();
    if (ionic.Platform.isWebView()) {
      var shareConfigs = {
        message: 'Hi. ' + $rootScope.main.user.name + " wants to share with you item \"" + item.name + "\". If you're not a dressbook user you can now download the app from the apple store " + AppConfig.linkOnAppleStore + " or play store " + AppConfig.linkOnPlayStore + ", or login to the app. From Dressbook Team.",
        subject: null,
        file: Item.mainImageLink(item, 'medium'),
        link: AppConfig.site
      };

      $cordovaSocialSharing
        .share(shareConfigs.message, shareConfigs.subject, shareConfigs.file, shareConfigs.link) // Share via native share sheet
        .then(function(result) {
          GAnalyticsHelper.trackEvent('Items', 'Share item', 'Item ' + item._id);
          // Success!
        }, function(err) {
          // An error occured. Show a message to the user
        });
    }
  };

  $scope.removeFriend = function(event, user) {
    event.preventDefault();

    swalFac.confirmErrorWithAvatar({
      title: 'Confirm unfriend',
      imageUrl: Utils.getUserAvatar(user),
      html: 'Are you sure unfriend with "' + user.username + '"?',
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      closeOnConfirm: false
    }).then(function() {
      $scope.main.user.unFriend(user._id).then(function() {
        // remove this friend in friend list of main user
        $scope.main.user.friends = _.reject($scope.main.user.friends, function(friendId) {
          return friendId == user._id;
        });

        swalFac.success({
          title: "Friend removed",
          html: '<strong>' + user.username + '</strong> has been removed from your friend list.'
        });
      }, function() {
        swalFac.error({
          title: 'Error!',
          text: 'Has error with remove friend. Please try later.'
        });
      });
    }, function(){
      console.log("Cancel");
    });
  };
});
