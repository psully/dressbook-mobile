var app = angular.module('dressbook.controllers');

app.controller('BrowseAvailableDateCtrl', function($rootScope, $scope, $state, $ionicModal, $timeout, $ionicLoading, $ionicPopup, Category, GAnalyticsHelper) {
  GAnalyticsHelper.trackView('Browser available date view');

  $scope.browseData = {};
  $scope.forms = {};

  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1)

  $scope.fromPicker = {
    titleLabel: 'Available from',
    templateType: 'popup',
    inputDate: new Date(),
    mondayFirst: true,
    from: yesterday,
    callback: function(val) {
      if (typeof(val) === 'undefined') {
        // do nothing
      } else {
        $scope.browseData.to = undefined;
        $scope.fromPicker.inputDate = new Date(val);
        $scope.browseData.from = moment(val).format("DD MMM YYYY");

        $scope.toPicker.from = new Date(val);
        $scope.toPicker.inputDate = $scope.fromPicker.inputDate;

        $scope.forms.browseFrm.from.$dirty = true;
        $scope.forms.browseFrm.from.$validate();
        $scope.forms.browseFrm.to.$validate();
      }
    }
  };

  $scope.toPicker = {
    titleLabel: 'Available to',
    templateType: 'popup',
    mondayFirst: true,
    from: yesterday,
    callback: function(val) {
      if (typeof(val) === 'undefined') {
        // do nothing
      } else {
        $scope.toPicker.inputDate = new Date(val);
        $scope.browseData.to = moment(val).format("DD MMM YYYY");

        $scope.forms.browseFrm.to.$dirty = true;
        $scope.forms.browseFrm.from.$validate();
        $scope.forms.browseFrm.to.$validate();
      }
    }
  };

  $scope.browseReq = function($event){
    $event.preventDefault();
    $state.go('app.listItems', {availableFrom:$scope.browseData.from, availableTo:$scope.browseData.to});
  }
});
