var app = angular.module('dressbook.controllers');

app.controller('ResetPasswordCtrl', function($scope, $state, $ionicLoading, $timeout, AuthFactory, GAnalyticsHelper, swalFac) {
  GAnalyticsHelper.trackView('Reset password view');
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.resetPasswordData = {};
  // Perform the login action when the user submits the login form
  $scope.doResetPassword = function($event,isValid) {
    $event.preventDefault();
    if(isValid){
      $ionicLoading.show({
        template: 'Requesting...'
      });
      GAnalyticsHelper.trackEvent('Authentication', 'Request reset password');

      // do login
      AuthFactory.resetPassword($scope.resetPasswordData.email)
        .then(function(user){
          $ionicLoading.hide();
          swalFac.success({
              title: 'Reset Password Success',
              text: "Your password hass been reset. Please check your email to see new password. Thank you."
          });
        }, function(rps){
          $ionicLoading.hide();
          var error = rps.data.error?rps.data.error:'Reset password has error. Please try again';
          swalFac.error({
              title: 'Reset Password Error',
              text: error
          });
        });
    }else{
      // do nothing
    }
  };
});
