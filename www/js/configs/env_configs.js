
var APP_ENV = 'production'; // development || production || test
var ENV_CONFIGS = {
  development:{
    SERVER:'http://192.168.56.1:3000', // the server host. Must be change follow your local
    SOCKET_SERVER:'http://192.168.56.1:8088', // the socket server host

    DOMAIN: 'www.dressbook.com.uk', // domain of app
    SITE: 'https://app.dressbook.co.uk',  // url of app
    MOBILE_SITE: 'http://localhost:8100',
    LINK_ON_APPLE_STORE: 'http://linkOnAppleStore.com', // link of app on apple store
    LINK_ON_PLAY_STORE: 'http://linkOnPlayStore.com', // link of app on play store
    GG_TRAKER_ID:'UA-75861405-1', // google tracker id
    GG_PROJECT_NUMBER:'768335942943', // sender id or project number id

    DF_BROWSE_ITEM_DISTANCE:5, // 5KM

    STRIPE_KEY:'pk_test_D0mSU2LrI6paBL26CFem13ow', // Publicable key from: https://dashboard.stripe.com/account/apikeys
    STRIPE_CONNECT_CLIENTID:'ca_82JjSLq4HESxVs2LqkQpRTdfSA205q4e' // client id from: https://dashboard.stripe.com/account/applications/settings
  },
  production:{
    SERVER:'https://app.dressbook.co.uk', // the server host. NOTE*** this doesn't have port(production doesn't use port)
    SOCKET_SERVER:'https://app.dressbook.co.uk', // the socket server host. NOTE*** this doesn't have port(production doesn't use port)

    DOMAIN: 'www.dressbook.com.uk', // domain of app
    SITE: 'https://app.dressbook.co.uk',  // url of app
    MOBILE_SITE: 'http://localhost:8100',
    LINK_ON_APPLE_STORE: 'http://linkOnAppleStore.com', // link of app on apple store
    LINK_ON_PLAY_STORE: 'http://linkOnPlayStore.com', // link of app on play store
    GG_TRAKER_ID:'UA-75861405-1', // google tracker id
    GG_PROJECT_NUMBER:'768335942943', // sender id or project number id

    DF_BROWSE_ITEM_DISTANCE:5, // 5KM

    STRIPE_KEY:'pk_live_lPocDifCCMzsBc0bFA57ohWK', // Publicable key from: https://dashboard.stripe.com/account/apikeys
    STRIPE_CONNECT_CLIENTID:'ca_82JjMbC0v9jhFMfBEqpp3mOix0hVZYNR' // client id from: https://dashboard.stripe.com/account/applications/settings
  },
  test:{
    SERVER:'https://test.dressbook.co.uk', // the server host. NOTE*** this doesn't have port(production doesn't use port)
    SOCKET_SERVER:'https://test.dressbook.co.uk', // the socket server host. NOTE*** this doesn't have port(production doesn't use port)

    // SERVER:'http://127.0.0.1:8081', // the server host. Must be change follow your local
    // SOCKET_SERVER:'http://127.0.0.1:8088', // the socket server host

    DOMAIN: 'www.dressbook.com.uk', // domain of app
    SITE: 'https://test.dressbook.co.uk',  // url of app
    LINK_ON_APPLE_STORE: 'http://linkOnAppleStore.com', // link of app on apple store
    LINK_ON_PLAY_STORE: 'http://linkOnPlayStore.com', // link of app on play store
    GG_TRAKER_ID:'UA-79402423-2', // google tracker id
    GG_PROJECT_NUMBER:'768335942943', // sender id or project number id

    DF_BROWSE_ITEM_DISTANCE:5, // 5KM

    STRIPE_KEY:'pk_test_sJz4N79UHLcOXAuiFGknh2zu', // Publicable key from: https://dashboard.stripe.com/account/apikeys
    STRIPE_CONNECT_CLIENTID:'ca_8dxFdKJcLA3s13bXgwa1rePFJtvyl01K' // client id from: https://dashboard.stripe.com/account/applications/settings
  }
}
var APP_ENV_CONFIGS = ENV_CONFIGS[APP_ENV];
