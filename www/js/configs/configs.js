var app = angular.module('dressbook.configs');
app.factory('AppConfig', function() {
  var configsMap = {
    general: {
      domain: APP_ENV_CONFIGS.domain,
      site: APP_ENV_CONFIGS.SITE,

      mobileSite: APP_ENV_CONFIGS.MOBILE_SITE,

      linkOnAppleStore: APP_ENV_CONFIGS.LINK_ON_APPLE_STORE,
      linkOnPlayStore: APP_ENV_CONFIGS.LINK_ON_PLAY_STORE,

      ggTrakerId: APP_ENV_CONFIGS.GG_TRAKER_ID,
      ggProjectNumber: APP_ENV_CONFIGS.GG_PROJECT_NUMBER,

      server: APP_ENV_CONFIGS.SERVER,
      socketServer: APP_ENV_CONFIGS.SOCKET_SERVER,

      stripeKey: APP_ENV_CONFIGS.STRIPE_KEY,
      stripeConnectClientId: APP_ENV_CONFIGS.STRIPE_CONNECT_CLIENTID,

      dfBrowseItemDistance: APP_ENV_CONFIGS.DF_BROWSE_ITEM_DISTANCE,

      defaultAvatar: 'img/no_avatar.gif',
      groupAvatar: 'img/user-group-128.png',
      publicTempPath: '/uploads/tmp'
    },
    development: { // config for dev environment
	    itemImagesPath:'/uploads/images/item', // path items images at local
	    colorImagesPath:'/uploads/images/normal', // path colors images at local
	    profilePicturePath:'/uploads/images/profile_avatars' // profile picture at local
    },
    production: { // config for production environment
      itemImagesPath: '/cached/images/item', // from public folder
	    colorImagesPath:'/cached/images/normal', // path colors images at local
	    profilePicturePath:'/cached/images/profile_avatars' // profile picture at local
    },
    test: { // config for production environment
      itemImagesPath: '/cached/images/item', // from public folder
	    colorImagesPath:'/cached/images/normal', // path colors images at local
	    profilePicturePath:'/cached/images/profile_avatars' // profile picture at local
    }
  }
  return _.merge({}, configsMap.general, configsMap[APP_ENV]);
});
