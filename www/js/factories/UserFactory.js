var app = angular.module('dressbook.factories');

app.factory("UserFactory",function($http, $rootScope,$localStorage,$q, AppConfig){
	// create user
	function create(userData){
		return $http.post(AppConfig.server+'/api/user', userData);
	};
	
	return {
		create:create
	};
});