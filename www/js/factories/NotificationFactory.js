var app = angular.module('dressbook.factories');

app.factory("NotificationFactory", function($http, $rootScope, $q, SocketFactory, Notification, AppConfig, AuthFactory) {
  var emitter;
  var getSocketEmitter = function() {
    return emitter;
  }

  // create new socket emitter
  var resetEmitter = function() {
    emitter = new EventEmitter();
  };

  // create socket emitter
  resetEmitter();

  var notifications = [];
  var countNews = 0;
  var page = 1;
  var perPage = 20;
  var fullLoaded = false;

  var getCountNews = function() {
    return countNews;
  };
  var setCountNews = function(val) {
    countNews = val;
    emitter.trigger('countNewsChanged', [countNews]);
  };
  var getNotifications = function() {
    return notifications;
  }

  var isFullLoaded = function(){
    return fullLoaded;
  }

  // connect socket
  SocketFactory.onConnected(function(isNewConnected) {
    console.log("Notification factor socket");
    console.log(isNewConnected);
    if (isNewConnected) {
      console.log("Notification factory socket connected");
      var socket = SocketFactory.overallSocket();

      // new notification
      socket.on('notification', function(notification) {
        console.log('new notification come');
        notifications.unshift(notification);
        countNews++;
        emitter.trigger('notification', [null, notification]);
      });

      // delete notification
      socket.on('deleteNotification', function(notificationId){
        var indexRemove = _.findIndex(notifications, function(notification){
          return notification._id == notificationId;
        });
        if(indexRemove != -1){
          notifications.splice(indexRemove,1);
          emitter.trigger('notification');
        }
      });
    }
  });

  // function for load chatters list
  var loadNext = function() {
    var deferred = $q.defer();
    if (fullLoaded) {
      deferred.resolve();
      emitter.trigger('loadNext', [null,[], notifications]);
    } else {
      var after;
      if(notifications.length){
        after = notifications[notifications.length -1]._id;
      }
      var params = {
        after: after,
        perPage: perPage
      }
      Notification.query(params).$promise.then(function(nextNotifications) {
        if(nextNotifications){
          if (nextNotifications.length < perPage) {
            fullLoaded = true;
          }
          if(nextNotifications.length){
            notifications = notifications.concat(nextNotifications);
          }
        }else{
          fullLoaded = true;
        }

        deferred.resolve(notifications);
        emitter.trigger('loadNext', [null, nextNotifications, notifications]);
      }, function(rps) {
        deferred.reject(rps);
        emitter.trigger('loadNext', [rps]);
      });
    }

    return deferred.promise;
  };

  var loadCountNews = function() {
    return Notification.countNews();
  };


  var markRead = function(changeHasSeenLocal) {
    if(!changeHasSeenLocal){
      changeHasSeenLocal = false;
    }
    if(changeHasSeenLocal){
      _.each(notifications, function(notification){
        notification.hasSeen = true;
      });
    }
    return $http.post(AppConfig.server + '/api/notification/own/markRead');
  };

  var init = function(callback) {
    fullLoaded = false;
    notifications = [];
    countNews = 0;
    resetEmitter();

    loadCountNews().then(function(rps) {
      countNews = rps.data.count;
      var data = {countNews:countNews};
      if (callback) {
        callback(null, data);
      }
      emitter.trigger('init', [null, data]);
    }, function(rps){
      if (callback) {
        callback(rps);
      }
    });
  };

  return {
    emitter: getSocketEmitter,
    notifications: getNotifications,
    countNews: getCountNews,
    setCountNews: setCountNews,
    init: init,
    loadCountNews: loadCountNews,
    markRead: markRead,
    loadNext:loadNext,
    isFullLoaded:isFullLoaded
  }
});
