var app = angular.module('dressbook.factories');

app.factory("ChatFactory", function($http, $rootScope, $q, SocketFactory, AppConfig, AuthFactory) {
  var socketEmitter = null;

  var getSocketEmitter = function() {
    return socketEmitter;
  }

  // create new socket emiiter
  var resetSocketEvents = function() {
    socketEmitter = new EventEmitter();
  };

  // create socket emitter
  resetSocketEvents();

  SocketFactory.onConnected(function(isNewConnected) {
    console.log("Chat factory socket");
    if (isNewConnected) {
      console.log("Chat factory socket connected");
      var socket = SocketFactory.overallSocket();

      // event for new message come
      socket.on('newMessage', function(message) {
        console.log('new message come');
        socketEmitter.trigger('newMessage', arguments);
      });

      // event for new message come
      socket.on('updateChatConnected', function(message) {
        console.log('update chatter connected');
        socketEmitter.trigger('updateChatConnected', arguments);
      });
    }
  });

  // function for load chatters list
  var loadChatsConnected = function() {
    return $http.get(AppConfig.server + '/api/chat/listforuser');
  };

  // function for load users for chat
  var loadChatters = function(data) {
    return $http({
      url: AppConfig.server + '/api/user/otherWithLatestMessage',
      method: 'GET',
      params: data
    });
  };

  // function for load private chat by members
  var loadPrivateChat = function(data) {
    return $http({
      url: AppConfig.server + '/api/chat/private_chat_with',
      method: 'GET',
      params: data
    });
  };

  // function for load messages
  var loadMessages = function(data) {
    return $http({
      url: AppConfig.server + '/api/message',
      method: 'GET',
      params: data
    });
  };

  return {
    resetSocketEvents: resetSocketEvents,
    socketEmitter: getSocketEmitter,

    // functions utils
    loadChatsConnected: loadChatsConnected,
    loadChatters: loadChatters,
    loadPrivateChat: loadPrivateChat,
    loadMessages: loadMessages
  }
});
