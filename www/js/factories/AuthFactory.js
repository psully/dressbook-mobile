var app = angular.module('dressbook.factories');

app.factory("AuthFactory", function($http, $injector, $rootScope, $localStorage, $q, GAnalyticsHelper, AppConfig, User) {
  function urlBase64Decode(str) {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw 'Illegal base64url string!';
    }
    return window.atob(output);
  };

  function loginUserData(data) {
    $localStorage['user'] = data;
    // _.forEach(data, function(value, key) {
    //   $localStorage[key] = value;
    // });
    // $localStorage.token = data.token;
    // $localStorage.role = data.role;
    // $localStorage.postcode = data.postcode;
    $rootScope.main.isLoggedIn = true;
    $rootScope.main.user = new User(data);

    var SocketFactory = $injector.get('SocketFactory');
    // create socket factory
    SocketFactory.overallSocketConnect();

    // load chat connected for inbox, chat notification inbox notification count
    var ChatsConnectedService = $injector.get('ChatsConnectedService');
    ChatsConnectedService.init();

    var NotificationFactory = $injector.get('NotificationFactory');
    // init notifications
    NotificationFactory.init();

    // set user id
    GAnalyticsHelper.setUserId(data._id);

    // broadcast event
    $rootScope.$broadcast('loggedin_callback');
  };

  function getLoginData() {
    return $localStorage.user;
    // var keys = ['token', 'postcode', 'role', 'email'];
    // return _.pick($localStorage, keys);
  };

  function logoutUserData() {
    // delete $localStorage.token;
    // delete $localStorage.role;
    delete $localStorage.user;

    var SocketFactory = $injector.get('SocketFactory');
    // disconnect socket factory
    SocketFactory.overallSocketDisconnect();

    $rootScope.$broadcast('logout_callback');
    $rootScope.main.isLoggedIn = false;

    // clear user id
    GAnalyticsHelper.setUserId(undefined);
  };

  function getToken() {
    if($localStorage.user){
      return $localStorage.user.token;
    }
    // return $localStorage.token;
  };

  function getUserFromToken() {
    if(!$localStorage.user){
      return null;
    }
    var token = $localStorage.user.token;
    var user = null;
    if (typeof token !== 'undefined') {
      var encoded = token.split('.')[1];
      try {
        user = JSON.parse(urlBase64Decode(encoded));
      } catch (err) {
        // call for logout
        logoutUserData();
        return false;
      }
    }
    return user;
  };

  function getUser(userID) {
    if (userID) {
      return User.get({id:userID});
    } else {
      return null;
    }
  };

  function getCurrentUser() {
    var user = getUserFromToken();
    if (user) {
      return getUser(user.id);
    } else if (user === false) {
      return false;
    } else {
      return null;
    }
  }

  function isLogged() {
    return getUserFromToken();
  };

  function login(data) {
    var deferred = $q.defer();
    $http.post(AppConfig.server + "/api/user/login", data)
      .success(function(rps) {
        if(data.rememberMe){
          rps.rememberMe = data.rememberMe;
        }
        loginUserData(rps);
        deferred.resolve(rps);
      })
      .error(function(rps) {
        var error = rps ? rps.error || rps : "Has error.";
        deferred.reject(error);
      });
    return deferred.promise;
  };

  function logout(request) {
    if (angular.isUndefined(request)) {
      request = true;
    }

    var deferred = $q.defer();
    var user = getUserFromToken();

    logoutUserData();

    if (request) {
      // call logout
      if (user) {
        var logoutData = {
            user: user.id,
            platform: 'browser'
          };
        // call api to logout
        $http.post(AppConfig.server + "/api/user/logout", logoutData)
          .success(function() {})
          .error(function(data) {})
          .finally(function() {
            deferred.resolve(data);
          });
      } else {
        deferred.resolve();
      }
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  };


  function resetPassword(email) {
    return $http.post(AppConfig.server + "/api/user/resetPassword", {
      email: email
    });
  };

  return {
    getUser: getUser,
    getCurrentUser: getCurrentUser,
    getUserFromToken: getUserFromToken,
    getToken: getToken,
    getLoginData: getLoginData,
    isLogged: isLogged,
    login: login,
    loginUserData: loginUserData,
    logoutUserData: logoutUserData,
    logout: logout,
    resetPassword: resetPassword
  };
});
