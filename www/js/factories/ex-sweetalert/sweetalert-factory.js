app.factory("swalFac", function(ExSwal, swangular){
  return {
    error: function(){
      arguments[0].customClass = 'error';
      arguments[0].confirmButtonColor = '#ff6263';
      return ExSwal.swal.apply(null, arguments);
    },
    success: function(){
      arguments[0].customClass = 'success';
      arguments[0].confirmButtonColor = '#7ccd94';
      return ExSwal.swal.apply(null, arguments);
    },
    info: function(){
      arguments[0].customClass = 'info';
      arguments[0].confirmButtonColor = '#9b59b6';
      return ExSwal.swal.apply(null, arguments);
    },
    warning: function(){
      arguments[0].customClass = 'warning';
      arguments[0].confirmButtonColor = '#e67e22';
      return ExSwal.swal.apply(null, arguments);
    },
    confirm: function(){
      arguments[0].customClass = 'confirm';
      // arguments[0].type = 'warning';
      arguments[0].showCancelButton = true;
      arguments[0].confirmButtonColor = '#7ccd94';
      return ExSwal.swal.apply(null, arguments);
    },
    confirmSuccessWithAvatar: function(){
      arguments[0].customClass = 'confirm-success-with-avatar';
      // arguments[0].type = 'warning';
      arguments[0].showCancelButton = true;
      arguments[0].confirmButtonColor = '#7ccd94';
      return ExSwal.swal.apply(null, arguments);
    },
    confirmErrorWithAvatar: function(){
      arguments[0].customClass = 'confirm-error-with-avatar';
      arguments[0].confirmButtonColor = '#ff6263';
      // arguments[0].type = 'warning';
      arguments[0].showCancelButton = true;
      return ExSwal.swal.apply(null, arguments);
    },
    close: function(){
      swangular.close();
    }
  }
});
