var app = angular.module('dressbook.factories');

app.factory("ExSwal", function($interval, swangular){
  // set default
  swal.setDefaults({
    confirmButtonColor: "#7ccd94",
    padding: 0,
    animation: true
  });

  return {
    swal: function(){
      // var imageUrl;
      // if(arguments[0] && arguments[0].imageUrl){
      //   imageUrl = arguments[0].imageUrl;
      //   delete arguments[0].imageUrl;
      // }

      // if(arguments[0].customClass){
      //   arguments[0].customClass += ' animated slideInDown';
      // }else{
      //   arguments[0].customClass = 'animated slideInDown';
      // }

      var promise;
      if(arguments[0].scope){
        promise = swangular.open.apply(null, arguments);
      }else{
        promise = swangular.swal.apply(null, arguments);
      }


      var stop = $interval(function(){
        if($(".swal2-modal").length){
          $interval.cancel(stop);
          stop = undefined;

          // add close button
          if(!$(".swal2-modal .swal-close").length){
            var swalCloseE = $('<i class="ion-ios-close-outline swal-close"></i>');
            $(".swal2-modal").prepend(swalCloseE);
            swalCloseE.on('click',function(evt){
              evt.preventDefault();

              swangular.close()
            });
          }

          // $(".swal2-modal .ex-sa-picture").remove();
          // if(imageUrl){
          //   var saCustomE = '<div class="ex-sa-picture"><img src="'+imageUrl+'" /></div>';
          //   $(".swal2-modal .swal2-image").after(saCustomE);
          // }
        }
      }, 200);

      return promise;
    }
  }
});
