var app = angular.module('dressbook.factories');

app.factory('UploadFactory', function($window, $q, $cordovaCamera, $cordovaFile, $cordovaImagePicker, $cordovaFileTransfer, $ionicActionSheet) {
  var emitter;
  var getEmitter = function() {
    return emitter;
  }

  // create new socket emitter
  var resetEmitter = function() {
    emitter = new EventEmitter();
  };

  // create socket emitter
  resetEmitter();

  // function for upload single picture
  /*
  	uploadOpts = {
  		- fileKey: The name of the form element. Defaults to file. (DOMString)
  		- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  		- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  		- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  		- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  		- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  		- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  	}
  */
  var uploadPic = function(server, file, uploadOpts) {
    emitter.trigger('beginWebviewUploadPic');

    if (!uploadOpts) {
      uploadOpts = {};
    }
    var dfOpts = {
      chunkedMode: true,
      httpMethod: 'POST'
    };

    uploadOpts = _.merge(dfOpts, uploadOpts);

    var deferred = $q.defer();
    if (ionic.Platform.isWebView()) {
      if (file) {
        file.emitter = new EventEmitter();

        if(uploadOpts.beginUploadCallback_File){
          uploadOpts.beginUploadCallback_File(file);
        }

        var cancelUpload = $cordovaFileTransfer.upload(server, file.fileUrl, uploadOpts, true);

        cancelUpload.then(function(res) {
          if (res.response) {
            var response = angular.fromJson(res.response);
            if(_.isArray(response)){
              response = response[0];
            }

            // console.log("Upload response");
            // console.log(JSON.stringify(response));

            deferred.resolve(response);
            emitter.trigger('successWebviewUploadPic', response);
            file.response = response;
            file.uploadStatus = 1; // uploaded
            file.emitter.trigger("uploadSucceed", response);
          } else {
            deferred.reject(res);
            emitter.trigger('errorWebviewUploadPic');

            // console.log("Upload error");
            // console.log(JSON.stringify(res));

            file.uploadStatus = 2; // upload error
            file.emitter.trigger("uploadError");
          }
        }, function(err) {
          deferred.reject(err);
          emitter.trigger('errorWebviewUploadPic');

          // console.log("Upload error");
          // console.log(JSON.stringify(err));

          file.uploadStatus = 2; // upload error
          file.emitter.trigger("uploadError");
        });

        file.abortUpload = function(){
          if(!file.uploadStatus){
            file.uploadStatus = 3; // abort upload
            // abort upload
            cancelUpload.abort();
            // send status reject
            deferred.reject({status:'cancelled'});
          }
        };
      } else {
        deferred.resolve();
        emitter.trigger('successWebviewUploadPic');
      }
    } else {
      // do nothing
    }
    return deferred.promise;
  };

  // function for upload pictures from local to server
  /*
  	uploadOptions = {
  		- fileKey: The name of the form element. Defaults to file. (DOMString)
  		- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  		- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  		- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  		- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  		- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  		- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  	}
  */
  var uploadPics = function(server, files, uploadOptions) {
    emitter.trigger('beginWebviewUploadPics');

    if (!uploadOptions) {
      uploadOptions = {};
    }

    var deferred = $q.defer();
    if (ionic.Platform.isWebView()) {
      if (files.length) {
        var promises = [];
        files.forEach(function(file) {
          // console.log("uploadPics file");
          // console.log(file);
          promises.push(uploadPic(server, file, uploadOptions));
        });

        $q.all(promises).then(function(responses) {
          // console.log("uploadPics success");
          // console.log(responses);
          deferred.resolve(responses);
          emitter.trigger('successWebviewUploadPics', responses);
        }, function(err) {
          deferred.reject(err || "Has error in processing.");
          emitter.trigger('errorWebviewUploadPic')
        });
      } else {
        deferred.resolve();
        emitter.trigger('successWebviewUploadPics');
      }
    } else {
      deferred.reject('Uploading not supported in browse');
      emitter.trigger('errorWebviewUploadPic')
    }
    return deferred.promise;
  };


  // function for browse an picture
  /*
  	browseOptions:{
  		- quality	Number	Quality of the saved image, range of 0 - 100
  		- sourceType: Camera.PictureSourceType.PHOTOLIBRARY ~ 0, Camera.PictureSourceType.CAMERA ~ 1,  Camera.PictureSourceType.SAVEDPHOTOALBUM ~ 2
  		- destinationType: Camera.DestinationType.DATA_URL ~ 0 => Return base64 encoded string, Camera.DestinationType.FILE_URI ~ 1 => Return file uri (content://media/external/images/media/2 for Android), Camera.DestinationType.NATIVE_URI ~ 2 => Return native uri (eg. asset-library://... for iOS)
  		- allowEdit	Boolean	Allow simple editing of image before selection
  		- encodingType	Number	JPEG = 0, PNG = 1
  		- targetWidth	Number	Width to scale image (pixels). Used with targetHeight
  		- targetHeight	Number	Height to scale image (pixels). Used with targetHeight
  		- mediaType	String	Set the type of media to select from
  		- cameraDirection	Number	Back = 0, Front-facing = 1
  		- popoverOptions	String	iOS-only options that specify popover location in iPad
  		- saveToPhotoAlbum	Boolean	Save image to photo album on the device
  		- correctOrientation	Boolean	correct camera captured images in case wrong orientation

      - browseCallback_File: callback after browse image
  	}
  */
  var webviewBrowsePic = function(browseOptions) {
    emitter.trigger('beginWebviewBrowsePic');
    if (!browseOptions) {
      browseOptions = {};
    }

    var dfOpts = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: Camera.DestinationType.FILE_URI
    };

    browseOptions = _.merge(dfOpts, browseOptions);

    var deferred = $q.defer();
    if (ionic.Platform.isWebView()) {
      // get picture
      $cordovaCamera.getPicture(browseOptions).then(function(file) {
        if (file) {
          $window.resolveLocalFileSystemURL(file, function(fileEntry) {
            var fileObj = {
              fileUrl: fileEntry.toURL(),
              uploadStatus: 0 // uploading
            }
            if(browseOptions.browseCallback_File){
              browseOptions.browseCallback_File(fileObj, function(next, file){
                if(next){
                  deferred.resolve(file);
                }else{
                  deferred.resolve();
                }
              });
            }else{
              deferred.resolve(fileObj);
            }

            emitter.trigger('successWebviewBrowsePic', fileObj);
          }, function() {
            deferred.reject();
            emitter.trigger('errorWebviewBrowsePic');
          });
        } else {
          deferred.resolve();
          emitter.trigger('successWebviewBrowsePic');
        }
      }, function() {
        deferred.reject();
        emitter.trigger('errorWebviewBrowsePic');
      });
    } else {
      deferred.reject("Browse pics doesn't supported");
      emitter.trigger('errorWebviewBrowsePic');
    }

    return deferred.promise;
  };

  // function to get pic from local or camera then upload image
  /*
  	opts : {
  		- server: server url to upload picture
  		- browseOptions: {
  			- quality	Number	Quality of the saved image, range of 0 - 100
  			- sourceType: Camera.PictureSourceType.PHOTOLIBRARY ~ 0, Camera.PictureSourceType.CAMERA ~ 1,  Camera.PictureSourceType.SAVEDPHOTOALBUM ~ 2
  			- destinationType: Camera.DestinationType.DATA_URL ~ 0 => Return base64 encoded string, Camera.DestinationType.FILE_URI ~ 1 => Return file uri (content://media/external/images/media/2 for Android), Camera.DestinationType.NATIVE_URI ~ 2 => Return native uri (eg. asset-library://... for iOS)
  			- allowEdit	Boolean	Allow simple editing of image before selection
  			- encodingType	Number	JPEG = 0, PNG = 1
  			- targetWidth	Number	Width to scale image (pixels). Used with targetHeight
  			- targetHeight	Number	Height to scale image (pixels). Used with targetHeight
  			- mediaType	String	Set the type of media to select from
  			- cameraDirection	Number	Back = 0, Front-facing = 1
  			- popoverOptions	String	iOS-only options that specify popover location in iPad
  			- saveToPhotoAlbum	Boolean	Save image to photo album on the device
  			- correctOrientation	Boolean	correct camera captured images in case wrong orientation
  		},
  		- uploadOptions: {
  			- fileKey: The name of the form element. Defaults to file. (DOMString)
  			- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  			- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  			- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  			- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  			- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  			- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  		}
  	}
  */
  var webviewGetPicAndUpload = function(opts) {
    emitter.trigger('beginWebviewGetPicAndUpload');
    if (!opts) {
      opts = {};
    }

    var dfOpts = {
      // server: server url to upload picture
      browseOptions: {},
      uploadOptions: {}
    };
    opts = _.merge(dfOpts, opts);

    if(opts.browseCallback_File){
      opts.browseOptions.browseCallback_File = opts.browseCallback_File;
    }

    if(opts.beginUploadCallback_File){
      opts.uploadOptions.beginUploadCallback_File = opts.beginUploadCallback_File;
    }

    var deferred = $q.defer();
    webviewBrowsePic(opts.browseOptions).then(function(file) {
      var nextDo = function(file){
        if (file) {
          uploadPic(opts.server, file, opts.uploadOptions).then(function(response) {
            deferred.resolve(response);
            emitter.trigger('successWebviewGetPicAndUpload');
          }, function() {
            deferred.reject();
            emitter.trigger('errorWebviewGetPicAndUpload');
          });

          if(opts.beginUploadCallback){
            opts.beginUploadCallback(file);
          }
        } else {
          deferred.resolve();
          emitter.trigger('successWebviewGetPicAndUpload');
        }
      };

      // call callback
      if(opts.browseCallback){
        var files = file?[file]:null;
        opts.browseCallback(files, function(next, files){
          if(next){
            var file = files.length?files[0]:null;
            nextDo(file);
          }else{
            deferred.resolve();
          }
        });
      }else{
        nextDo(file);
      }
    }, function(err) {
      deferred.reject(err);
      emitter.trigger('errorWebviewGetPicAndUpload');
    });

    return deferred.promise;
  };

  // select one picture from local and upload
  /*
  	opts : {
  		- server: server url to upload picture
  		- uploadOptions: {
  			- fileKey: The name of the form element. Defaults to file. (DOMString)
  			- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  			- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  			- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  			- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  			- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  			- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  		},
  		- browseOptions: {
  			- destinationType: Camera.DestinationType.DATA_URL ~ 0 => Return base64 encoded string, Camera.DestinationType.FILE_URI ~ 1 => Return file uri (content://media/external/images/media/2 for Android), Camera.DestinationType.NATIVE_URI ~ 2 => Return native uri (eg. asset-library://... for iOS)
  		}
  	}
  */
  var webviewBrowsePicAndUpload = function(opts) {
    emitter.trigger('beginWebviewBrowsePicAndUpload');
    if (!opts) {
      opts = {};
    }
    // make sure source type from photo library
    opts.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

    var deferred = $q.defer();
    webviewGetPicAndUpload(opts).then(function(response) {
      deferred.resolve(response);
      emitter.trigger('successWebviewBrowsePicAndUpload');
    }, function() {
      deferred.reject();
      emitter.trigger('errorWebviewBrowsePicAndUpload');
    });

    return deferred.promise;
  };


  // take photo and upload photo
  /*
  	opts : {
  		- server: server url to upload picture
  		- browseOptions: {
  			- destinationType: Camera.DestinationType.DATA_URL ~ 0 => Return base64 encoded string, Camera.DestinationType.FILE_URI ~ 1 => Return file uri (content://media/external/images/media/2 for Android), Camera.DestinationType.NATIVE_URI ~ 2 => Return native uri (eg. asset-library://... for iOS)
  		},
  		- uploadOptions: {
  			- fileKey: The name of the form element. Defaults to file. (DOMString)
  			- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  			- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  			- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  			- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  			- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  			- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  		}
  	}
  */
  var webviewTakePhotoAndUpload = function(opts) {
    emitter.trigger('beginWebviewTakePhotoAndUpload');
    if (!opts) {
      opts = {};
    }
    // make sure source type from camera
    if (!opts.browseOptions) {
      opts.browseOptions = {};
    }
    opts.browseOptions.sourceType = Camera.PictureSourceType.CAMERA;

    var deferred = $q.defer();
    webviewGetPicAndUpload(opts).then(function(response) {
      deferred.resolve(response);
      emitter.trigger('successWebviewTakePhotoAndUpload');
    }, function() {
      deferred.reject();
      emitter.trigger('errorWebviewTakePhotoAndUpload');
    });

    return deferred.promise;
  };

  // function for browse pictures
  /*
  	browseOptions:{
  		- maximumImagesCount: 10,
  		- width: 800,
  		- height: 800,
  		- quality: 100

      - browseCallback_File : callback function after browse image file
  	}
  */
  var webviewBrowsePics = function(browseOptions) {
    emitter.trigger('beginBrowsePics');

    if (!browseOptions) {
      browseOptions = {};
    }
    var dfOpts = {
      // maximumImagesCount: 10,
      // width: 800,
      // height: 800,
      // quality: 100
      useOriginal:true
    };

    browseOptions = _.merge(dfOpts, browseOptions);

    var deferred = $q.defer();
    if (ionic.Platform.isWebView()) {
      $cordovaImagePicker.getPictures(browseOptions).then(function(files) {
        console.log('-------- files.length ------');
        console.log(files.length);
        if (files.length) {
          var results = [];
          async.each(files, function(file, callback) {
            $window.resolveLocalFileSystemURL(file, function(fileEntry) {
              var fileObj = {
                fileUrl: fileEntry.toURL(),
                uploadStatus: 0 // uploading
              };
              if(browseOptions.browseCallback_File){
                browseOptions.browseCallback_File(fileObj, function(next, file){
                  if(next){
                    results.push(fileObj);
                  }
                });
              }else{
                results.push(fileObj);
              }
              callback();
            }, function(err) {
              callback(err);
            });
          }, function(err) {
            if (err) {
              deferred.reject(err);
              emitter.trigger('errorBrowsePics');
              return;
            }
            deferred.resolve(results);
            emitter.trigger('successBrowsePics', results);
          });
        } else {
          deferred.resolve();
          emitter.trigger('successBrowsePics');
        }
      }, function() {
        deferred.reject();
        emitter.trigger('errorBrowsePics');
      });
    } else {
      deferred.reject("Browse pics doesn't supported");
      emitter.trigger('errorBrowsePics');
    }
    return deferred.promise;
  };


  // browse picture from local and upload
  /*
  	opts : {
  		- server: utl to upload image
  		browseOptions:{
  			- maximumImagesCount: 10,
  			- width: 800,
  			- height: 800,
  			- quality: 100
  		},
  		uploadOptions = {
  			- fileKey: The name of the form element. Defaults to file. (DOMString)
  			- fileName: The file name to use when saving the file on the server. Defaults to image.jpg. (DOMString)
  			- httpMethod: The HTTP method to use - either PUT or POST. Defaults to POST. (DOMString)
  			- mimeType: The mime type of the data to upload. Defaults to image/jpeg. (DOMString)
  			- params: A set of optional key/value pairs to pass in the HTTP request. (Object, key/value - DOMString)
  			- chunkedMode: Whether to upload the data in chunked streaming mode. Defaults to true. (Boolean)
  			- headers: A map of header name/header values. Use an array to specify more than one value. On iOS, FireOS, and Android, if a header named Content-Type is present, multipart form data will NOT be used. (Object)
  		}
  	}
  */
  var webviewBrowsePicsAndUpload = function(opts) {
    emitter.trigger('beginBrowsePicsAndUpload');

    // console.log("webviewBrowsePicsAndUpload");
    // console.log(JSON.stringify(opts));

    if (!opts) {
      opts = {};
    }
    var dfOpts = {
      // server: server url to upload
      browseOptions: {
        // maximumImagesCount: 10,
        // width: 800,
        // height: 800,
        // quality: 100
      },
      uploadOptions: {
        chunkedMode: true,
        httpMethod: 'POST'
      }
    };

    opts = _.merge(dfOpts, opts);

    if(opts.browseCallback_File){
      opts.browseOptions.browseCallback_File = opts.browseCallback_File;
    }

    if(opts.beginUploadCallback_File){
      opts.uploadOptions.beginUploadCallback_File = opts.beginUploadCallback_File;
    }

    var deferred = $q.defer();
    if (ionic.Platform.isWebView()) {
      // browse images
      webviewBrowsePics(opts.browseOptions).then(function(files) {
        // console.log("webviewBrowsePics files");
        // console.log(JSON.stringify(files));
        var nextDo = function(files){
          // upload files
          if (files && files.length) {
            uploadPics(opts.server, files, opts.uploadOptions).then(function(responses) {
              // console.log("webviewBrowsePicsAndUpload success");
              // console.log(JSON.stringify(responses));
              deferred.resolve(responses);
              emitter.trigger('successBrowsePicsAndUpload', responses);
            }, function() {
              deferred.reject();
              emitter.trigger('errorBrowsePicsAndUpload');
            });

            if(opts.beginUploadCallback){
              opts.beginUploadCallback(files);
            }
          } else {
            deferred.resolve();
            emitter.trigger('successBrowsePicsAndUpload');
          }
        };

        if(opts.browseCallback){
          opts.browseCallback(files, function(next, files){
            if(next){
              nextDo(files);
            }else{
              deferred.resolve();
            }
          });
        }else{
          nextDo(files);
        }
      }, function() {
        deferred.reject();
        emitter.trigger('errorBrowsePicsAndUpload');
      });
    } else {
      deferred.reject('Uploading not supported in browse');
      emitter.trigger('errorBrowsePicsAndUpload');
    }
    return deferred.promise;
  };



  /* -------------------------------------------- */
  /* -------------------------------------------- */

  /* for multiple upload */
  var multiUpload = function(options) {
    var dfOptions = {
      fromGalleryOptions:{},
      fromCameraOptions:{}
    };
    options = _.merge(dfOptions, options);

    if(options.browseCallback_File){
      options.fromGalleryOptions.browseCallback_File = options.fromCameraOptions.browseCallback_File = options.browseCallback_File;
      delete options.browseCallback_File;
    }
    if(options.beginUploadCallback_File){
      options.fromGalleryOptions.beginUploadCallback_File = options.fromCameraOptions.beginUploadCallback_File = options.beginUploadCallback_File;
      delete options.beginUploadCallback_File;
    }

    if(options.browseCallback){
      options.fromGalleryOptions.browseCallback = options.fromCameraOptions.browseCallback = options.browseCallback;
      delete options.browseCallback;
    }
    if(options.beginUploadCallback){
      options.fromGalleryOptions.beginUploadCallback = options.fromCameraOptions.beginUploadCallback = options.beginUploadCallback;
      delete options.beginUploadCallback;
    }

    var _uploadFromGallery = function() {
      return webviewBrowsePicsAndUpload(options.fromGalleryOptions);
    };

    var _takePhotoAndUpload = function() {
      return webviewTakePhotoAndUpload(options.fromCameraOptions);
    };

    this.browseUpload = function() {
      if (options.disabled) {
        return;
      }

      // show action sheet to choose upload
      $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-images"></i> Choose from photo gallery'
        }, {
          text: '<i class="icon ion-ios-camera-outline"></i> Take a photo'
        }],
        // destructiveText: 'Delete',
        titleText: 'Choose to upload',
        cancelText: 'Cancel',
        cancel: function() {},
        buttonClicked: function(index) {
          if(options.afterChooseMenuCallback){
            options.afterChooseMenuCallback();
          }

          var promise;
          if (index == 0) {
            promise = _uploadFromGallery();
            promise.then(function(responses) {
              // console.log("responses");
              // console.log(responses);
              if(options.doneCallback){
                options.doneCallback(null, responses);
              }
            }, function(err) {
              if(options.doneCallback){
                options.doneCallback(err || "Has error occurs");
              }
            });
          } else if (index == 1) {
            promise = _takePhotoAndUpload();
            promise.then(function(response) {
              // console.log('response');
              // console.log(response);
              if(options.doneCallback){
                options.doneCallback(null, [response]);
              }
            }, function(err) {
              if(options.doneCallback){
                options.doneCallback(err || "Has error occurs");
              }
            });
          }
          if(options.pushUploadPromise){
            options.pushUploadPromise(promise);
          }
          return true;
        }
      });
    };
  };

  /* for single browse and upload */
  var singleUpload = function(options) {
    var dfOptions = {
      fromGalleryOptions:{},
      fromCameraOptions:{}
    };
    options = _.merge(dfOptions, options);
    if(options.browseCallback_File){
      options.fromGalleryOptions.browseCallback_File = options.fromCameraOptions.browseCallback_File = options.browseCallback_File;

      delete options.browseCallback_File;
    }
    if(options.beginUploadCallback_File){
      options.fromGalleryOptions.beginUploadCallback_File = options.fromCameraOptions.beginUploadCallback_File = options.beginUploadCallback_File;

      delete options.beginUploadCallback_File;
    }

    if(options.browseCallback){
      options.fromGalleryOptions.browseCallback = options.fromCameraOptions.browseCallback = options.browseCallback;

      delete options.browseCallback;
    }
    if(options.beginUploadCallback){
      options.fromGalleryOptions.beginUploadCallback = options.fromCameraOptions.beginUploadCallback = options.beginUploadCallback;

      delete options.beginUploadCallback;
    }

    var _uploadFromGallery = function() {
      return webviewBrowsePicAndUpload(options.fromGalleryOptions);
    };

    var _takePhotoAndUpload = function() {
      return webviewTakePhotoAndUpload(options.fromCameraOptions);
    };

    this.browseUpload = function() {
      // show action sheet to choose upload
      $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-images"></i> Choose from photo gallery'
        }, {
          text: '<i class="icon ion-ios-camera-outline"></i> Take a photo'
        }],
        // destructiveText: 'Delete',
        titleText: 'Choose to upload',
        cancelText: 'Cancel',
        cancel: function() {},
        buttonClicked: function(index) {
          if(options.afterChooseMenuCallback){
            options.afterChooseMenuCallback();
          }

          if (index == 0) {
            var promise = _uploadFromGallery();
            promise.then(function(response) {
              // console.log("uploadFromGallery response");
              // console.log(response);
              options.doneCallback(null, response);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          } else if (index == 1) {
            var promise = _takePhotoAndUpload();
            promise.then(function(response) {
              // console.log('takePhotoAndUpload response');
              // console.log(response);
              options.doneCallback(null, [response]);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          }
          return true;
        }
      });
    };
  }

  /* multiple file browse */
  var multiFilesBrowse = function(options) {
    var _browseFromGallery = function() {
      return webviewBrowsePics(options.fromGalleryOptions);
    };

    var _takePhotoByCamera = function() {
      return webviewBrowsePic(options.fromCameraOptions);
    };

    this.browseFiles = function() {
      // show action sheet to choose upload
      $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-images"></i> Choose from photo gallery'
        }, {
          text: '<i class="icon ion-ios-camera-outline"></i> Take a photo'
        }],
        // destructiveText: 'Delete',
        titleText: 'Choose to upload',
        cancelText: 'Cancel',
        cancel: function() {},
        buttonClicked: function(index) {
          if(options.afterChooseMenuCallback){
            options.afterChooseMenuCallback();
          }

          if (index == 0) {
            _browseFromGallery().then(function(files) {
              // console.log("browse from gallery files result");
              // console.log(files);
              options.doneCallback(null, files);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          } else if (index == 1) {
            _takePhotoByCamera().then(function(file) {
              // console.log("take picture from camera file result");
              // console.log(file);
              options.doneCallback(null, [file]);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          }
          return true;
        }
      });
    };
  }

  /* single file browse */
  var singleFileBrowse = function(options) {

    var _browseFromGallery = function() {
      return webviewBrowsePic(options.fromGalleryOptions);
    };

    var _takePhotoAndUpload = function() {
      return webviewBrowsePic(options.fromCameraOptions);
    };

    this.browseFile = function() {
      // show action sheet to choose upload
      $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-images"></i> Choose from photo gallery'
        }, {
          text: '<i class="icon ion-ios-camera-outline"></i> Take a photo'
        }],
        // destructiveText: 'Delete',
        titleText: 'Choose to upload',
        cancelText: 'Cancel',
        cancel: function() {},
        buttonClicked: function(index) {
          if(options.afterChooseMenuCallback){
            options.afterChooseMenuCallback();
          }

          if (index == 0) {
            _browseFromGallery().then(function(file) {
              // console.log("_browseFromGallery file");
              // console.log(JSON.stringify(file));
              options.doneCallback(null, file);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          } else if (index == 1) {
            _takePhotoAndUpload().then(function(file) {
              // console.log('_takePhotoAndUpload file');
              // console.log(file);
              options.doneCallback(null, file);
            }, function(err) {
              options.doneCallback(err || "Has error occurs");
            });
          }
          return true;
        }
      });
    };
  };

  return {
    emitter: getEmitter,
    uploadPic: uploadPic, // upload single file
    uploadPics: uploadPics, // upload multiple files
    webviewBrowsePic: webviewBrowsePic, // browse a picture from local
    webviewGetPicAndUpload: webviewGetPicAndUpload, // browse a picture (from local or take from camera) and upload
    webviewBrowsePicAndUpload: webviewBrowsePicAndUpload, // browse a picture from local and upload
    webviewTakePhotoAndUpload: webviewTakePhotoAndUpload, // take a picture using camera and upload
    webviewBrowsePics: webviewBrowsePics, // browse pictures from local
    webviewBrowsePicsAndUpload: webviewBrowsePicsAndUpload, // browse picture from local and upload
    /*-----------------*/
    multiUpload: multiUpload,
    singleUpload: singleUpload,
    multiFilesBrowse: multiFilesBrowse,
    singleFileBrowse: singleFileBrowse
  };
});
