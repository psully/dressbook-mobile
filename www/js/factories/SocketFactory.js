var app = angular.module('dressbook.factories');

app.factory("SocketFactory", function($http, $rootScope, $q, AppConfig, AuthFactory) {
  var emitter;
  var getSocketEmitter = function() {
    return emitter;
  }

  // create new socket emitter
  var resetEmitter = function() {
    emitter = new EventEmitter();
  };

  // create socket emitter
  resetEmitter();


  var overallSocket = null;

  var getOverallSocket = function() {
    return overallSocket;
  };

  var isConnected = false;
  // check if new connect(after login) or just disconnect with server and reconnect again
  var isNewConnected = true;
  var overallSocketConnect = function() {
    if (overallSocket) {
      return overallSocket;
    }
    if (AuthFactory.isLogged()) {
      console.log("create new socket");
      overallSocket = io(AppConfig.socketServer, {
        query: "token=" + encodeURI(AuthFactory.getToken()) + '&isOverall=1'
      });
      isNewConnected = true;

      overallSocket.on('connected', function() {
        console.log("Socket connected");
        isConnected = true;

        emitter.trigger('connected');

        overallSocket.on('disconnect', function() {
          isConnected = false;
          // set not new connected at disconnect
          isNewConnected = false;
          emitter.trigger('disconnect');
        });

        // update user information
        overallSocket.on('updateUser', function(user) {
          if (!$rootScope.$$phase && (!$rootScope.$root || !$rootScope.$root.$$phase)) {
            $rootScope.$apply(function() {
              _.extend($rootScope.main.user, user);
            });
          }
        });
      });
      overallSocket.on('connect_failed', function() {
        console.log('Connection Failed');
      });
      overallSocket.on('connect_error', function() {
        console.log('Connection Failed');
      });
      return overallSocket;
    }

    return null;
  };

  var overallSocketDisconnect = function() {
    if (overallSocket) {
      overallSocket.disconnect(); // disconnect server
      overallSocket.destroy(); // destroy socket connect
      overallSocket = null;
    }
    isConnected = false;
  };

  var onConnected = function(callback) {
    if (isConnected) {
      callback(isNewConnected);
    }
    emitter.on('connected', function() {
      callback(isNewConnected);
    });
  };

  return {
    emitter: getSocketEmitter,
    resetEmitter: resetEmitter,
    overallSocket: getOverallSocket,
    onConnected: onConnected,
    overallSocketConnect: overallSocketConnect,
    overallSocketDisconnect: overallSocketDisconnect
  }
});
