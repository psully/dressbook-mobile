var app = angular.module('dressbook', ['ionic', 'dressbook.configs', 'dressbook.routes', 'dressbook.factories', 'dressbook.services', 'dressbook.controllers', 'dressbook.models', 'starter.controllers', 'ngStorage', 'ngCordova']);

var serviceDownDialogShown = false;
var pingServerInterval;
//  APP RUN & PLUGIN
app.config(function($httpProvider, $ionicConfigProvider) {
  $ionicConfigProvider.backButton.previousTitleText(false).text('');

  $httpProvider.interceptors.push(function($q, $location, $injector, $interval, AppConfig) {
    return {
      'request': function(config) {
        var AuthFactory = $injector.get('AuthFactory');

        config.headers = config.headers || {};
        var token = AuthFactory.getToken();
        if (token) {
          config.headers.Authorization = token;
        }
        return config;
      },
      'responseError': function(response) {
        var $state = $injector.get('$state');
        var AuthFactory = $injector.get('AuthFactory');
        var $http = $injector.get('$http');
        var swalFac = $injector.get('swalFac');

        if (response.status <= 0 && !serviceDownDialogShown) {
          console.log("------- errorrrrrrrrrrrrrr -----------------");
          serviceDownDialogShown = true;

          // server down or lost network => show error
          swalFac.error({
            text: response.url,
            title: 'Error!'
          });

          if (!pingServerInterval) {
            // require ping server to check server work
            var clearPingServer = function() {
              if (pingServerInterval) {
                $interval.cancel(pingServerInterval);
                pingServerInterval = undefined;

                // close popup
                // swal.close();

                // reload route
                // $state.reload();
                AuthFactory.logout();
                $state.go('app.login');
                swalFac.error({
                  text: 'We’ve noticed that something has gone wrong, but the problem has now been resolved. Please continue to use Dressbook, thanks.',
                  title: 'Connected!'
                });
              }
              serviceDownDialogShown = false;
            };

            pingServerInterval = $interval(function() {
              $http.get(AppConfig.server + '?' + new Date().getTime()).then(function() {
                clearPingServer();
              }, function(response) {
                if (response.status > 0) {
                  clearPingServer();
                }
              });
            }, 1000);
          }
        }

        if (response.status === 401 || response.status === 403) {
          AuthFactory.logout();
          $state.go('app.login');
        }

        return $q.reject(response);
      }
    };
  });
});

var firstRun = true;
var checkFirst = false;
app.run(function($injector, $controller, $ionicPlatform, $timeout, $http, $log, $rootScope, $location, $state, AuthFactory, AppConfig, GAnalyticsHelper, Notification, User, swalFac) {
  $rootScope.main = {
    userAvatar: AppConfig.defaultAvatar
  };

  $rootScope.profileUrl = function(user) {
    if (user._id == $rootScope.main.user._id) {
      return $state.href('app.profile');
    } else {
      return $state.href('app.viewUser', {
        user: user._id
      });
    }
  };

  $rootScope.goToProfile = function(user, event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (user._id == $rootScope.main.user._id) {
      return $state.go('app.profile');
    } else {
      return $state.go('app.viewUser', {
        user: user._id
      });
    }
  };

  $rootScope.$watch('main.user.imageUrl', function(newValue) {
    console.log('Main user changed');
    if ($rootScope.main.user) {
      console.log($rootScope.main.user.getAvatar());
      $rootScope.main.userAvatar = $rootScope.main.user.getAvatar();
    }
  });

  if (serviceDownDialogShown) {
    // server down or lost network => show error
    swalFac.error({
      text: 'Sorry the service is not available at the moment, please try again shortly.',
      title: 'Error!'
    });
  }

  // authentication
  $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
    $rootScope.hideNavBar = false;
    console.log("------------ to state");
    console.log(JSON.stringify(toState.url));

    // show or hide menu
    $rootScope.main.dragContent = true;
    if (toState.env) {
      if (toState.env.dragContent === false) {
        $rootScope.main.dragContent = false;
      }
    }

    // check authentication
    if (firstRun) {
      console.log("first run -----------------------");
      firstRun = false;

      if(toState.access && !toState.access.requiredLogin){
        return ;
      }else if(AuthFactory.isLogged()){
        var userData = AuthFactory.getLoginData();
        if(userData.rememberMe){
          // load user data
          User.get({
        		id: AuthFactory.getUserFromToken().id
        	}, function(user) {
            console.log('-------------- re-logged in --------------');
            user.rememberMe = true;
            AuthFactory.loginUserData(user);
            $state.go('app.listItems');
          });
          event.preventDefault();
          return ;
        }else{
          AuthFactory.logoutUserData();
          $state.go('app.login');
          event.preventDefault();
          return ;
        }
      } else{
        $state.go('app.login');
        event.preventDefault();
        return ;
      }
    }

    console.log("after first run -----------------------");
    $rootScope.toState = toState;
    $rootScope.toStateParams = toStateParams;
    if (toState.access && toState.access.requiredLogin && !AuthFactory.isLogged()) {
      console.log("after first run go to login -----------------------");
      $state.go('app.login');
      event.preventDefault();
    }
  });

  // add broadcast event
  $rootScope.$on('loggedin_callback', function() {
    console.log("loggedin_callback trigged");
    // push this data to server
    // alert("loggedin_callback "+JSON.stringify($rootScope.deviceData));
    if($rootScope.deviceData){
      console.log("----------register device: "+JSON.stringify($rootScope.deviceData));
      $rootScope.main.user.registerDevice($rootScope.deviceData);
    }
  });

  $rootScope.$on('logout_callback', function() {
    // do nothing
  });

  $rootScope.searchCondition = {
    words: '',
    categorys: []
  };

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // hide splash screen
    console.log("----------- Ionic ready ------------");
    // alert("Before ide splash screen");
    if (ionic.Platform.isWebView()) {
      console.log('--------- hide splash screen ---------');
      navigator.splashscreen.hide();
      // alert("splash screen hidden");
    }

    // start google analytics tracker
    if (ionic.Platform.isWebView()) {
      GAnalyticsHelper.startTrackerWithId(AppConfig.ggTrakerId);
    }

    // set stripe key
    Stripe.setPublishableKey(AppConfig.stripeKey);

    // reload page
    // if (!$state.current.abstract) {
    //   $state.go($state.current, {}, {
    //     reload: true
    //   });
    // }

    // register push notification
    console.log("---------------------------------------------");
    if (ionic.Platform.isWebView()) {
      console.log("-------- ionic.Platform.isWebView trigged");
      var push = PushNotification.init({
        android: {
          senderID: AppConfig.ggProjectNumber
        },
        browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        },
        ios: {
          alert: "true",
          badge: "true",
          sound: "true"
        },
        windows: {}
      });

      push.on('registration', function(data) {
        // data.registrationId
        console.log("Push notification: On registration");
        console.log(JSON.stringify(data));

        // save device info
        $rootScope.deviceData = {
          deviceToken: data.registrationId,
          deviceType: device.platform
        };
      });

      push.on('notification', function(data) {
        console.log("Push notification: received notification");
        console.log(JSON.stringify(data));
        // data.message,
        // data.title,
        // data.count,
        // data.sound,
        // data.image,
        // data.additionalData
      });

      push.on('error', function(e) {
        // e.message
        console.log("Register push notification error");
        console.log(JSON.stringify(e));
        // $rootScope.main.user.registerDeviceError({error:e});
      });
    }
  });
});
