var app = angular.module('dressbook.models');

app.factory("Comment",function($resource, AppConfig){
    return $resource(AppConfig.server+'/api/comment/:id',{id:"@_id"});
});