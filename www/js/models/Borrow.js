var app = angular.module('dressbook.models');

app.factory("Borrow",function($resource, $http,AppConfig){
    var Borrow = $resource(AppConfig.server+'/api/borrow/:id',{id:"@_id"});
    Borrow.prototype.accept = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/borrow/'+this._id+'/accept',
    		data:{
    			accept:true
    		}
    	});
    };
    Borrow.prototype.reject = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/borrow/'+this._id+'/accept',
    		data:{
    			accept:false
    		}
    	});
    };
    Borrow.prototype.markDelivered = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/borrow/'+this._id+'/mark-delivered',
    		data:{
    			accept:false
    		}
    	});
    };
    Borrow.prototype.markReturned = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/borrow/'+this._id+'/mark-returned',
    		data:{
    			accept:false
    		}
    	});
    };
    Borrow.prototype.markReceivedBack = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/borrow/'+this._id+'/mark-received-back',
    		data:{
    			accept:false
    		}
    	});
    };
    return Borrow;
});
