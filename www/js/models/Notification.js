var app = angular.module('dressbook.models');

app.factory("Notification",function($q, $resource, $http,AppConfig, Borrow){
    var Notification = $resource(AppConfig.server+'/api/notification/own/:id',{id:"@_id"});

    Notification.countNews = function(){
    	return $http.get(AppConfig.server+'/api/notification/own/countNews');
    }

    return Notification;
});