var app = angular.module('dressbook.models');

app.factory("Size",function($resource,AppConfig){
    return $resource(AppConfig.server+'/api/size/:id',{id:"@_id"});
});