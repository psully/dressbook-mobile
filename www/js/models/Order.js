var app = angular.module('dressbook.models');

app.factory("Order",function($resource, $http,AppConfig){
    var Order = $resource(AppConfig.server+'/api/order/:id',{id:"@_id"});
    Order.prototype.accept = function(){
    	return $http({
    		method:'PUT',
    		url:AppConfig.server+'/api/order/'+this._id+'/accept',
    		data:{
    			accept:true
    		}
    	});
    };

    Order.prototype.markShipped = function(){
    	return $http({
    		method:'POST',
    		url:AppConfig.server+'/api/order/'+this._id+'/markShipped'
    	});
    };

    Order.prototype.reject = function(){
    	return $http({
    		method:'POST',
    		url:AppConfig.server+'/api/order/'+this._id+'/reject'
    	});
    };

    return Order;
});
