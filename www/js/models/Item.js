var app = angular.module('dressbook.models');

app.factory("Item",function($q, $resource, $http,AppConfig, Borrow, AppConfig){
    var Item = $resource(AppConfig.server+'/api/item/:id',{id:"@_id"},{
        getRaw:{
            method:'GET',
            url:AppConfig.server+'/api/item/:id?raw=true',
            params:{
                id:"@id"
            }
        }
    });

    Item.prototype.getOwnBorrow = function(){
    	var deffered = $q.defer();
    	$http.get(AppConfig.server+'/api/item/'+this._id+'/getOwnBorrow').then(function(rps){
    		if(rps.data){
	    		var borrow = new Borrow(rps.data);
	    		deffered.resolve(borrow);
    		}else{
    			deffered.resolve();
    		}
    	}, function(rps){
    		deffered.reject(err);
    	});

    	return deffered.promise;
    };

    Item.prototype.getOwnOrderNoResponse = function(){
    	var deffered = $q.defer();
    	$http.get(AppConfig.server+'/api/item/'+this._id+'/getOwnOrderNoResponse').then(function(rps){
    		if(rps.data){
	    		deffered.resolve(rps.data);
    		}else{
    			deffered.resolve();
    		}
    	}, function(rps){
    		deffered.reject(err);
    	});
    	return deffered.promise;
    };

    Item.prototype.getComments = function(){
        return $http.get(AppConfig.server+'/api/item/'+this._id+'/comment');
    };

    Item.prototype.addComment = function(data){
        return $http({
            method:'POST',
            url:AppConfig.server+'/api/item/'+this._id+'/comment',
            data:data
        });
    };

    Item.prototype.mainImageLink = function(version){
        return AppConfig.server+AppConfig.itemImagesPath+'/'+version+'/'+this.mainImage;
    };

    Item.prototype.imagesLinks = function(version){
        return _.map(this.images, function(image){
            return AppConfig.server+AppConfig.itemImagesPath+'/'+version+'/'+image.image;
        });
    };

    Item.prototype.delete = function(){
        var $this = this;

        var defer = $q.defer();
        $http({
            url:AppConfig.server+'/api/item/'+itemId,
            method:'DELETE'
        }).then(function(){
            $this.isDeleted = true;
            defer.resolve();
        }, function(){
            defer.reject();
        });

        return defer.promise;
    }

    Item.prototype.doLike = function(){
        return $http({
            method:'POST',
            url:AppConfig.server+'/api/item/'+this._id+'/dolike'
        });
    };

    Item.likeds = function (query){
    	return $http({
				url: AppConfig.server+'/api/item/likeds',
				method: "GET",
				params: query
			});
    };

    Item.checkLikedsHasNext = function(queryParams, callback){
      $http({
        url: AppConfig.server+'/api/item/likeds-has-next',
        method: 'GET',
        params:queryParams
      }).then(function(rps){
        callback(null, rps.data);
      }, callback);
    }

    Item.delete = function(itemId){
        return $http({
            url:AppConfig.server+'/api/item/'+itemId,
            method:'DELETE'
        });
    };

    Item.mainImageLink = function(item, version){
        return AppConfig.server+AppConfig.itemImagesPath+'/'+version+'/'+item.mainImage;
    };

    Item.imageLink = function(image,version){
        return AppConfig.server+AppConfig.itemImagesPath+'/'+version+'/'+image;
    }

    Item.imagesLinks = function(item, version){
        return _.map(item.images, function(image){
            return AppConfig.server+AppConfig.itemImagesPath+'/'+version+'/'+image.image;
        });
    };

    Item.checkHasNext = function(queryParams, callback){
      $http({
        url: AppConfig.server+'/api/item/has-next',
        method: 'GET',
        params:queryParams
      }).then(function(rps){
        callback(null, rps.data);
      }, callback);
    }

    return Item;
});
