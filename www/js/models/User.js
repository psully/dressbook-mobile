var app = angular.module('dressbook.models');

app.provider("User", function() {
  this.$get = function($rootScope, $q, $resource, $http, $timeout, $injector, AppConfig) {
    var User = $resource(AppConfig.server + '/api/user/:id', {
      id: "@_id"
    });

    User.list = function(data) {
      return $http({
        url: AppConfig.server + '/api/user',
        method: 'GET',
        params: data
      });
    };

    User.prototype.saveGeneralInfo = function(data){
      return $http.post(AppConfig.server + '/api/user/general', data);
    };

    User.prototype.saveEmail = function(data){
      return $http.post(AppConfig.server + '/api/user/email', data);
    };

    User.prototype.savePassword = function(data){
      return $http.post(AppConfig.server + '/api/user/password', data);
    };

    User.prototype.getAvatar = function() {
      return this.imageUrl ? AppConfig.server + AppConfig.profilePicturePath + '/' + this.imageUrl : AppConfig.defaultAvatar;
    };

    User.prototype.countItems = function() {
      return $http.get(AppConfig.server + '/api/user/' + this._id + '/countItems');
    };

    User.prototype.countItemsLiked = function() {
      return $http.get(AppConfig.server + '/api/user/' + this._id + '/countItemsLiked');
    };

    User.prototype.countScore = function() {
      return $http.get(AppConfig.server + '/api/user/' + this._id + '/countScore');
    };

    User.prototype.scoreHistories = function() {
      return $http.get(AppConfig.server + '/api/user/' + this._id + '/scoreHistories');
    };

    User.prototype.transactions = function() {
      return $http.get(AppConfig.server + '/api/user/' + this._id + '/transactions');
    };

    User.prototype.reqAddFriend = function(userId) {
      return $http.post(AppConfig.server + '/api/user/reqAddFriend/', {
        user: userId
      });
    };

    User.prototype.acceptFriendReq = function(friendRequestId) {
      return $http.post(AppConfig.server + '/api/user/acceptFriendReq/', {
        friendRequest: friendRequestId
      });
    };

    User.prototype.rejectFriendReq = function(friendRequestId) {
      return $http.post(AppConfig.server + '/api/user/rejectFriendReq/', {
        friendRequest: friendRequestId
      });
    };

    User.prototype.unFriend = function(friendId) {
      return $http.post(AppConfig.server + '/api/user/unFriend/', {
        friend: friendId
      });
    };

    User.prototype.saveCardInfo = function(data) {
      return $http({
        method: 'POST',
        url: AppConfig.server + '/api/user/savecard',
        data: data
      });
    };

    User.prototype.refreshStripe = function() {
      var deferred = $q.defer();
      $http.get(AppConfig.server + '/api/user/' + this._id + '/stripeData').then(function(rps) {
        // update main user
        $rootScope.main.user.stripe = rps.data;
        deferred.resolve(rps.data);
      }, function(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    };



    User.prototype.refresh = function() {
      var defer = $q.defer();
      var $this = this;
      $timeout(function() {
        // reload user
        User.get({
          id: $this._id
        }, function(user) {
          $this = _.merge($this, _.pick(user, ['email', 'imageUrl', 'name', 'postcode', 'role', 'stripe', 'token', 'username']));
          defer.resolve();
        }, function(err) {
          defer.resolve();
        });
      });
      return defer.promise;
    }

    User.prototype.registerDevice = function(data) {
      var deferred = $q.defer();
      $http.post(AppConfig.server + '/api/user/registerDevice', data).then(function(rps) {
        deferred.resolve(rps.data);
      }, function(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    User.prototype.registerDeviceError = function(data) {
      var deferred = $q.defer();
      $http.post(AppConfig.server + '/api/user/registerDeviceError', data).then(function(rps) {
        deferred.resolve(rps.data);
      }, function(err) {
        deferred.reject(err);
      });
      return deferred.promise;
    }

    return User;
  };
});
