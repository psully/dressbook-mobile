var app = angular.module('dressbook.models');

app.provider("Category",function(){
	this.$get = function($resource, AppConfig){
		var Category = $resource(AppConfig.server+'/api/category/:id',{id:"@_id"});
		Category.prototype.Color = function(){
			return $resource(AppConfig.server+'/api/category/'+this._id+'/colors/:color',{color:"@_id"});
		};
		Category.prototype.Size = function(){
			return $resource(AppConfig.server+'/api/category/'+this._id+'/sizes/:size',{size:"@_id"});
		};
		Category.prototype.Brand = function(){
			return $resource(AppConfig.server+'/api/category/'+this._id+'/brands/:brand',{brand:"@_id"});
		};
		Category.buildTree = function(categories){
			var results = [];
			categories.forEach(function(category){
				var checkInSubs = false;
				categories.forEach(function(cat){
					if(category.parent == cat._id){
						if(!cat.subs){
							cat.subs = [];
						}
						cat.subs.push(category);
						checkInSubs = true;
					}
				});
				if(!checkInSubs){
					results.push(category);
				}
			});

			return results;
		};
		return Category;
	}
});