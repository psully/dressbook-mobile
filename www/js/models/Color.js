var app = angular.module('dressbook.models');

app.factory("Color",function($resource, AppConfig){
    return $resource(AppConfig.server+'/api/color/:id',{id:"@_id"});
});
