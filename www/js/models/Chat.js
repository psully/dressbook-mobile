var app = angular.module('dressbook.models');

app.factory("Chat",function($resource, $http, AppConfig){
    var  chat = $resource(AppConfig.server+'/api/chat/:id',{id:"@_id"});
    chat.prototype.leave = function(){
    	return $http.get(AppConfig.server+'/api/chat/'+this._id+'/leave');
    };
    return chat;
});