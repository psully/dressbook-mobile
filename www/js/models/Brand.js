var app = angular.module('dressbook.models');

app.factory("Brand",function($resource, AppConfig){
    return $resource(AppConfig.server+'/api/brand/:id',{id:"@_id"});
});