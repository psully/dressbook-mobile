angular.module('ionicApp', [])

.controller('LoginCtrl', function($scope, $http) {
 $http.get('localhost:3000/').then(function(resp) {
    console.log('Success', resp);
    // For JSON responses, resp.data contains the result
  }, function(err) {
    console.error('ERR', err);
    // err.status will contain the status code
  })
})

//the above controller is used to log in to Dressbook. This should also hold the session.

.controller('LogOutCtrl', function($scope, $http) {
 $http.get('https://cors-test.appspot.com/test').then(function(resp) {
    console.log('Success', resp);
    // For JSON responses, resp.data contains the result
  }, function(err) {
    console.error('ERR', err);
    // err.status will contain the status code
  })
})

//the above controller is used to log a user OUT of dressbook.
