var app = angular.module('dressbook.services');

app.service("ChatsConnectedService",function($q, $timeout, User, AppConfig, AuthFactory, ChatFactory, Utils, swalFac){
	var $this = this;

	this.chatsConnected = [];

	this.emitter = null;
	this.resetEmiter = function(){
		this.emitter = new EventEmitter();
	}
	this.resetEmiter();

	var addChatConnected = function(connect, isUnshift){
		if(connect.user){
			connect.user.avatar = Utils.getUserAvatar(connect.user);
		}
		// build field for check has been seen
		if(connect.lastMessage){
			connect.lastMessage.hasSeen = _.includes(connect.lastMessage.membersSeen, AuthFactory.getUserFromToken().id);
		}

		if (!isUnshift) {
			$this.chatsConnected.push(connect);
		} else {
			$this.chatsConnected.unshift(connect);
		}

		$this.emitter.trigger('addChatConnected',{
			connect:connect,
			isUnshift:isUnshift
		});
	};

	var updateChatConnectedProcess = function(rps) {
		if (rps.isNew) {
			if (!$this.chatsConnected) {
				$this.chatsConnected = [];
			}
			addChatConnected(rps.connected, true);
		} else{
			if(rps.connected.lastMessage){
				var hasSeen = _.includes(rps.connected.lastMessage.membersSeen, AuthFactory.getUserFromToken().id);
				// update current connect
				var inListConnectedIndex = _.findIndex($this.chatsConnected, function(inListConnected) {
					return inListConnected._id == rps.connected._id;
				});
				if(!hasSeen){
					if (inListConnectedIndex >= 0) {
						$this.chatsConnected.splice(inListConnectedIndex, 1);
					}
					addChatConnected(rps.connected, true);
				}else{
					// just update data
					$this.chatsConnected[inListConnectedIndex] = _.merge(
						$this.chatsConnected[inListConnectedIndex],
						rps.connected,
						{
							lastMessage:{
								hasSeen:true
							}
						}
					);
					$this.emitter.trigger('updateExistingChatConnected', rps);
				}
			}
		}
	};

	// register event for receiving update
	ChatFactory.socketEmitter().on('updateChatConnected', updateChatConnectedProcess);

	// var isLoad = false;

	this.isLoading = false;
	this.isLoaded = false;
	this.init = function(){
		this.isLoading = true;
		this.isLoaded = false;
		this.chatsConnected = [];
		this.resetEmiter();

		var deffered = $q.defer();

		async.parallel([
			function(callback) {
				// load chat history
				ChatFactory.loadChatsConnected().then(function(response) {
					response.data.forEach(function(connect) {
						addChatConnected(connect);
					});
					callback();
				}, callback);
			}
		], function(err, results) {
			$this.isLoading = false;
			if (err) {
				swalFac.error({
					text: 'Has error in loadding chat data. Please logout and login again.',
					title: 'Error!'
				});
				deffered.reject(err);
				this.emitter.trigger('onLoadedError');
				return;
			}
			deffered.resolve();
			$this.emitter.trigger('onLoaded');
			$this.isLoaded = true;
		});
		return deffered.promise;
	};

	this.onLoad = function(callback){
		if(this.isLoaded){
			callback();
		}else{
			this.emitter.on('onLoaded',function(){
				callback();
			});
		}
	};

	this.runUpdate = function(){
		this.emitter.trigger('update');
	};
});
