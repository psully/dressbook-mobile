var app = angular.module('dressbook.helpers');

app.factory("Utils", function(AppConfig) {
  return {
    // substring by index and space
    subStringBySpace: function(str, index, addDots) {
      if (index >= str.length) {
        return str;
      }
      if (!addDots) {
        addDots = true;
      }
      var result = str.substr(0, index + 1);

      var subTo = result.lastIndexOf(' ') != -1 ? result.lastIndexOf(' ') : index;
      result = result.substr(0, subTo);
      if (addDots && str.length > subTo) {
        result += '...';
      }
      return result;
    },
    moneyFormat: function(amount, c, d, t) {
      var n = amount,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
      return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    fromNow: function(from, now) {
      if (!now) {
        now = new Date();
      }
      var from = moment(from);
      var now = moment(now);

      var fDate = from.date();
      var fWeek = from.week();
      var fMonth = from.month();
      var fYear = from.year();

      var nDate = now.date();
      var nWeek = now.week();
      var nMonth = now.month();
      var nYear = now.year();

      if (fYear == nYear) {
        if (fWeek == nWeek) {
          if (fDate == nDate) {
            // return hour
            return from.format('h:mm A');
          }
          // return day
          return from.format('ddd');
        }
        // return date and month
        return from.format('MMM D');
      }

      return from.format('M/DD/YY');
    },

    getUserAvatar: function(user) {
      return user.imageUrl ? AppConfig.server + AppConfig.profilePicturePath + '/' + user.imageUrl : AppConfig.defaultAvatar;
    }
  }
});
