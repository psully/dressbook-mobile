var app = angular.module('dressbook.helpers');

app.factory("GAnalyticsHelper", function(AppConfig, $cordovaGoogleAnalytics) {
  return {
    debugMode: function(success, error) {
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.debugMode(success, error);
      }
    },
    startTrackerWithId: function(id){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.startTrackerWithId(id);
      }
    },
    trackView: function(view){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.trackView(view);
      }
    },
    trackEvent: function(Category, Action, Label, Value){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.trackEvent(Category, Action, Label, Value);
      }
    },
    addTransaction: function(transactionId, affiliation, revenue, tax, shipping, currencyCode, success, error){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.addTransaction(transactionId, affiliation, revenue, tax, shipping, currencyCode, success, error);
      }
    },
    addTransactionItem: function(transactionId, name ,sku, category, price, quantity, currencyCode, success, error){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.addTransactionItem(transactionId, name ,sku, category, price, quantity, currencyCode, success, error);
      }
    },
    setUserId: function(userId){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.setUserId(userId);
      }
    },
    trackException: function(description, fatal, success, error){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.trackException(description, fatal, success, error);
      }
    },
    addCustomDimension: function(key, value, success, error){
      if (ionic.Platform.isWebView() && typeof analytics !== 'undefined') {
        $cordovaGoogleAnalytics.addCustomDimension(key, value, success, error);
      }
    }
  }
});
