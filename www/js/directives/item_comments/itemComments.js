var app = angular.module('dressbook.directives');
app.directive('itemComments', ['$ionicScrollDelegate', '$ionicLoading', '$ionicPopup', '$timeout', '$rootScope', 'Item', 'AppConfig', 'AuthFactory', 'Utils', function($ionicScrollDelegate, $ionicLoading, $ionicPopup, $timeout, $rootScope, Item, AppConfig, AuthFactory, Utils) {
	return {
		templateUrl: 'js/directives/item_comments/item_comments.html',
		scope: {
			// search:'=?keyword',
			item:'=item',
			forcus:'=?forcus' // will implement later if required
		},
		restrict: 'EA',
		controller: function($scope, GAnalyticsHelper) {
			$scope.main = $rootScope.main;
			$scope.goToProfile = $rootScope.goToProfile;
			$scope.Utils = Utils;

			// show loading
			$scope.isLoadding = true;
			$scope.isLoadError = false;
			$scope.isAddingComment = false;
			$scope.isAddCommentError = false;
			$scope.isAddCommentSuccess = false;
			$scope.newCommentData = {};
			$scope.comments = [];

			async.parallel([
				// load item
				function(callback){
					if(_.isString($scope.item)){
						var item = Item.get({id:$scope.item}).then(function(){
							$scope.item = item;
							callback();
						}, function(err){
							callback('error');
						});
					}else if(!($scope.item instanceof Item)){
						$scope.item = new Item($scope.item);
						callback();
					}else{
						callback();
					}
				}
			], function(err){
				if(err){
					$scope.isLoadding = false;
					$scope.isLoaddingError = true;
					return ;
				}

				// load item comments
				$scope.item.getComments().then(function(rps){
					$scope.isLoadding = false;
					if(rps.data && rps.data.length){
						$scope.comments = rps.data;
					}

					// init events
					$scope.addComment = function($event, form){
						$event.preventDefault();

						// show loadding adding comments
						$scope.isAddingComment = true;

						$scope.item.addComment($scope.newCommentData).then(function(rps){
							GAnalyticsHelper.trackEvent('Items', 'Leave comment', 'Item '+ $scope.item._id);
							// append comment
							$scope.comments.unshift(rps.data);
							// reset form
							$scope.newCommentData = {};
							$scope.isAddingComment = false;
							$scope.isAddCommentSuccess = true;
							$timeout(function(){
								$scope.isAddCommentSuccess = false
							}, 5000);
						}, function(){
							$scope.isAddingComment = false;
							$scope.isAddCommentError = true;
						});
					}
				}, function(err){
					$scope.isLoadding = false;
					$scope.isLoadError = true;
				});
			});
		}
	}
}]);
