var app = angular.module('dressbook.directives');
app.directive('chatNotificationCount', ['$injector',function ($injector) {
	return {
		templateUrl:'js/directives/chat_notification_count/chat_notification_count.html',
		scope:{},
		restrict: 'EA',
		controller: ['$rootScope','$scope', function($rootScope, $scope) {
			$scope.count = 0;

			var ChatsConnectedService = $injector.get('ChatsConnectedService');
			ChatsConnectedService.onLoad(function(){
				var updateCount = function(){
					$scope.count = _.filter(ChatsConnectedService.chatsConnected, function(connect){
						if(!connect.lastMessage){
							return false;
						}
						return !connect.lastMessage.hasSeen;
					}).length;
					if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
						$scope.$apply();
					}
				};
				
				updateCount();

				ChatsConnectedService.emitter.on('addChatConnected',function(){
					updateCount();
				});
				ChatsConnectedService.emitter.on('updateExistingChatConnected',function(){
					updateCount();
				});
				ChatsConnectedService.emitter.on('update',function(){
					updateCount();
				});
			});
		}]
	}
}]);