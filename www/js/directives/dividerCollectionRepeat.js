var app = angular.module('dressbook.directives');

app.directive('dividerCollectionRepeat', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attributes, ngModel) {
        var item = JSON.parse(attributes.item);
        if(item.isDivider){
          $(element).children().hide();
          $(element).addClass('item-divider')
          $(element).append("<div>"+item.divider+"</div>");
        }
    }
  }
});
