var app = angular.module('dressbook.directives');

app.directive('whenScrolled', ['$timeout', function($timeout) {
    return function(scope, elm, attr) {
        var raw = elm[0];
        
        $timeout(function() {
            raw.scrollTop = raw.scrollHeight;          
        });         
        
        elm.bind('scroll', function() {
            if (raw.scrollTop <= 100) { // load more items before you hit the top
                var sh = raw.scrollHeight
                if(!scope.$$phase && !scope.$root.$$phase){
                    scope.$apply(attr.whenScrolled);
                }
                raw.scrollTop = raw.scrollHeight - sh;
            }
        });
    };
}]);