var app = angular.module('dressbook.directives');
app.directive('multiUpload', ['$ionicScrollDelegate', '$ionicLoading', '$ionicPopup', '$timeout', '$q', '$ionicActionSheet', 'User', 'AppConfig', 'UploadFactory', 'AuthFactory', 'ChatFactory', 'Utils', function($ionicScrollDelegate, $ionicLoading, $ionicPopup, $timeout, $q, $ionicActionSheet, User, AppConfig, UploadFactory, AuthFactory, ChatFactory, Utils) {
	return {
		templateUrl: 'js/directives/multi_upload/multi-upload.html',
		scope: {
			data: '=?data'
		},
		restrict: 'E',
		controller: function($scope) {
			var defaultData = {
				text: 'Upload images',
				fromGalleryOptions:{
				},
				fromCameraOptions:{
				}
			};

			$scope.data = _.merge($scope.data, defaultData);
			
			var multiUpload = new UploadFactory.multiUpload($scope.data);

			$scope.browseUpload = function($event) {
				$event.preventDefault();

				multiUpload.browseUpload();
			};
		}
	}
}]);
