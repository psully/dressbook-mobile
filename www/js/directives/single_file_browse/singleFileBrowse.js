var app = angular.module('dressbook.directives');
app.directive('singleFileBrowse', ['$ionicScrollDelegate', '$ionicLoading', '$ionicPopup', '$timeout', '$q', '$ionicActionSheet', 'User', 'AppConfig', 'UploadFactory', 'AuthFactory', 'ChatFactory', 'Utils', function($ionicScrollDelegate, $ionicLoading, $ionicPopup, $timeout, $q, $ionicActionSheet, User, AppConfig, UploadFactory, AuthFactory, ChatFactory, Utils) {
	return {
		templateUrl: 'js/directives/single_file_browse/single-file-browse.html',
		scope: {
			data: '=?data'
		},
		restrict: 'E',
		controller: function($scope) {
			var defaultData = {
				text: 'Upload images',
				fromGalleryOptions:{
					sourceType:Camera.PictureSourceType.PHOTOLIBRARY
				},
				fromCameraOptions:{
					sourceType: Camera.PictureSourceType.CAMERA
				}
			};

			$scope.data = _.merge(defaultData, $scope.data);
			
			var singleFileBrowse = new UploadFactory.singleFileBrowse($scope.data);

			$scope.browseFile = function($event) {
				$event.preventDefault();

				singleFileBrowse.browseFile();
			};
		}
	}
}]);
