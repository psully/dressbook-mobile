var app = angular.module('dressbook.directives');
app.directive('chatNotification', ['ChatFactory','$ionicPopover',function (ChatFactory, $ionicPopover) {
	return {
		templateUrl:'js/directives/chat_notification/chat_notification.html',
		scope:{
			notPartner:'=notPartner',
			notChat:'=notChat',
		},
		restrict: 'EA',
		controller: ['$scope', function($scope) {
			$scope.syncData = {
				countNews:0
			};
			$ionicPopover.fromTemplateUrl('js/directives/chat_notification/notification_popup.html', {
				scope: $scope
			}).then(function(popover) {
				$scope.popover = popover;
				
				$scope.$on('$destroy', function(){
					$scope.popover.remove();
				});
			});
		}]
	}
}]);