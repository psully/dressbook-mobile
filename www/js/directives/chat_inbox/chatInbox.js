var app = angular.module('dressbook.directives');
app.directive('chatInbox', ['$ionicScrollDelegate', 'User', 'AppConfig', 'AuthFactory', 'Utils', 'ChatsConnectedService', 'Chat', 'swalFac', function($ionicScrollDelegate, User, AppConfig, AuthFactory, Utils, ChatsConnectedService, Chat, swalFac) {
	return {
		templateUrl: 'js/directives/chat_inbox/chat_inbox.html',
		scope: {
			// search:'=?keyword',
			notPartner:'=?notPartner',
			notChat:'=?notChat',
			syncData:'=?syncData'
		},
		restrict: 'EA',
		controller: function($scope) {
			$scope.utils = Utils;
			$scope.showLoading = true;
			$scope.userAvatar = AppConfig.defaultAvatar;
			$scope.groupAvatar = AppConfig.groupAvatar;
			$scope.chatsConnected = [];
			$scope.user = null;

			$scope.removeChat = function(event, connect){
				event.preventDefault();
				event.stopPropagation();

				var confirmDialogOptions;
				if(connect.type == 'private'){
					confirmDialogOptions = {
						title:'Remove chat confirm',
						text:'Are you sure you want to delete this chat?',
		        showLoaderOnConfirm: true,
						closeOnConfirm: false
					};
				}else{
					confirmDialogOptions = {
						title:'Leave chat confirm',
						text:'Are you sure you want to leave this group chat?',
		        showLoaderOnConfirm: true,
						closeOnConfirm: false
					};
				}
				swalFac.confirm(confirmDialogOptions).then(function(){
					console.log('You are sure');
					var chat = new Chat(connect);
					chat.leave().then(function(){
						swalFac.success({
							title: 'Success',
							text: connect.type == 'private'?'You have been delete chat success.':'You have been leave group chat success.'
						});
						$scope.chatsConnected = _.reject($scope.chatsConnected, function(chatConnect){
							return chatConnect._id == connect._id;
						});
						// reject this connected from ChatsConnectedService
						ChatsConnectedService.chatsConnected = _.reject(ChatsConnectedService.chatsConnected, function(chatConnect){
							return chatConnect._id == connect._id;
						});
						ChatsConnectedService.runUpdate();
					}, function(){
						swalFac.error({
							title: 'Error!',
							text: connect.type == 'private'?'Has error with delete chat.':'Has error with leave group chat.'
						});
					});
				}, function(){
					console.log("cancel");
				});
			};

			var applyFilter = function(){
				$scope.chatsConnected = _.reject(ChatsConnectedService.chatsConnected, function(connect){
					if($scope.notPartner && connect.user){
						return $scope.notPartner == connect.user._id;
					}
					return false;
				});
				$scope.chatsConnected = _.reject($scope.chatsConnected, function(connect){
					if($scope.notChat){
						return $scope.notChat == connect._id;
					}
					return false;
				});
			};

			var buildChatsConnected = function(){
				applyFilter();
				// make sync data
				if($scope.syncData){
					$scope.syncData.countNews = _.filter($scope.chatsConnected, function(connect){
						if(connect.lastMessage){
							return !connect.lastMessage.hasSeen;
						}
						return false;
					}).length;
				}
				if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
					$scope.$apply();
				}
			};

			ChatsConnectedService.emitter.on('addChatConnected',function(){
				buildChatsConnected();
			});
			ChatsConnectedService.emitter.on('updateExistingChatConnected',function(){
				buildChatsConnected();
			});

			$scope.$watch('notPartner', function(newValue){
				if(newValue){
					applyFilter();
				}
			});
			$scope.$watch('notChat', function(newValue){
				if(newValue){
					applyFilter();
				}
			});

			var getUserAvatar = function(user) {
				var avatar = user.imageUrl ? AppConfig.server + AppConfig.profilePicturePath + '/' + user.imageUrl : AppConfig.defaultAvatar;
				return avatar;
			};

			ChatsConnectedService.onLoad(function(){
				buildChatsConnected();
				$scope.showLoading = false;
			});
		}
	}
}]);
