var app = angular.module('dressbook.directives');
app.directive('multiFilesBrowse', ['$ionicScrollDelegate', '$ionicLoading', '$ionicPopup', '$timeout', '$q', '$ionicActionSheet', 'User', 'AppConfig', 'UploadFactory', 'AuthFactory', 'ChatFactory', 'Utils', function($ionicScrollDelegate, $ionicLoading, $ionicPopup, $timeout, $q, $ionicActionSheet, User, AppConfig, UploadFactory, AuthFactory, ChatFactory, Utils) {
	return {
		templateUrl: 'js/directives/multi_files_browse/multi-files-browse.html',
		scope: {
			data: '=?data'
		},
		restrict: 'E',
		controller: function($scope) {
			var defaultData = {
				text: 'Upload images',
				fromGalleryOptions:{
				},
				fromCameraOptions:{
					sourceType: Camera.PictureSourceType.CAMERA
				}
			};

			$scope.data = _.merge($scope.data, defaultData);
			
			var multiBrowse = new UploadFactory.multiFilesBrowse($scope.data);

			$scope.browseFiles = function($event) {
				$event.preventDefault();

				multiBrowse.browseFiles();
			};
		}
	}
}]);