var app = angular.module('dressbook.directives');
app.directive('userNotificationButton', ['NotificationFactory','$ionicPopover',function (NotificationFactory, $ionicPopover) {
	return {
		templateUrl:'js/directives/user_notification/button/template.html',
		scope:{},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state','AppConfig','Utils', function($rootScope, $scope, $state, AppConfig, Utils) {
			$scope.AppConfig = AppConfig;
			$scope.utils = Utils;
			$scope.countNews = 0;

			$scope.countNews = NotificationFactory.countNews();
			var notificationEmitter = NotificationFactory.emitter();

			notificationEmitter.on('init', function(){
				$scope.countNews = NotificationFactory.countNews();
			});

			notificationEmitter.on('countNewsChanged', function(){
				$scope.countNews = NotificationFactory.countNews();
			});

			notificationEmitter.on('notification', function(){
				if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
					$scope.$apply(function(){
						$scope.countNews = NotificationFactory.countNews();
					});
				}else{
					$scope.countNews = NotificationFactory.countNews();
				}
			});
		}]
	}
}]);
