var app = angular.module('dressbook.directives');
app.directive('friendReqAcceptedNotificationItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/friend_req_accepted_notification_item/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$ionicPopup', '$timeout', 'Order','AppConfig','Utils', "User", function($rootScope, $scope, $state, $ionicPopup, $timeout, Order, AppConfig, Utils, User) {
			$scope.AppConfig = AppConfig;
			$scope.utils = Utils;
			$scope._ = _;
		}]
	}
}]);
