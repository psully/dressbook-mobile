var app = angular.module('dressbook.directives');
app.directive('borrowNotificationNormalItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/borrow_notification_item/borrow_normal/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$ionicPopup', '$timeout', 'Borrow','AppConfig','Utils', function($rootScope, $scope, $state, $ionicPopup, $timeout, Borrow, AppConfig, Utils) {
			$scope.utils = Utils;

			$scope.notificationClick = function($event){
				$event.preventDefault();
				return $state.go('app.message_chat',{user:$scope.notification.additionData.borrow.borrower._id});
			}

		}]
	}
}]);
