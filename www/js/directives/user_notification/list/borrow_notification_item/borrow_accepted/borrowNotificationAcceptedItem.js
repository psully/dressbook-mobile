var app = angular.module('dressbook.directives');
app.directive('borrowNotificationAcceptedItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/borrow_notification_item/borrow_accepted/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$timeout', 'Borrow','AppConfig','Utils', 'swalFac', function($rootScope, $scope, $state, $timeout, Borrow, AppConfig, Utils, swalFac) {
			$scope.utils = Utils;

			$scope.showLoading = false;
			$scope.markReturned = function($event){
				$scope.showLoading = true;

				$event.preventDefault();
				$event.stopPropagation();

				var borrow = new Borrow($scope.notification.additionData.borrow);
				borrow.markReturned().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.borrow.state = rps.data.state;
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};


			$scope.notificationClick = function($event){
				$event.preventDefault();
				return ; $state.go('app.message_chat',{user:$scope.notification.additionData.borrow.borrower._id});
			}
		}]
	}
}]);
