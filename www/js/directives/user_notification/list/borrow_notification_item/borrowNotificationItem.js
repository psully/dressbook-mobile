var app = angular.module('dressbook.directives');
app.directive('borrowNotificationItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/borrow_notification_item/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$ionicPopup', '$timeout', 'Borrow','AppConfig','Utils', function($rootScope, $scope, $state, $ionicPopup, $timeout, Borrow, AppConfig, Utils) {
		}]
	}
}]);
