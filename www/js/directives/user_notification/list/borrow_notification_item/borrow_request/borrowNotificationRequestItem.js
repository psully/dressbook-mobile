var app = angular.module('dressbook.directives');
app.directive('borrowNotificationRequestItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/borrow_notification_item/borrow_request/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$timeout', 'Borrow','AppConfig','Utils', 'swalFac', function($rootScope, $scope, $state, $timeout, Borrow, AppConfig, Utils, swalFac) {
			$scope.AppConfig = AppConfig;
			$scope.utils = Utils;

			$scope._ = _;

			$scope.showLoading = false;
			$scope.acceptBorrow = function($event){
				$scope.showLoading = true;

				$event.preventDefault();
				$event.stopPropagation();

				var borrow = new Borrow($scope.notification.additionData.borrow);
				borrow.accept().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.borrow.state = rps.data.state;
					$timeout(function(){
						$state.go('app.message_chat',{user:$scope.notification.additionData.borrow.borrower._id});
					}, 500);
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};
			$scope.rejectBorrow = function($event){
				$scope.showLoading = true;

				$event.preventDefault();
				$event.stopPropagation();

				var borrow = new Borrow($scope.notification.additionData.borrow);
				borrow.reject().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.borrow.state = rps.data.state;
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};

			$scope.markDelivered = function($event){
				$scope.showLoading = true;
				$event.preventDefault();
				$event.stopPropagation();

				var borrow = new Borrow($scope.notification.additionData.borrow);
				borrow.markDelivered().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.borrow.state = rps.data.state;
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};

			$scope.markReceivedBack = function($event){
				$scope.showLoading = true;
				$event.preventDefault();
				$event.stopPropagation();

				var borrow = new Borrow($scope.notification.additionData.borrow);
				borrow.markReceivedBack().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.borrow.state = rps.data.state;
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};

			$scope.notificationClick = function($event){
				$event.preventDefault();
				return $state.go('app.message_chat',{user:$scope.notification.additionData.borrow.borrower._id});
			}
		}]
	}
}]);
