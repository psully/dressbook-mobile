var app = angular.module('dressbook.directives');
app.directive('orderNotificationItem', [function () {
	return {
		templateUrl:'js/directives/user_notification/list/order_notification_item/template.html',
		scope:{
			notification:'=notification',
			popover:'=popover'
		},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$timeout', 'Order','AppConfig','Utils', "User", 'swalFac', function($rootScope, $scope, $state, $timeout, Order, AppConfig, Utils, User, swalFac) {
			$scope.AppConfig = AppConfig;
			$scope.utils = Utils;

			$scope._ = _;

			$scope.notificationClicked = function(event, notification){
				event.preventDefault();

				if(notification.additionData.order.is_accept){
					// redirect to private chat
					return $state.go('app.message_chat',{user:notification.additionData.order.lender._id});
				}else{
					// do nothing
				}
			};

			$scope.showLoading = false;
			$scope.markShippedOrder = function($event){
				$scope.showLoading = true;

				$event.preventDefault();
				$event.stopPropagation();

				var order = new Order($scope.notification.additionData.order);
				order.markShipped().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.order.shipped = true;
					$timeout(function(){
						$state.go('app.message_chat',{user:$scope.notification.additionData.order.buyer._id});
					}, 500);
				}, function(rps){
					console.log(rps);
					if(rps.data.user){
						$rootScope.main.user = new User(rps.data.user);
					}
					$scope.showLoading = false;
					swalFac.error({
						text: rps.data.error?rps.data.error:'Has error in processing data',
						title: 'Error!'
					});
				});
			};

			/*$scope.rejectOrder = function($event){
				$event.preventDefault();
				$event.stopPropagation();

				var order = new Order($scope.notification.additionData.order);
				order.reject().then(function(rps){
					$scope.showLoading = false;
					$scope.notification.additionData.order.rejected = true;
				}, function(rps){
					$scope.showLoading = false;
					swalFac.error({
						text: 'Has error in processing data',
						title: 'Error!'
					});
				});
			};*/

			$scope.notificationClick = function($event){
				$event.preventDefault();
				$state.go('app.message_chat',{user:$scope.notification.additionData.order.buyer._id});
			}
		}]
	}
}]);
