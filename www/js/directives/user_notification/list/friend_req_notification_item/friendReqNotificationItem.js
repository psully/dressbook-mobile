var app = angular.module('dressbook.directives');
app.directive('friendReqNotificationItem', [function() {
  return {
    templateUrl: 'js/directives/user_notification/list/friend_req_notification_item/template.html',
    scope: {
      notification: '=notification',
      popover: '=popover'
    },
    restrict: 'EA',
    controller: ['$rootScope', '$scope', '$state', '$timeout', 'Order', 'AppConfig', 'Utils', "User", 'swalFac', function($rootScope, $scope, $state, $timeout, Order, AppConfig, Utils, User, swalFac) {
      $scope.AppConfig = AppConfig;
      $scope.utils = Utils;

      $scope._ = _;

      $scope.showLoading = false;
      $scope.acceptFriendReq = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        /*$scope.showLoading = true;

        $rootScope.main.user.acceptFriendReq($scope.notification.additionData.request._id).then(function(res) {
          $scope.showLoading = false;
          $scope.notification.additionData.request.status = 2;
          $rootScope.main.user.friends = res.data.friends;
          $rootScope.main.user.friendsRequesting = res.data.friendsRequesting;
          if (!$rootScope.$$phase && (!$rootScope.$root || !$rootScope.$root.$$phase)) {
            $rootScope.$apply();
          }
        }, function(err) {
          $scope.showLoading = false;
          swalFac.error({
            text: rps.data.error ? rps.data.error : 'Has error in processing data',
            title: 'Error!'
          });
        });*/

        // show confirm
        swalFac.confirmSuccessWithAvatar({
          title: 'Confirm friend request',
          imageUrl: Utils.getUserAvatar($scope.notification.additionData.request.from),
          html: 'Please confirm you accept friend request from <strong>' + $scope.notification.additionData.request.from.username + '</strong>',
          confirmButtonText: 'Confirm',
          cancelButtonText: 'Cancel',
          showLoaderOnConfirm: true,
          closeOnConfirm: false
        }).then(function() {
          $scope.showLoading = true;

          $rootScope.main.user.acceptFriendReq($scope.notification.additionData.request._id).then(function(res) {
            $scope.showLoading = false;
            $scope.notification.additionData.request.status = 2;
            $rootScope.main.user.friends = res.data.friends;
            $rootScope.main.user.friendsRequesting = res.data.friendsRequesting;
            if (!$rootScope.$$phase && (!$rootScope.$root || !$rootScope.$root.$$phase)) {
              $rootScope.$apply();
            }

            swalFac.success({
              title: "Friend added",
              html: 'You and <strong>' + $scope.notification.additionData.request.from.username + '</strong> are now friend.'
            });
          }, function(err) {
            $scope.showLoading = false;
            swalFac.error({
              text: rps.data.error ? rps.data.error : 'Has error in processing data',
              title: 'Error!'
            });
          });
        }, function(){
          console.log("Cancel");
        });
      };

      $scope.rejectFriendReq = function($event) {
        $scope.showLoading = true;

        $event.preventDefault();
        $event.stopPropagation();

        $rootScope.main.user.rejectFriendReq($scope.notification.additionData.request._id).then(function(res) {
          $scope.showLoading = false;
          $scope.notification.additionData.request.status = 3;
        }, function(err) {
          $scope.showLoading = false;
          swalFac.error({
            text: rps.data.error ? rps.data.error : 'Has error in processing data',
            title: 'Error!'
          });
        });
      };
    }]
  }
}]);
