var app = angular.module('dressbook.directives');
app.directive('userNotificationList', ['NotificationFactory','$ionicPopover',function (NotificationFactory, $ionicPopover) {
	return {
		templateUrl:'js/directives/user_notification/list/template.html',
		scope:{},
		restrict: 'EA',
		controller: ['$rootScope', '$scope', '$state', '$timeout', '$ionicScrollDelegate', 'AppConfig', 'Utils', function($rootScope, $scope, $state, $timeout, $ionicScrollDelegate, AppConfig, Utils) {
			$scope.$on('$destroy', function(event) {
				NotificationFactory.markRead(true).then(function(){
			  	NotificationFactory.setCountNews(0);
				});
			});
			
			$scope.canInfiniteLoad = true;
			$scope.hasLoad = false;
		  $scope.notifications = NotificationFactory.notifications();
		  var notificationEmitter = NotificationFactory.emitter();

		  notificationEmitter.on('notification', function(notification){
		    if(!$scope.$$phase && (!$scope.$root || !$scope.$root.$$phase)){
		      $scope.$apply(function(){
		        $scope.notifications = NotificationFactory.notifications();
		        // NotificationFactory.setCountNews(0);
		      });
		    }else{
					$scope.notifications = NotificationFactory.notifications();
					// NotificationFactory.setCountNews(0);
				}
		  });

			notificationEmitter.on('loadNext', function(){
				$scope.hasLoad = true;
				$scope.notifications = NotificationFactory.notifications();
				$scope.canInfiniteLoad = !NotificationFactory.isFullLoaded();

				$timeout(function(){
	        $scope.$broadcast('scroll.infiniteScrollComplete');
	        $ionicScrollDelegate.resize();
	      });
			});

		  NotificationFactory.markRead(false).then(function(){
		  	NotificationFactory.setCountNews(0);
			});

			// -------------------------------
			$scope.loadMoreNotifications = function(){
				console.log("Load next");
				NotificationFactory.loadNext();
			}
		}]
	}
}]);
