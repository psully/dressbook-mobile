var app = angular.module('dressbook.directives');
app.directive('singleUpload', ['$ionicScrollDelegate', '$ionicLoading', '$ionicPopup', '$timeout', '$q', '$ionicActionSheet', 'User', 'AppConfig', 'UploadFactory', 'AuthFactory', 'ChatFactory', 'Utils', function($ionicScrollDelegate, $ionicLoading, $ionicPopup, $timeout, $q, $ionicActionSheet, User, AppConfig, UploadFactory, AuthFactory, ChatFactory, Utils) {
	return {
		templateUrl: 'js/directives/single_upload/single-upload.html',
		scope: {
			data: '=?data'
		},
		restrict: 'E',
		controller: function($scope) {
			var defaultData = {
				text: 'Upload images',
				fromGalleryOptions:{
				},
				fromCameraOptions:{
				}
			};

			$scope.data = _.merge($scope.data, defaultData);
			
			var singleUpload = new UploadFactory.singleUpload($scope.data);

			$scope.browseUpload = function($event) {
				$event.preventDefault();

				singleUpload.browseUpload();
			};
		}
	}
}]);