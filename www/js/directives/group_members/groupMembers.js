var app = angular.module('dressbook.directives');
app.directive('groupMembers', ['$ionicScrollDelegate', '$timeout', 'User', 'AppConfig', 'AuthFactory', 'ChatFactory', 'Utils', 'swalFac', function($ionicScrollDelegate, $timeout, User, AppConfig, AuthFactory, ChatFactory, Utils, swalFac) {
	return {
		templateUrl: 'js/directives/group_members/group_members.html',
		scope: {
			syncData: '=?syncData'
		},
		restrict: 'EA',
		controller: function($scope) {
			$scope.userId = AuthFactory.getUserFromToken().id;
			$scope.utils = Utils;

			$scope.ormitUsers = [];

			var currentPage = 0;
			var maxPagesLoad = 8;
			var endInfiniteLoad = false;
			var perPage = 25;

			$scope.moreDataCanBeLoaded = function() {
				return !endInfiniteLoad && currentPage <= maxPagesLoad;
			};

			$scope.chatters = [];
			$scope.showEmpty = false;

			var reloadChatters = function() {
				// reset state
				currentPage = 0;
				endInfiniteLoad = false;
				$scope.chatters = [];
				$scope.showEmpty = false;
				// begin search
				$scope.$broadcast('scroll.infiniteScrollComplete');
			};

			$scope.loadMoreChatters = function() {
				var ormitUsers = $scope.ormitUsers;
				if($scope.syncData.ormitUsers && $scope.syncData.ormitUsers.length){
					ormitUsers = ormitUsers.concat($scope.syncData.ormitUsers);
				}
				ChatFactory.loadChatters({
					page: ++currentPage,
					perPage: perPage,
					ormitUsers: ormitUsers,
					keyword: $scope.searchChattersKeyword
				}).then(function(rps) {
					endInfiniteLoad = rps.data < perPage;

					// build avatar
					rps.data = _.map(rps.data, function(item) {
						item.user.avatar = Utils.getUserAvatar(item.user);
						return item;
					});

					rps.data.forEach(function(item) {
						$scope.chatters.push(item);
					});

					// success
					if ($scope.chatters.length == 0) {
						$scope.showEmpty = true;
					}

					$scope.$broadcast('scroll.infiniteScrollComplete');
				}, function(err) {
					swalFac.error({
						text: 'Has error in loadding data. Please try again.',
						title: 'Error!'
					});
				});
			}

			var typingTimeOut;
			var searchChanged = false;
			$scope.typingSearchChatters = function($event) {
				if (typingTimeOut) {
					$timeout.cancel(typingTimeOut);
					typingTimeOut = undefined;
				}
				searchChanged = false;

				typingTimeOut = $timeout(function() {
					if (!searchChanged) {
						reloadChatters();
						searchChanged = true;
					}
					typingTimeOut = undefined;
				}, 1000);
			};

			$scope.reqSearchChatters = function($event) {
				if (!searchChanged) {
					reloadChatters();
					searchChanged = true;
				}
			};

			$scope.addToGroup = function($event, userId, index) {
				$event.preventDefault();

				// call sync function
				$scope.syncData.addToGroup($scope.chatters[index]);

				// add to ormit users
				$scope.ormitUsers.push($scope.chatters[index].user._id);

				// remove from chatters
				var temp = _.reject($scope.chatters, function(chatter) {
					return chatter.user._id == userId;
				});

				$scope.chatters = [];
				$timeout(function() {
					temp.forEach(function(item) {
						$scope.chatters.push(item);
					});
				});
			};

			// register sync function
			$scope.syncData.removeToGroup = function(chatter) {
				// remove from ormit users
				$scope.ormitUsers = _.reject($scope.ormitUsers, function(userId){
					return userId == chatter.user._id;
				});

				// add to chatters
				var indexInChatters = _.sortedIndexBy($scope.chatters, chatter, function(chatter) {
					return chatter.user.name;
				});

				var temp = $scope.chatters;
				$scope.chatters = [];
				temp.splice(indexInChatters, 0, chatter);

				$timeout(function() {
					temp.forEach(function(item) {
						$scope.chatters.push(item);
					});
				});
			};
		}
	}
}]);
