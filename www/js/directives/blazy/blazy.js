var app = angular.module('dressbook.directives');
app.directive('bLazy', [function () {
  return {
    templateUrl: 'js/directives/blazy/blazy-template.html',
    scope:{
      image: '=imageData'
    },
    restrict: 'EA',
    controller: ['$scope', '$timeout', 'AppConfig', function($scope, $timeout, AppConfig) {
      $scope.AppConfig = AppConfig;
      $timeout(function(){
        new Blazy({
          src: "data-blazy", // large
          breakpoints: [{
            width: 480, // max-width
            src: 'data-blazy-thumb'
          }, {
            width: 768, // max-width
            src: 'data-blazy-medium'
          }, {
            width: 1080, // max-width
            src: 'data-blazy-big'
          }],
          success: function(element) {
            $timeout(function() {
              // We want to remove the loader gif now.
              // First we find the parent container
              // then we remove the "loading" class which holds the loader image
              var parent = element.parentNode;
              parent.className = parent.className.replace(/\bloading\b/, '');
            }, 200);
          }
        });
      });
    }]
  }
}]);