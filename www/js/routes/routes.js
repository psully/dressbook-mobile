var app = angular.module('dressbook.routes', ['jett.ionic.filter.bar', 'dressbook.controllers', 'dressbook.services', 'dressbook.configs', 'dressbook.models']);

//  APP CONFIG AND ROUTES.
app.config(function($stateProvider, $urlRouterProvider, $ionicFilterBarConfigProvider, $httpProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'js/templates/menu.html',
      controller: 'AppCtrl'
    })
    .state('app.none', {
      url: '/none',
      resolve: {
        none: function($q, $timeout, $state, AuthFactory) {
          var defer = $q.defer();
          $timeout(function() {
            defer.reject();
          });
          return defer.promise;
        }
      }
    })
    .state('app.TandC', {
      url: '/TandC?back',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/TandC.html',
          controller: 'TAndCCtrl'
        }
      },
      env: {
        dragContent: false
      }
    })
    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/login.html',
          controller: 'LoginCtrl'
        }
      },
      cache: false,
      env: {
        dragContent: false
      }
    })

  .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/signup.html',
        controller: 'SignupCtrl'
      }
    },
    // cache: false,
    env: {
      dragContent: false
    }
  })

  .state('app.resetPassword', {
    url: '/resetpassword',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/reset-password.html',
        controller: 'ResetPasswordCtrl'
      }
    },
    cache: false,
    env: {
      dragContent: false
    }
  })

  .state('app.logout', {
    url: '/logout',
    resolve: {
      logout: function($q, $timeout, $state, AuthFactory) {
        var defer = $q.defer();
        $timeout(function() {
          AuthFactory.logout();
          $state.go("app.login");
          // everything is fine, proceed
          // deferred.resolve();
          defer.reject();
        });
        return defer.promise;
      }
    }
  })

  .state('app.search', {
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/search.html'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.browse', {
      url: '/browse',
      resolve: {
        browse: function($q, $timeout, $state, $ionicActionSheet) {
          var defer = $q.defer();
          $timeout(function() {
            $ionicActionSheet.show({
              buttons: [{
                text: '<i class="icon ion-ios-list-outline"></i> Category'
              }, {
                text: '<i class="icon ion-ios-calendar-outline"></i> Available Date'
              }, {
                text: '<i class="icon ion-ios-location-outline"></i> Location'
              }],
              // destructiveText: 'Delete',
              titleText: 'Choose to browse',
              cancelText: 'Cancel',
              cancel: function() {
                defer.reject();
              },
              buttonClicked: function(index) {
                if (index == 0) {
                  $state.go("app.browseCategories");
                  defer.reject();
                } else if (index == 1) {
                  $state.go("app.browseAvailableDate");
                  defer.reject();
                } else if (index == 2) {
                  $state.go("app.browseLocation");
                  defer.reject();
                }
                return true;
              }
            });
          });
          return defer.promise;
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.browseCategories', {
      url: '/browse-categories?current&keyword',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/browse-categories.html',
          controller: 'BrowseCategoriesCtrl'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.browseAvailableDate', {
      url: '/browse-available-date',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/browse-available-date.html',
          controller: 'BrowseAvailableDateCtrl'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.browseLocation', {
      url: '/browse-location',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/browse-location/browse-location.html',
          controller: 'BrowseLocationCtrl'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/home.html',
        controller: 'AppCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.invite', {
    url: '/invite',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/invite.html',
        controller: 'InviteCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  /*.state('app.inbox', {
    url: '/inbox',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/inbox.html',
        controller: 'AppCtrl'
      }
    },
    access: {requiredLogin: true}
  })*/

  .state('app.chats', {
    url: '/chats',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/chats.html',
        controller: 'AppCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.profile', {
    url: '/user/profile',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/profile.html',
        controller: 'UserCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.userScore', {
    url: '/user/score',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/score.html',
        controller: 'UserScoreCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.userTransactions', {
    url: '/user/transactions',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/transactions.html',
        controller: 'UserTransactionCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.viewUser', {
    url: '/user/view/:user',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/view_user.html',
        controller: 'ViewUserCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.friends', {
    url: '/user/friends',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/friends.html',
        controller: 'friendsCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  // .state('app.paymentSettings', {
  //   url: '/user/payment-settings',
  //   views: {
  //     'menuContent': {
  //       templateUrl: 'js/templates/user/payment-settings.html',
  //       controller: 'UserPaymentSettingsCtrl'
  //     }
  //   },
  //   cache: false,
  //   access: {
  //     requiredLogin: true
  //   }
  // })

  .state('app.searchUser', {
    url: '/user/search',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/user/search.html',
        controller: 'searchUserCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.connectCallback', {
    url: '/user/connect-callback?scope&code&error&error_description',
    resolve: {
      solvePromise: ["$rootScope", "$q", "$http", "$timeout", "$state", "$stateParams", "AuthFactory", "AppConfig", "User", "swalFac", function($rootScope, $q, $http, $timeout, $state, $stateParams, AuthFactory, AppConfig, User, swalFac) {
        var defer = $q.defer();

        var errorCallback = function() {
          swalFac.error({
            text: 'Has error in connect your stripe accound with dressbook. Please try again.',
            title: 'Error!'
          }, function(){
            // reidrect to payment settings
            $state.go("app.paymentSettings");
            // everything is fine, proceed
            // deferred.resolve();
            defer.reject();
          });
        }

        $timeout(function() {
          if ($stateParams.error) {
            errorCallback();
          } else {
            // send code to server to excute connect
            $http({
              method: 'POST',
              url: AppConfig.server + '/api/payment/connectcallback',
              data: $stateParams
            }).then(function(rps) {
              console.log(rps);
              $rootScope.main.user = new User(rps.data);

              $state.go("app.paymentSettings");
              defer.reject();
            }, function(err) {
              errorCallback();
            });
          }
        });
        return defer.promise;
      }]
    },
    access: {
      requiredLogin: true,
      staySession: true
    }
  })

  .state('app.message_inbox', {
    url: '/message/inbox',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/message/inbox.html',
        controller: 'Message_InboxCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.message_newchat', {
    url: '/message/newchat',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/message/newchat.html',
        controller: 'Message_NewChatCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.message_chat', {
    url: '/message/chat/:user',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/message/chat.html',
        controller: 'Message_ChatCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.message_creategroup', {
    url: '/message/creategroup',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/message/creategroup.html',
        controller: 'Message_CreateGroupCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.message_chatgroup', {
    url: '/message/chatgroup/:chat',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/message/chatgroup.html',
        controller: 'Message_ChatGroupCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.history', {
    url: '/history',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/history.html',
        controller: 'AppCtrl'
      }
    },
    access: {
      requiredLogin: true
    }
  })

  .state('app.itemAdd', {
    url: '/item/add',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/item/add-edit/add.html',
        controller: 'AddItemCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.itemEdit', {
    url: '/item/edit/:id',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/item/add-edit/edit.html',
        controller: 'EditItemCtrl'
      }
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.listItems', {
    url: '/item/list?cat&keyword&availableFrom&availableTo',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/item/list.html',
        controller: 'ListItemCtrl'
      }
    },
    onEnter: function($rootScope) {
      $rootScope.showSearchIcon = true;
    },
    onExit: function($rootScope) {
      $rootScope.showSearchIcon = false;
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.listOwnItems', {
    url: '/item/listown',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/item/list_own.html',
        controller: 'ListOwnItemsCtrl'
      }
    },
    onEnter: function($rootScope) {
      $rootScope.showSearchIcon = true;
    },
    onExit: function($rootScope) {
      $rootScope.showSearchIcon = false;
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.ListItemsLiked', {
    url: '/item/listliked',
    views: {
      'menuContent': {
        templateUrl: 'js/templates/item/list_liked.html',
        controller: 'ListItemsLikedCtrl'
      }
    },
    onEnter: function($rootScope) {
      $rootScope.showSearchIcon = true;
    },
    onExit: function($rootScope) {
      $rootScope.showSearchIcon = false;
    },
    cache: false,
    access: {
      requiredLogin: true
    }
  })

  .state('app.listUserItems', {
      url: '/item/listfor/:user',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/list_for_user.html',
          controller: 'ListUserItemsCtrl'
        }
      },
      onEnter: function($rootScope) {
        $rootScope.showSearchIcon = true;
      },
      onExit: function($rootScope) {
        $rootScope.showSearchIcon = false;
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.listItemsUserLiked', {
      url: '/item/listlikedfor/:user',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/list_liked_for_user.html',
          controller: 'ListItemsUserLikedCtrl'
        }
      },
      onEnter: function($rootScope) {
        $rootScope.showSearchIcon = true;
      },
      onExit: function($rootScope) {
        $rootScope.showSearchIcon = false;
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.itemView', {
      url: '/item/:id?startDate&returnDate',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/item/view.html',
          controller: 'ViewItemCtrl'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    })
    .state('app.notifications', {
      url: '/notifications',
      views: {
        'menuContent': {
          templateUrl: 'js/templates/user/notifications.html',
          controller: 'UserNotificationsCtrl'
        }
      },
      cache: false,
      access: {
        requiredLogin: true
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
